<?php
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

namespace PSData;                   //  Указываем пространство имён

/*
git init
git remote add compile /cygdrive/d/Work/git/DBSIDE
git fetch compile
git pull compile master

*/

//  Русские символы в файлах: git config --global core.quotepath off

//  Получение списка файлов коммита: git show --pretty=format:"" --name-only b886ae7cf09c179217895992f79976a33b5f2978
//  Получение списка коммитов в формате "ХЕШ|ИмяАвтора|Дата|Описание": git log --pretty=format:"%H | %an | %ad | %s"
//  Получение списка коммитов в формате "ХЕШ|ИмяАвтора|ДатаВUnixTime|Описание": git log --pretty=format:"%H | %an | %at | %s"
//  Список файлов в репозитории (в текущей ветке): git ls-tree --name-only -r HEAD
//  Список коммитов файла: git log --all --pretty=format:'%H|%an|%ci|%s' BIS/bis/CIS_CUSTOM_PROG_AGE/cis_custom_prog_age_body.sql
//  Получение файла нужной ревизии: git checkout b886ae7cf09c179217895992f79976a33b5f2978
//                                  Считать файл


/*

setrevpath = setRevisionsPath
getrevpath = getRevisionsPath
getlastid = getLastRevision
checkid = checkRevisionID
getfilelist = getRevisionFullFileList
getfilerevisions = getRevisionsFile
getfilelastrevision = getLastRevisionFile
getfile = getFile
status = gitStatus
*/


/**
 *  Интерфейс для класса работы с VCS (Version Control System)
 */
interface VCS
{
    /**
     *  Инициализация репозитория (прямой вызов из конструктора). Все параметры могут быть переданы одним массивом (ключ/значение), обязательный параметр
     *  только первый, для остальных должно быть предусмотренно значение по-умолчанию
     *
     * @param string $remoterepo   Полный адрес удалённого (от слова дальний) репозитория
     * @param string $remotename   [optional] Название удалённого репозитория
     * @param string $remotebranch [optional] Удалённая ветку репозитория
     * @param string $localrepo    [optional] Каталог, куда будет скачан репозиторий
     * @param string $remotekey    [optional] Путь к файлу ключа (если нужно работать по ssh)
     * @param string $log          [optional] Путь к файлу лога (если нужно логирование)
     */
    static function initRepo($remoterepo, $remotename, $remotebranch, $localrepo, $remotekey, $log);
    
    /**
     *  Установка адреса удалённого репозитория
     *
     * @param string $var Адрес удалённого репозитория
     */
    static function setRemoteRepo($var);
    
    /**
     *  Установка ветки удалённого репозитория
     *
     * @param string $var Удалённая (от слова дальний) ветка репозитория
     */
    static function setRemoteBranch($var);
    
    /**
     *  Установка файла ключа для работы с репозиторием
     *
     * @param string $var Строка, содержащая путь к файлу ключа (если работать по ssh)
     */
    static function setRemoteKey($var);
    
    /**
     *  Установка названия удалённого репозитория
     *
     * @param string $var Имя удалённого репозитория
     */
    static function setRemoteName($var);
    
    /**
     *  Установка каталога локального репозитория
     *
     * @param string $var Каталог, куда будет скачан репозиторий
     */
    static function setLocalRepo($var);
    
    /**
     *  Установка лог файла
     *
     * @param string $var Путь к файлу лога
     */
    static function setLog($var);
    
    /**
     *  Установка разрешения логирования
     *
     * @param bool $var Флаг, указывающий на необходимость логирования
     */
    static function setLogging($var);
    
    //  Работа со списком ревизий/коммитов (далее ревизий)
    
    /**
     *  Получение идентификатора последней ревизии
     *
     * @param string $remotebranch [optional] Строка, содержащая название ветки в удалённом репозитории
     */
    static function getLastRevision($remotebranch);
    
    /**
     *  Проверяет идентификатор ревизии на соответствие формату идентификаторов git
     *
     * @param string $id Строка, содержащая идентификатор ревизии
     */
    static function checkRevisionID($id);
    
    /**
     *  Получение списка файлов в указанной ревизии (или по имени/ветке) (только изменённые/добавленные в ней)
     *
     * @param string $remotebranch Строка, содержащая название ветки в удалённом репозитории
     */
    static function getRevisionFileList($remotebranch);
    
    /**
     *  Получение списка файлов в указанной ревизии (или по имени/ветке) (полный список в репозитории, а не только изменённые в ней)
     *
     * @param string $remotebranch Строка, содержащая название ветки в удалённом репозитории
     */
    static function getRevisionFullFileList($remotebranch);
    
    /**
     *  Получение информации о всех ревизиях указанного файла
     *
     * @param string $file         Строка, содержащая полный путь файла в репозитории
     * @param string $remotebranch [optional] Строка, содержащая название ветки в удалённом репозитории
     */
    static function getRevisionsFile($file, $remotebranch);
    
    /**
     *  Получение информации о последней ревизии указанного файла
     *
     * @param string $file         Строка, содержащая полный путь файла в репозитории
     * @param string $remotebranch [optional] Строка, содержащая название ветки в удалённом репозитории
     */
    static function getLastRevisionFile($file, $remotebranch);
    
    
    // Работа с файлами
    
    /**
     *  Получение содержимого файла нужной ревизии из репозитория
     *
     * @param string $file         Строка, содержащая полный путь файла в репозитории
     * @param string $remotebranch [optional] Строка, содержащая название ветки в удалённом репозитории (обязательный параметр)
     * @param string $save_file    [optional] Строка, содержащая полный путь файла, куда записать данные, в файловой системе. При отсутствии содержимое файла
     *                             не записывается, а возвращается
     */
    static function getFile($file, $remotebranch, $save_file);
    
    
}

/**
 * Class Git
 *
 * @package MegaFon\PSData
 */
class Git implements VCS
{
    //  Секция удалённого репозитория
    
    //  Адрес удалённого репозитория
    static $remoterepo = "";
    
    //  Название удалённого репозитория
    static $remotename = "origin";
    
    //  Ветка удалённого репозитория
    static $remotebranch = "master";
    
    //  Путь к приватному ключу (cygwin)
    static $remotekey = "";
    
    //  Секция локального репозитория
    
    //  Путь к локальному репозиторию
    static $localrepo = "";
    
    //  Секция локальных настроек
    
    //  Уникальный идентификатор класса (один на сессию)
    static $uniq = "";
    
    //  Выходной массив stdout после выполнения командыs
    static $out = array();
    
    //  Путь до log файла
    static $log = "";
    
    //  Флаг, указывающий на необходимость логирования операций
    static $logging = false;
    
    public $init = false;
    
    /**
     * Конструктор класса
     *
     * @param array ...$args
     */
    function __construct(...$args)
    {
        $this->init = call_user_func_array(array(__CLASS__, "initRepo"), $args);
        return $this->init;
    }
    
    /**
     *  Установка адреса удалённого репозитория
     *
     * @param string $var Адрес удалённого репозитория
     *
     * @return bool Всегда true
     */
    static function setRemoteRepo($var): bool
    {
        self::$remoterepo = $var;
        return true;
    }
    
    /**
     *  Установка ветки удалённого репозитория
     *
     * @param string $var Удалённая (от слова дальний) ветка репозитория
     *
     * @return bool Всегда true
     */
    static function setRemoteBranch($var): bool
    {
        self::$remotebranch = $var;
        return true;
    }
    
    /**
     *  Установка файла ключа для работы с репозиторием
     *
     * @param string $var Строка, содержащая путь к файлу ключа (если работать по ssh)
     *
     * @return bool Всегда true
     */
    static function setRemoteKey($var): bool
    {
        self::$remotekey = $var;
        return true;
    }
    
    /**
     *  Установка каталога локального репозитория
     *
     * @param string $var Каталог, куда будет скачан репозиторий
     *
     * @return bool Всегда true
     */
    static function setLocalRepo($var): bool
    {
        @mkdir($var, 0775, true);
        self::$localrepo = $var;
        return true;
    }
    
    /**
     *  Установка лог файла
     *
     * @param string $var Путь к файлу лога
     *
     * @return bool Всегда true
     */
    static function setLog($var): bool
    {
        //  Рекурсивно создаём каталог (0775), и файл (0644), если требуется
        @mkdir(dirname($var), 0775, true);
        if (!file_exists($var)) {
            touch($var);
            chmod($var, 0664);
        }
        
        //  Запоминаем имя файла и выходим
        self::$log = $var;
        return true;
    }
    
    /**
     *  Установка разрешения логирования
     *
     * @param bool $var Флаг, указывающий на необходимость логирования
     *
     * @return bool Если предварительно не задан log файл, то false, в остальных случаях - true
     */
    static function setLogging($var): bool
    {
        //  Выходим с false если не указан лог файл по-умолчанию
        if (trim(self::$log) == "")
            return false;
        self::$logging = $var;
        return true;
    }
    
    /**
     *  Установка названия удалённого репозитория
     *
     * @param string $var Имя удалённого репозитория
     *
     * @return bool Всегда true
     */
    static function setRemoteName($var): bool
    {
        
        self::$remotename = $var;
        return true;
    }
    
    /**
     *  Запись данных в log файл
     *
     * @param string $data    Строка, которую нужно записать в log файл
     * @param string $logfile [optional] Имя лог файла, если не указан, то будет использоваться лог файл по-умолчанию
     *
     * @return string|bool Возвращает тот же текст, что и получили или false в случае ошибки
     */
    static function log($data, $logfile = "")
    {
        //  Если передано название лог файла или выставлен флаг логгированиея и указан лог файл по-умолчанию
        if ((trim(self::$log) != "" && self::$logging) || trim($logfile) != "") {
            
            //  Дописываем текст к указанному файлу
            $h = fopen((trim($logfile) != "") ? $logfile : self::$log, "a");
            if (!$h)
                return false;
            fwrite($h, date("Y-m-d_H-i-s")." ".self::$uniq." ".$data."\n");
            fclose($h);
        }
        
        //  Возвращаем тот же текст, что и получили
        return $data;
    }
    
    /**
     *  Выполнение команды git
     *
     * @param string $cmd       Строка, содержащая команду
     * @param bool   $showerror [optional] Флаг, указывающий, перенаправлять ли stderr в поток stdout
     * @param string $post      [optional] Дополнительный код, к команде, подставляется в конец. Можно перенаправить вывод или добавить что-то через pipe
     *
     * @return string stdout команды (возможно с stderr)
     */
    static function cmd($cmd, $showerror = false, $post = ""): string
    {
        $pwd = getcwd();
        
#        $cmd = "git config -l";
        //  Переходим в локальный каталог репозитория, если это не клонирование и не создание локального репозитория
        if (substr($cmd, 0, 9) != "git clone" && substr($cmd, 0, 8) != "git init")
            chdir(self::$localrepo);
        
        //  Подменяем git на полный путь к гиту, если вызывается команда, начинающаяся на git
        if (substr($cmd, 0, 4) == "git ")
            $cmd = GIT_PATH." ".substr($cmd, 4);
        
        //  Добавляем ssh ключ для работы с удалённым репозиторием, если он указан
        if (trim(self::$remotekey) != "")
            $cmd = GIT_RUN_PREFIX." ".self::$remotekey."; ".$cmd."'";
        
        //  Добавляем вывод ошибок в stdout, если это необходимо, а также дополнительные команды
        $cmd = $cmd.($showerror ? " 2>&1" : "").$post;
        
        //  Логируем команду, и выполняем её, получая весь вывод в строке
        self::log($cmd);
        $out = array();
        exec($cmd, $out);
        $out = implode("\n", $out);
        
        //  Удаляем из вывода помойку от команды добавления ключа
        if (trim(self::$remotekey) != "")
            $out = preg_replace("/^Identity added:.*?$/m", "", $out);
        
        //  Если включено логирование, то вывод также добавляем в лог
        if (self::$logging)
            self::log($out);
        
        //  Переходим в исходный каталог и возвращаем текст вывода
        chdir($pwd);
        return $out;
    }
    
    /**
     *  Выполнение локальной команды git (без обращения к удалённому репозиторию)
     *
     * @param string $cmd       Строка, содержащая команду
     * @param bool   $showerror [optional] Флаг, указывающий, перенаправлять ли stderr в поток stdout
     * @param string $post      [optional] Дополнительный код, к команде, подставляется в конец. Можно перенаправить вывод или добавить что-то через pipe
     *
     * @return string stdout команды (возможно с stderr)
     */
    static function localcmd($cmd, $showerror = false, $post = ""): string
    {
        //  Запоминаем remote_key, и стираем его, ибо от него зависит формироватние строки для работы с удалённым репозиторием
        $remote_key = self::$remotekey;
        self::setRemoteKey("");
        
        //  Выполняем команду, восстанавливаем remote_key и возвращаем результат
        $return = self::cmd($cmd, $showerror, $post);
        self::setRemoteKey($remote_key);
        return $return;
    }
    
    /**
     *  Получение информации по репозиторию
     *
     * @return bool|string stdout команды или false при ошибке
     */
    static function gitStatus(): bool
    {
        //  Получаем статус репозитория
        self::$out = self::localcmd("git status", true);
        
        //  Возвращаем результат операции
        return mb_stripos(self::$out, "fatal: ") !== false ? false : self::$out;
    }
    
    /**
     *  Клонирование репозитория
     *
     * @param string $remoterepo   Строка, указывающая полный адрес удалённого (от слова дальний) репозитория
     * @param string $remotename   [optional] Строка, указывающая название удалённого репозитория, по-умолчанию "origin"
     * @param string $remotebranch [optional] Строка, указывающая на удалённую ветку репозитория, по-умолчанию "master"
     * @param string $localrepo    [optional] Строка, указывающая каталог, куда будет скачан репозиторий, по-умолчания текущий каталог
     *
     * @return bool Успешность операции
     */
    static function gitClone($remoterepo = "", $remotename = "", $remotebranch = "", $localrepo = ""): bool
    {
        //  Обработка параметров, переданных в массиве
        if (is_array($remoterepo)) {
            if (trim($remoterepo['remotename']) != "")
                $remotename = $remoterepo['remotename'];
            if (trim($remoterepo['remotebranch']) != "")
                $remotebranch = $remoterepo['remotebranch'];
            if (trim($remoterepo['localrepo']) != "")
                $localrepo = $remoterepo['localrepo'];
            if (trim($remoterepo['remoterepo']) != "")
                $remoterepo = $remoterepo['remoterepo'];
            else
                $remoterepo = "";
        }
        
        //  Выставляем значения из переменных класса, если они не были переданы в параметрах
        if (trim($remoterepo) == "")
            $remoterepo = self::$remoterepo;
        if (trim($remotename) == "")
            $remotename = self::$remotename;
        if (trim($remotebranch) == "")
            $remotebranch = self::$remotebranch;
        if (trim($localrepo) == "")
            $localrepo = self::$localrepo;
        
        //  Конвертируем путь в cygwin совместимый, если установлена переменная окружения CYGWIN
        if ($_SERVER['CYGWIN'] != "")
            $localrepo = preg_replace("/\s*([a-zA-Z]{1}):(?:\/|\\\)(.*)/", "/cygdrive/\\1/\\2", $localrepo);
        
        //  Экранируем передаваемые в командной строке параметры (добавляем при надобности ключи командной строки)
        if (trim($remoterepo) != "")
            $remoterepo = escapeshellarg($remoterepo);
        if (trim($remotename) != "")
            $remotename = escapeshellarg($remotename);
        if (trim($remotebranch) != "")
            $remotebranch = escapeshellarg($remotebranch);
        if (trim($localrepo) != "")
            $localrepo = escapeshellarg($localrepo);
        
        //  Клонируем репозиторий
        self::$out = self::cmd("git clone -o {$remotename} -b {$remotebranch} {$remoterepo} {$localrepo}", true);
        
        //  Возвращаем результат операции
        return (trim(self::$out) != "" && mb_stripos(self::$out, "Could not read from remote repository") === false && mb_stripos(self::$out,
                "already exists and is not an empty directory") === false);
    }
    
    /**
     *  Инициализация пустого репозитория
     *
     * @return bool Успешность операции
     */
    static function gitInit(): bool
    {
        //  Создаём локальный репозиторий и отключаем в нём автоконвертирование строк под Windows
        self::$out = self::localcmd("git init", true);
        if (substr(PHP_OS, 0, 3) == "WIN")
            self::localcmd("git config --global core.autocrlf false");
        
        //  Возвращаем результат операции
        return !(mb_stripos(self::$out, "Initialized empty Git repository") === false);
    }
    
    /**
     *  Получение данных из удалённого репозитория без замены текущих файлов
     *
     * @param string $remotename   [optional] Строка, содержащая название удалённого репозитория
     * @param string $remotebranch [optional] Строка, содержащая название ветки в удалённом репозитории
     *
     * @return bool Возвращаем результат операции
     */
    static function gitFetch($remotename = "", $remotebranch = ""): bool
    {
        //  Обработка параметров, переданных в массиве
        if (is_array($remotename)) {
            if (trim($remotename['remotebranch']) != "")
                $remotebranch = $remotename['remotebranch'];
            if (trim($remotename['remotename']) != "")
                $remotename = $remotename['remotename'];
            else
                $remotename = "";
        }
        
        //  Выставляем значения из переменных класса, если они не были переданы в параметрах
        if (trim($remotename) == "")
            $remotename = self::$remotename;
        if (trim($remotebranch) == "")
            $remotebranch = self::$remotebranch;
        
        //  Очищаем название ветки, если в названии репозитория указана ревизия
        if (self::checkRevisionID($remotename))
            $remotebranch = "";
        
        //  Экранируем передаваемые в командной строке параметры
        if (trim($remotename) != "")
            $remotename = " ".escapeshellarg($remotename);
        if (trim($remotebranch) != "")
            $remotebranch = " ".escapeshellarg($remotebranch);
        
        //  Получаем данные репозитория, не обновляя локальные файлы
        self::$out = self::cmd("git fetch".$remotename.$remotebranch, true);
        
        //  Возвращаем результат операции
        return mb_stripos(self::$out, "fatal: ") === false;
    }
    
    /**
     *  Получение данных из удалённого репозитория с заменой локальных файлов
     *
     * @param string $remotename   [optional] Строка, содержащая название удалённого репозитория
     * @param string $remotebranch [optional] Строка, содержащая название ветки в удалённом репозитории
     *
     * @return bool Возвращаем результат операции
     */
    static function gitPull($remotename = "", $remotebranch = ""): bool
    {
        //  Обработка параметров, переданных в массиве
        if (is_array($remotename)) {
            if (trim($remotename['remotebranch']) != "")
                $remotebranch = $remotename['remotebranch'];
            if (trim($remotename['remotename']) != "")
                $remotename = $remotename['remotename'];
            else
                $remotename = "";
        }
        
        //  Выставляем значения из переменных класса, если они не были переданы в параметрах
        if (trim($remotename) == "")
            $remotename = self::$remotename;
        if (trim($remotebranch) == "")
            $remotebranch = self::$remotebranch;
        
        //  Очищаем название ветки, если в названии репозитория указана ревизия
        if (self::checkRevisionID($remotename))
            $remotebranch = "";
        
        //  Экранируем передаваемые в командной строке параметры
        if (trim($remotename) != "")
            $remotename = " ".escapeshellarg($remotename);
        if (trim($remotebranch) != "")
            $remotebranch = " ".escapeshellarg($remotebranch);
        
        //  Стягиваем данные репозитория, предварительно вычистив всякое говно, дополнительно удаляя untracked файлы, на случай, если такие же окажутся в
        //  стягиваемых данных
        self::localcmd("git reset --hard", true);
        $untracked = explode("\n", trim(self::localcmd("git ls-files --others --exclude-standard", true)));
        foreach ($untracked as $file)
            if (is_file(self::$localrepo."/{$file}"))
                unlink(self::$localrepo."/{$file}");
        self::$out = self::cmd("git pull".$remotename.$remotebranch, true);
        
        //  Возвращаем результат операции
        return mb_stripos(self::$out, "fatal: ") === false;
    }
    
    /**
     *  Переключение на ветку/коммит
     *
     * @param string $remotebranch [optional] Строка, содержащая название ветки в локальном/удалённом репозитории
     *
     * @return bool Возвращаем результат операции
     */
    static function gitCheckout($remotebranch = ""): bool
    {
        //  Обработка параметров, переданных в массиве
        if (is_array($remotebranch)) {
            if (trim($remotebranch['remotebranch']) != "")
                $remotebranch = $remotebranch['remotebranch'];
            else
                $remotebranch = "";
        }
        //  Выставляем значения из переменных класса, если они не были переданы в параметрах
        if (trim($remotebranch) == "")
            $remotebranch = self::$remotebranch;
        
        //  Экранируем передаваемые в командной строке параметры
        if (trim($remotebranch) != "")
            $remotebranch = " ".escapeshellarg($remotebranch);
        
        //  Переключаемся на удаленную ветку
        self::$out = self::localcmd("git checkout ".$remotebranch, true);
        
        //  Возвращаем результат операции
        return mb_stripos(self::$out, "error: ") === false;
    }
    
    /**
     *  Инициализация репозитория (прямой вызов из конструктора). Все параметры могут быть переданы одним массивом (ключ/значение), обязательный параметр
     *  только первый, для остальных должно быть предусмотренно значение по-умолчанию
     *
     * @param string $remoterepo   Полный адрес удалённого (от слова дальний) репозитория
     * @param string $remotename   [optional] Название удалённого репозитория
     * @param string $remotebranch [optional] Удалённая ветку репозитория
     * @param string $localrepo    [optional] Каталог, куда будет скачан репозиторий
     * @param string $remotekey    [optional] Путь к файлу ключа (если нужно работать по ssh)
     * @param string $log          [optional] Путь к файлу лога (если нужно логирование)
     *
     * @return bool|string строка с токеном или false, если авторизация не удалась
     */
    static function initRepo($remoterepo, $remotename = "origin", $remotebranch = "master", $localrepo = "./", $remotekey = "", $log = "")
    {
        //  Обработка параметров, переданных в массиве
        if (is_array($remoterepo)) {
            if (trim($remoterepo['remotename']) != "")
                $remotename = $remoterepo['remotename'];
            if (trim($remoterepo['remotebranch']) != "")
                $remotebranch = $remoterepo['remotebranch'];
            if (trim($remoterepo['localrepo']) != "")
                $localrepo = $remoterepo['localrepo'];
            if (trim($remoterepo['remotekey']) != "")
                $remotekey = $remoterepo['remotekey'];
            if (trim($remoterepo['log']) != "")
                $log = $remoterepo['log'];
            if (trim($remoterepo['remoterepo']) != "")
                $remoterepo = $remoterepo['remoterepo'];
            else
                $remoterepo = "";
        }
        
        //  Запоминаем переданные параметры в переменных класса (если они не были переданы)
        if (trim($remoterepo) != "")
            self::setRemoteRepo($remoterepo);
        if (trim($remotebranch) != "")
            self::setRemoteBranch($remotebranch);
        if (trim($remotekey) != "")
            self::setRemoteKey($remotekey);
        if (trim($remotename) != "")
            self::setRemoteName($remotename);
        if (trim($localrepo) != "")
            self::setLocalRepo($localrepo);
        
        //  Запоминаем название log файла и включаем логгирование, если логфайл был передан
        if (trim($log) != "") {
            self::setLog($log);
            self::setLogging(true);
        }
        
        //  Присваиваем классу уникальный идентификатор сессии ("токен") для отображения в логе
        self::$uniq = uniqid();
        
        //  Запоминаем, существует ли каталог локального репозитория 
        $local_repo_presents = is_dir(self::$localrepo."/.git");
        
        //  Если каталога локального репозитория нет, то возвращаем результат клонирования/создания репозитория, в зависимости от указания удалённого репо)
        if (!$local_repo_presents) {
            if (self::$remoterepo != "")
                return self::gitClone();
            else
                return self::gitInit();
        }
        
        return true;
    }
    
    /**
     *  Получение идентификатора последней ревизии
     *
     * @param string $remotebranch [optional] Строка, содержащая название ветки в удалённом репозитории
     *
     * @return bool Возвращаем результат операции
     */
    static function getLastRevision($remotebranch = ""): bool
    {
        //  Обработка параметров, переданных в массиве
        if (is_array($remotebranch)) {
            if (trim($remotebranch['remotebranch']) != "")
                $remotebranch = $remotebranch['remotebranch'];
            else
                $remotebranch = "";
        }
        
        //  Выставляем значения из переменных класса, если они не были переданы в параметрах
        if (trim($remotebranch) == "")
            $remotebranch = self::$remotebranch;
        
        //  Экранируем передаваемые в командной строке параметры
        if (trim($remotebranch) != "")
            $remotebranch = " ".escapeshellarg($remotebranch);
        
        //  Обновляем информацию из удалённого репозитория
        self::gitFetch();
        
        //  Получаем номер последней ревизии из репозитория и возвращаем его
        self::$out = self::localcmd("git rev-parse".$remotebranch, false);
        return self::$out;
    }
    
    /**
     *  Проверяет идентификатор ревизии на соответствие формату идентификаторов git
     *
     * @param string $id Строка, содержащая идентификатор ревизии
     *
     * @return bool Возвращаем результат операции
     */
    static function checkRevisionID($id): bool
    {
        //  Обработка параметров, переданных в массиве
        if (is_array($id)) {
            if (trim($id['id']) != "")
                $id = $id['id'];
            else
                $id = "";
        }
        
        //  Возвращаем результат проверки переданного идентификатора (номер идентификатора состоит из 40 символов в нижнем регистре и цифр)
        return (preg_match("/^[a-z0-9]{40}$/", $id));
    }
    
    /**
     *  Получение списка файлов в указанной ревизии (или по имени/ветке) (только изменённые/добавленные в ней)
     *
     * @param string $remotebranch Строка, содержащая название ветки в удалённом репозитории
     *
     * @return bool Возвращаем результат операции
     */
    static function getRevisionFileList($remotebranch): bool
    {
        //  Обработка параметров, переданных в массиве
        if (is_array($remotebranch)) {
            if (trim($remotebranch['remotebranch']) != "")
                $remotebranch = $remotebranch['remotebranch'];
            else
                $remotebranch = "";
        }
        
        //  Экранируем передаваемые в командной строке параметры
        if (trim($remotebranch) != "")
            $remotebranch = " ".escapeshellarg($remotebranch);
        
        //  Обновляем информацию из удалённого репозитория
        self::gitFetch();
        
        //  Получаем список изменённых файлов из последней ревизии
        self::$out = self::localcmd("git show --pretty=format:\"\" --name-only".$remotebranch, false);
        
        //  Возвращаем false, если в логе обнаружена ошибка
        if (mb_stripos(self::$out, "fatal: ") !== false || mb_stripos(self::$out, "usage: ") !== false)
            return false;
        
        //  Возвращаем скомпанованные данные, разложенные построчно, без пустых значений
        return array_values(array_diff(explode("\n", trim(self::$out)), array("")));
    }
    
    /**
     *  Получение списка файлов в указанной ревизии (или по имени/ветке) (полный список в репозитории, а не только изменённые в ней)
     *
     * @param string $remotebranch Строка, содержащая название ветки в удалённом репозитории
     *
     * @return bool Возвращаем результат операции
     */
    static function getRevisionFullFileList($remotebranch): bool
    {
        //  Обработка параметров, переданных в массиве
        if (is_array($remotebranch)) {
            if (trim($remotebranch['remotebranch']) != "")
                $remotebranch = $remotebranch['remotebranch'];
            else
                $remotebranch = "";
        }
        
        //  Экранируем передаваемые в командной строке параметры
        if (trim($remotebranch) != "")
            $remotebranch = " ".escapeshellarg($remotebranch);
        
        //  Обновляем информацию из удалённого репозитория
        self::gitFetch();
        
        //  Получаем список файлов из последней ревизии, отсекая каталоги (у них в конце /)
        self::$out = self::localcmd("git ls-tree --name-only -r".$remotebranch, false);
        
        //  Возвращаем false, если в логе обнаружена ошибка
        if (mb_stripos(self::$out, "fatal: ") !== false || mb_stripos(self::$out, "usage: ") !== false)
            return false;
        
        //  Возвращаем скомпанованные данные, разложенные построчно, без пустых значений
        return array_values(array_diff(explode("\n", trim(self::$out)), array("")));
    }
    
    /**
     *  Получение информации о всех ревизиях указанного файла
     *
     * @param string $file         Строка, содержащая полный путь файла в репозитории
     * @param string $remotebranch [optional] Строка, содержащая название ветки в удалённом репозитории
     *
     * @return bool|array формат возвращаемого массива:
     *  Array
     *  (
     *      [0] => Array
     *          (
     *              [revision] => de4d8e8fc0e87faefbe3d85f0fbd228165c1c726
     *              [rollback] => daae42eceb466dfcae8a5f8c8e0fc00a8a6ea0c3
     *              [author] => Vyacheslav Sitnikov
     *              [date] => 2016-09-23 19:13:32 +0300
     *              [msg] => Дабавление фалов от Алекса
     *              [uid] => b50a01efac22c6cb867ad27766e98f5d1b1f2814
     *              [fileid] => 1ab552252729179d8d142d2cdaae85dddd075e0f
     *              [rollback_author] => Dmitriy Trunov
     *              [rollback_date] => 2016-09-14 15:16:41 +0300
     *              [rollback_msg] => GFBPTST-7346:ВР для мобильной коммерции
     *              [revisionlist] => Array
     *                  (
     *                      [0] => de4d8e8fc0e87faefbe3d85f0fbd228165c1c726
     *                      [1] => daae42eceb466dfcae8a5f8c8e0fc00a8a6ea0c3
     *                      [2] => 4fc38829077edbfe6ae8969e3049847bdd1ee4ee
     *                      [3] => 8e1845f717f26ec05539a9b7e718b7e9a95ea641
     *                  )
     *          )
     *      [1] => Array
     *          (
     *              [revision] => daae42eceb466dfcae8a5f8c8e0fc00a8a6ea0c3
     *              [rollback] => 4fc38829077edbfe6ae8969e3049847bdd1ee4ee
     *              [author] => Dmitriy Trunov
     *              [date] => 2016-09-14 15:16:41 +0300
     *              [msg] => GFBPTST-7346:ВР для мобильной коммерции
     *              [uid] => b704714d9d1d5f9726838eb524f6c2cb613e175c
     *              [fileid] => 1ab552252729179d8d142d2cdaae85dddd075e0f
     *              [rollback_author] => Vyacheslav Sitnikov
     *              [rollback_date] => 2016-08-25 13:43:47 +0300
     *              [rollback_msg] => Версия для компиляции. Исключён каталог GF$CMSAREG_EVENT_TRG, содержащий файл gf$cmsareg_event_cis_trg.sql
     *              [revisionlist] => Array
     *                  (
     *                      [0] => de4d8e8fc0e87faefbe3d85f0fbd228165c1c726
     *                      [1] => daae42eceb466dfcae8a5f8c8e0fc00a8a6ea0c3
     *                      [2] => 4fc38829077edbfe6ae8969e3049847bdd1ee4ee
     *                      [3] => 8e1845f717f26ec05539a9b7e718b7e9a95ea641
     *                  )
     *          )
     *  )
     */
    static function getRevisionsFile($file, $remotebranch = "")
    {
        //  Обработка параметров, переданных в массиве
        if (is_array($file)) {
            if (trim($file['remotebranch']) != "")
                $remotebranch = $file['remotebranch'];
            if (trim($file['file']) != "")
                $file = $file['file'];
            else
                $file = "";
        }
        
        //  Выставляем значения из переменных класса, если они не были переданы в параметрах
        if (trim($remotebranch) == "")
            $remotebranch = self::$remotebranch;
        
        //  Экранируем передаваемые в командной строке параметры
        if (trim($remotebranch) != "")
            $remotebranch = " ".escapeshellarg($remotebranch);
        
        //  Получаем информацию о всех коммитах файла из удалённого репозитория
        self::$out = self::localcmd("git log".$remotebranch." --all --pretty=format:\"%H|%an|%ci|%s\" ".escapeshellarg($file), false);
        
        //  Возвращаем false, если в логе обнаружена ошибка
        if (mb_stripos(self::$out, "fatal: ") !== false)
            return false;
        
        $return_data = array();
        $revision_list = array();
        $out = explode("\n", self::$out);
        for ($i = 0; $i < sizeof($out); $i++) {
            
            //  Парсим строку (d9b13f002fd7059e5e799fec3d7740ea4235e15d|Vyacheslav Sitnikov|2017-08-01 17:26:39 +0300|Проект в IDEA), получая:
            //  id текущей ревизии, её автора, дату и сообщение
            preg_match("/(^[^\|]+)\|([^\|]+)\|([^\|]+)\|(.*$)/", $out[$i], $tmp2);
            $revision_id = $tmp2[1];
            $revision_list[] = $revision_id;
            $return_data[$i]['revision'] = $revision_id;
            $return_data[$i]['author'] = $tmp2[2];
            $return_data[$i]['date'] = $tmp2[3];
            $return_data[$i]['msg'] = $tmp2[4];
            
            //  Генерируем uid (хэш ревизии и названия файла) и fileid (хэш только названия файла)
            $return_data[$i]['uid'] = sha1($revision_id.$file);
            $return_data[$i]['fileid'] = sha1($file);
            
            //  Инициализируем id ревизии отката (чтобы было, т.к. JS не любит когда в массиве нет полей)
            $return_data[$i]['rollback'] = "";
            
            //  Если это не самая свежая ревизия, запоминаем её id, автора, дату и сообщение для ревизии отката
            if ($i) {
                $return_data[$i - 1]['rollback'] = $revision_id;
                $return_data[$i - 1]['rollback_author'] = $tmp2[2];
                $return_data[$i - 1]['rollback_date'] = $tmp2[3];
                $return_data[$i - 1]['rollback_msg'] = $tmp2[4];
            }
        }
        
        //  Добавляем в полный список ревизий в информацию о каждой ревизии
        for ($i = 0; $i < sizeof($return_data); $i++)
            $return_data[$i]['revisionlist'] = $revision_list;
        
        return $return_data;
    }
    
    /**
     *  Получение информации о последней ревизии указанного файла
     *
     * @param string $file         Строка, содержащая полный путь файла в репозитории
     * @param string $remotebranch [optional] Строка, содержащая название ветки в удалённом репозитории
     *
     * @return bool|array формат возвращаемого массива:
     *  Array
     *  (
     *      [revision] => de4d8e8fc0e87faefbe3d85f0fbd228165c1c726
     *      [rollback] => daae42eceb466dfcae8a5f8c8e0fc00a8a6ea0c3
     *      [author] => Vyacheslav Sitnikov
     *      [date] => 2016-09-23 19:13:32 +0300
     *      [msg] => Дабавление фалов от Алекса
     *      [uid] => b50a01efac22c6cb867ad27766e98f5d1b1f2814
     *      [fileid] => 1ab552252729179d8d142d2cdaae85dddd075e0f
     *      [rollback_author] => Dmitriy Trunov
     *      [rollback_date] => 2016-09-14 15:16:41 +0300
     *      [rollback_msg] => GFBPTST-7346:ВР для мобильной коммерции
     *      [revisionlist] => Array
     *          (
     *              [0] => de4d8e8fc0e87faefbe3d85f0fbd228165c1c726
     *              [1] => daae42eceb466dfcae8a5f8c8e0fc00a8a6ea0c3
     *              [2] => 4fc38829077edbfe6ae8969e3049847bdd1ee4ee
     *              [3] => 8e1845f717f26ec05539a9b7e718b7e9a95ea641
     *          )
     *  )
     */
    static function getLastRevisionFile($file, $remotebranch = "")
    {
        //  Получаем данные о всех ревизиях файла
        $data = self::getRevisionsFile($file, $remotebranch);
        
        //  Возвращаем данные о самой свежей ревизии или false в случае ошибки
        return $data === false ? $data : $data[0];
        
    }
    
    /**
     *  Получение содержимого файла нужной ревизии из репозитория
     *
     * @param string $file         Строка, содержащая полный путь файла в репозитории
     * @param string $remotebranch [optional] Строка, содержащая название ветки в удалённом репозитории (обязательный параметр)
     * @param string $save_file    [optional] Строка, содержащая полный путь файла, куда записать данные, в файловой системе. При отсутствии содержимое файла
     *                             не записывается, а возвращается
     *
     * @return string|bool Результат операции, при указании файла для записи или (string) строка с содержимым указанного файла
     */
    static function getFile($file, $remotebranch = "", $save_file = "")
    {
        //  Обработка параметров, переданных в массиве
        if (is_array($file)) {
            if (trim($file['remotebranch']) != "")
                $remotebranch = $file['remotebranch'];
            if (trim($file['save_file']) != "")
                $save_file = $file['save_file'];
            if (trim($file['file']) != "")
                $file = $file['file'];
            else
                $file = "";
        }
        
        //  Возвращаем ошибку, если название ветки не передано
        if (trim($remotebranch) == "")
            return false;
        
        //  Экранируем передаваемые в командной строке параметры
        $remotebranch = escapeshellarg($remotebranch);
        if (trim($file) != "")
            $file = escapeshellarg($file);
        
        //  Обновляем информацию из удалённого репозитория
        self::gitFetch();
        
        //  Генерируем имя временного файла
        $tempname = tempnam("", "cmpl");
        
        //  Считываем содержимое файла в указанной ревизии
        self::$out = self::localcmd("git show ".$remotebranch.":".$file." > ".escapeshellarg($tempname), false);
        
        //  Удаляем временный файл, если операция завершилась с ошибкой, и возвращаем false
        if (mb_stripos(self::$out, "fatal: ") !== false) {
            unlink($tempname);
            return false;
        }
        
        //  Переносим файл в место для сохранения, если оно было указано, и сохраняем флаг успешности операции в переменной возврата
        if (trim($save_file) != "") {
            @rename($tempname, $save_file);
            $return = true;
        }
        //  Или сохраняем содержимое файла в переменной возврата, если место для сохранения не было указано
        else
            $return = @file_get_contents($tempname);
        
        //  Удаляем временный файл и возвращаем результат
        @unlink($tempname);
        return $return;
    }
}
