<?php
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

namespace PSData;

use \PSData\Api as api;

require_once(PSDATA_LIBRARY_DIR."/lib_str.php");

/**
 * Класс авторизации по LDAP
 *
 * property string $token    Название токена<br>
 * property array  $user     Информация по пользователю, полученная из LDAP<br>
 * property int    $lifetime Время жизни нового токена по-умолчанию (в секундах)<br>
 * <br>
 *
 * method array|bool  getInfo    Получение информации по пользователю (под логином и паролем пользователя или, при NTLM авторизации, под служебным логином)<br>
 * method string|bool newToken   Выдача нового токена с авторизацией или по старому токену или по логину и паролю<br>
 * method bool        checkToken Проверка токена (а также продление его времени жизни)<br>
 * <br>
 *
 * Формат токена:<br>
 *  - контейнер<br>
 *    array(<br>
 *      "type"      => authtoken<br>
 *      "expire"    => таймштамп времени (в GMT), когда токен истечёт<br>
 *      "lifetime"  => время жизни (продления) токена в секундах<br>
 *      "token"     => сам токен<br>
 *    )<br>
 *
 * @author    Vyacheslav Sitnikov <vyacheslav.sitnikov@megafon.ru>
 * @version   1.0
 * @package   MegaFon\PSData
 * @copyright Copyright (c) 2017 ПАО "МегаФон"
 */
class Auth
{
    /**
     * @var string  Название токена
     */
    public $token = "";

    /**
     * @var array Информация по пользователю, полученная из LDAP
     */
    public $user = array();

    /**
     * @var int Время жизни токена по-умолчанию (в секундах)
     */
    public $lifetime = 28800;

    /**
     * @var resource Ресурс хранилища
     */
    private $vault = null;                    //  Ресурс хранилища

    /**
     *  Инициализация класса
     *
     * @param string $login    (optional) Строка, содержащая выданный токен или логин пользователя в AD
     * @param string $password (optional) Строка, содержащая пароль пользователя в AD
     *
     * return string|bool В случае успешности проверки - true, или массив, содержащий поле принадлежности классу
     */
    function __construct($login = "", $password = "")
    {
        $token = "";

        //  Обработка случая, когда первый параметр - массив
        if (is_array($login)) {
            if (trim($login['password']) != "")
                $password = $login['password'];
            if (trim($login['token']) != "")
                $token = $login['token'];
            if (trim($login['login']) != "")
                $login = $login['login'];
            else
                $login = "";
        }

        if ($token == "")
            $token = $login;

        //  Выходим, в случае отсутствия класса Vault
        class_exists('\PSData\Vault') or api::out("Отсутствует класс Vault", 500);
        $this->vault = new Vault();
        $this->vault->setSubDirectory("tokens");

        //  Возвращаем успех, в случае успешной проверки токена либо результат создания нового токена
        return $this->checkToken($token) || $this->newToken($login, $password);
    }

    /**
     *  Проверка токена (а также продление его времени жизни)
     *
     * @param string $token (optional) Строка, содержащая токен
     *
     * @return bool Результат проверки/продления
     */
    function checkToken($token = "")
    {
        if (defined(GodMode))
            return true;

        //  Возвращаем неудачу, если указанного токена нет
        if (!($check = $this->vault->isVaultKey(md5($token)))) {
            api::log($token." Token file not found");
            return false;
        }

        //  Возвращаем неудачу, если на выходе не получили контейнер с типом токена
        $token_decrypt = $this->vault->decryptFromVault(md5($token));
        // Добавил MVYS для логирования 
      //  api::log($token_decrypt . " - DEBUG"); 
        
        if (!is_array($dat = json_decode($token_decrypt, true)) || $dat['type'] != "authtoken") {
            api::log($token." Token incorrect data: {$token_decrypt}");
            return false;
        }

        //  Запоминаем данные по пользователю из токена, если данных по пользователю нет, но они есть в данных токена
        if (!sizeof($this->user) && sizeof($dat['token']['userdata']))
            $this->user = $dat['token']['userdata'];

        //  Возвращаем неудачу, если срок токена указан не в виде таймштампа или истёк (предварительно удалив его из хранилища)
        if (!is_numeric($dat['expire']) || $dat['expire'] < gmdate("U")) {
            $this->vault->DeleteVaultKey(md5($token));
            api::log($token." Token expiried");
            return false;
        }

        //  Устанавливаем время жизни токена в дефолтовое значение, если оно в нём не указано
        if (!is_numeric($dat['lifetime']))
            $dat['lifetime'] = $this->lifetime;
        
        //  Продляем жизнь токена
        $dat['expire'] = gmdate("U") + $dat['lifetime'];

        //  Записываем его в хранилище
        $this->vault->cryptToVault(json_encode($dat, JSON_UNESCAPED_UNICODE), md5($token));
        $this->token = $token;
        return true;
    }

    /**
     *  Выдача нового токена с авторизацией или по старому токену или по логину и паролю
     *
     * @param string $username Строка, содержащая токен или логин пользователя в AD
     * @param string $password (optional) Строка, содержащая пароль пользователя в AD
     *
     * @return string|bool Строка с токеном или false, если авторизация не удалась
     */
    function newToken($username, $password = "")
    {
        // Возвращаем неудачу, если получить информацию по пользователю не удалось
        if (!($user = $this->getInfo($username, $password)))
            return false;

        $token = $this->generateTokenName();
        $dat = array(
            "type"     => "authtoken",
            "expire"   => gmdate("U") + $this->lifetime,
            "lifetime" => $this->lifetime,
            "token"    => array("username" => $username, "userdata" => $user, "rand" => $token),
        );

        //  Возвращаем неудачу, если сохранение данных в хранилище прошло неудачно
        if (!$this->vault->cryptToVault(json_encode($dat, JSON_UNESCAPED_UNICODE), md5($token)))
            return false;
        $this->user = $user;
        return $this->token = $dat['token']['rand'];
    }

    /**
     *  Получение информации по авторизованному пользователю (под логином и паролем пользователя или, при NTLM авторизации, под служебным логином)
     *
     * @param string $username (optional) Строка, содержащая выданный токен или логин пользователя в AD
     * @param string $password (optional) Строка, содержащая пароль пользователя в AD
     *
     * @return array|bool Массив, содержащий информацию по пользователю или false, если поиск прошёл неуспешно
     */
    function getInfo($username = "", $password = "")
    {
        //TODO: Это заглушка
        return false;

        $token_info = array();
        $token_username = null;

        //  Если передан логин пользователя и для него существует токен
        if ($check = $this->vault->isVaultKey(md5($username))) {

            //  Получаем по нему информацию и, если на выходе получили контейнер с типом токена авторизации, запоминаем логин пользователя из токена
            if (is_array($token_info = json_decode($this->vault->decryptFromVault(md5($username)), true)) && $token_info['type'] == "authtoken")
                $token_username = $token_info['token']['username'];
        }
        $ldap_username = $username;
        $ldap_password = $password;

        //  Если это пользователь, пришедший по NTLM авторизации или по токену
        if ($_SERVER['AUTH_TYPE'] == "NTLM" || $token_info['type'] == "authtoken") {
            $username = $_SERVER['AUTH_TYPE'] == "NTLM" ? $_SERVER['REMOTE_USER'] : $token_username;
            $vault = new Vault();

            //  Выдираем служебного пользователя, которым будем коннектиться к LDAP
            list($ldap_username, $ldap_password) = @array_values(json_decode($vault->decryptFromVault("psdata_ad_user"), true));
            if ($ldap_username == "")
                api::out("Отсутствует данные ТУЗ LDAP", 500);

        }
        $ldap_conn = @ldap_connect("127.0.0.1") or api::out("Не найден сервер LDAP авторизации", 500);

        //  Возвращаем false, если авторизация не удалась
        if (!@ldap_bind($ldap_conn, "MEGAFON\\".$ldap_username, $ldap_password))
            return false;

        //  Подготавливаем аккаунт для поиска, протокол, путь поиска
        $search_str = "(samaccountname=".$username.")";
        @ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        $ldapBase = "OU=IDM,DC=megafon,DC=ru";

        //  Возвращаем false, если пользователь не найден
        if (!($ldapSearch = @ldap_search($ldap_conn, $ldapBase, $search_str)))
            return false;
        $user_data = @ldap_get_entries($ldap_conn, $ldapSearch)[0];

        //  Возвращаем true, если пользователь и пароль верны, но пользователь не в IDM
        if (!is_array($user_data))
            return true;

        //  Рекурсивно проходимся по всему массиву, переделывая кодировку в UTF8, если необходимо
        array_walk_recursive($user_data, function (&$item) {
            if (strtolower($encode = DetectEncode($item)) != "utf-8")
                $item = @mb_convert_encoding($item, "utf-8", $encode);
        });

        //  Добавляем в группы пользователя его логин и почту (чтобы находились при поиске по группам)
        $user_data['memberof'][] = "CN=".$user_data['samaccountname'][0].",OU=MANUAL";
        $user_data['memberof'][] = "CN=".$user_data['mail'][0].",OU=MANUAL";
        $user_data['memberof']['count'] += 2;
        $user = array(
            "username" => $user_data['samaccountname'][0],
            "fn"       => $user_data['givenname'][0],
            "mn"       => $user_data['middlename'][0],
            "ln"       => $user_data['sn'][0],
            "job"      => $user_data['title'][0],
            "mail"     => $user_data['mail'][0],
            "mobile"   => $user_data['mobile'][0],
            "work"     => $user_data['pager'][0],
            "memberof" => $user_data['memberof'],
        );

        //  Возвращаем информацию
        return $user;
    }

    /**
     *  Генерация названия токена
     *
     * @return string Возвращает название токена
     */
    function generateTokenName()
    {
        $length = 32;

        // Генерируем случайный токен, используюя возможности PHP7
        $bytes = random_bytes($length / 2);
        $token = bin2hex($bytes);

        return $token;
    }

    /**
     *  Проверка разрешения на доступ текущего пользователя
     *
     * Параметры могут быть переданы как ключи массива, если первый параметр - массив
     *
     * @param string|array $allow (optional) Строка, содержащая списки групп/пользователей, которым доступ разрешён
     * @param string       $deny  (optional) Строка, содержащая списки групп/пользователей, которым доступ запрещён
     * @param string       $order (optional) Порядок применения списков
     *
     * @return bool результат проверки (или null если не подошёл ни один из вариантов)
     */
    function checkAccess($allow = 'none', $deny = 'none', $order = 'deny, allow')
    {
        if (defined(GodMode))
            return true;

        //  Обработка ситуации, когда параметры переданы в массиве
        if (is_array($allow)) {
            if (trim($allow['deny']) != "")
                $deny = $allow['deny'];
            if (trim($allow['order']) != "")
                $order = $allow['order'];
            if (trim($allow['allow']) != "")
                $allow = $allow['allow'];
            else
                $allow = "none";
        }

        //  Получаем список order в указанном порядке, убирая лишние пробелы и переводя всё в нижний регистр
        $order = array_values(array_map('strtolower', array_map('trim', explode(',', $order))));

        //  Изначально мы не можем сказать, разрешить пользователю доступ или нет
        $passed = null;
        foreach ($order as $value) {

            //  В зависимости от пункта сортировке выставляем ссылку на одноимённый параметр ($allow или $deny)
            if ($value == "deny")
                $list_of_rules = &$deny;
            else
                $list_of_rules = &$allow;

            //  Выставляем запрет или разрешение на доступ в зависимости от списка, если этот список влияет на всё
            if (strtolower($list_of_rules) == 'all') {
                $passed = ($value == 'allow');
                continue;
            }

            //  Переходим на следующий список, если список не влияет ни на что
            elseif (strtolower($list_of_rules) == 'none')
                continue;

            //  Разделяем данные списка по массиву, убирая лишние пробелы и переводя всё в нижний регистр
            $access_variants = array_values(array_map('strtolower', array_map('trim', explode(',', $list_of_rules))));
            foreach ($access_variants as $variant) {

                //  Перебираем группы текущего пользователя (из сессии)
                for ($i = 0; $i < $_SESSION['psdatauthdata']['memberof']['count']; $i++) {

                    //  Если в какой-то из групп произошло совпадение
                    if (strpos(strtolower($_SESSION['psdatauthdata']['memberof'][$i]), "cn=".$variant.",") !== false) {

                        //  Выставляем запрет или разрешение на доступ в зависимости от списка
                        $passed = ($value == 'allow');
                        break 2;
                    }
                }
            }
        }

        //  Возврашаем результат проверки доступа
        return $passed;
    }
}
