<?php
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

//  Указываем пространство имён
namespace PSData;

/**
 * Class transport_http
 *
 * @package MegaFon\PSData
 */
class TransportHTTP
{
    public $protocol = "https";
    public $host = "localhost";
    public $path = "/";
    public $address = "localhost";
    public $connect_timeout = 10;
    public $timeout = 600;
    public $headers = [];
    public $return_headers = true;
    public $referer = null;
    public $follow = true;
    public $max_redirect = 10;
    public $useragent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:52.0) Gecko/20100101";
    public $http_method = CURLOPT_HTTPGET;
    
    private $curl;
    
    /**
     *  Конструктор. Описание параметров в методе parse_args
     *
     * @param $args
     */
    function __construct($args)
    {
        $this->parse_args($args);
        $this->cookie_file = tempnam(sys_get_temp_dir(), 'curl');
    }
    
    /**
     *  Деструктор
     */
    function __destruct()
    {
        // TODO: Implement __destruct() method.
        curl_close($this->curl);
        @unlink($this->cookie_file);
        @unlink($this->cookie_file."jo");
    }
    
    /**
     *  Метод HTTP запроса
     *
     * @param array $args Передаваемые KV параметры:
     *                    string  address            Адрес, по которому обращается запрос
     *                    int     connect_timeout    [10] Таймаут на соединения в секундах
     *                    int     timeout            [600] Таймаут на получение данных в секундах
     *                    array   headers            Массив значения которого будут переданы как заголовке при отправке данных
     *                    boolean return_headers     [true] Возвращать ли принятые заголовки
     *                    string  referer            Если указан, будет выставлена реферальная ссылка
     *                    boolean follow             [true] Переходить ли по автопереходам (например по 302), количество переходов устанавливается в maxredir
     *                    int     maxredir           [10] Максимальное количество автопереходов по follow параметру
     *                    string  cookie             Выставление необходимых кук в формате GET ("a=1&b=2&c=3") или в родном для HTTP ("a=1; b=2; c=3")
     *                    string  useragent          ["internal"] Строка useragent'а
     *                    array   post               Массив, значения которого будет переданы в POST параметре
     *                    int     http_method        Метод запроса
     *                    string  custom_http_method Собственный метод запроса
     *
     *
     */
    function parse_args($args)
    {
        //  Начинаем перебирать полученные параметры, переделывая булевые стринги в реальные булевые параметры 
        if (is_array($args))
            foreach ($args as $key => $value)
                if (@strtolower(trim($value)) == "true")
                    $args[$key] = true;
                else if (@strtolower(trim($value)) == "false")
                    $args[$key] = false;
        
        //  Обрабатываем адрес
        if (!is_null($args['address'])) {
            if (preg_match("/(https*):\/\/([^\/]+)(.*)/", $args['address'], $parse_address))
                list(, $this->protocol, $this->host, $this->path) = $parse_address;
            else
                $this->path = $args['address'];
        }
        
        //  Обрабатываем параметр таймаута соединения
        if (is_numeric($args['connect_timeout']))
            $this->connect_timeout = $args['connect_timeout'];
        
        //  Обрабатываем параметр таймаута получения данных
        if (is_numeric($args['timeout']))
            $this->timeout = $args['timeout'];
        
        //  Обрабатываем параметр передаваемых заголовков
        if (isset($args['headers']))
            $this->headers = $args['headers'];
        
        //  Обрабатываем параметр возврата заголовков
        if (isset($args['return_headers']))
            $this->return_headers = $args['return_headers'];
        
        //  Обрабатываем параметр автоперехода
        if (isset($args['follow']))
            $this->follow = $args['follow'];
        
        //  Обрабатываем параметр количества автоперехода
        if (is_numeric($args['maxredir']))
            $this->max_redirect = $args['maxredir'];
        
        //  Обрабатываем параметр useragent
        if (!isset($args['useragent']))
            $args['useragent'] = $this->useragent;
        //  Обрабатываем параметр метода
        if (isset($args['http_method']))
            $this->http_method = $args['http_method'];
        
    }
    
    /**
     *  Функция обработки POST запроса
     *
     * @param array $args    Описание параметра в методе parse_args, в параметре модифицируется только элемент post, затем вызывается стандартный метод query
     * @param null  $address [optional] Адрес, по которому шлётся запрос
     * @param bool  $binary  [optional] Передаются ли в POST запросе двоичные данные. Это влияет на фомирование POST переменных
     *
     * @return array
     */
    function post($args, $address = null, $binary = false)
    {
        if (!isset($args['post'])) {
            $post = $args;
            $args = [];
        }
        else {
            $post = $args['post'];
            $args['post'] = "";
        }
        if (!is_null($address))
            $args['address'] = $address;
        if (!$binary && is_array($post)) {
            foreach ($post as $key => $value)
                $args['post'] .= $key."=".urlencode($value)."&";
            $args['post'] = substr($args['post'], 0, -1);
        }
        elseif ($binary && !is_array($post)) {
            $post = explode("&", substr($post, 0, 1) == "?" ? substr($post, 1) : $post);
            foreach ($post as $key => $value)
                $args['post'][$key] = $value;
        }
        return $this->query($args);
    }
    
    /**
     * @param array|null $args Смотри описание метода parse_args
     *
     * @return array Выходной массив:
     *               body           => Содержимое страницы
     *               headers        => Заголовоки ответа
     *               headers_list   => Массив всех полученних заголовков с учётом переходов
     *               headers_output => Передаваемые заголовоки
     *               request_info   => Информация о последней операции
     *               error          => Описание последней ошибки текущего сеанса
     */
    function query($args = null)
    {
        //  Обрабатываем переданые параметры
        if (!is_null($args))
            $this->parse_args($args);
        
        //  Инициализируем curl, если не сделали это раньше
        if (!is_resource($this->curl))
            $this->curl = curl_init();
        
        //  Возвращать полученные данные в оригинале
        curl_setopt($this->curl, CURLOPT_BINARYTRANSFER, true);
        
        //  Возвращать полученные данные в переменную
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        
        //  Проверять сертификат не будем
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        
        //  Включаем заголовки в вывод, если необходимо (потом их вырежем оттуда)
        curl_setopt($this->curl, CURLOPT_HEADER, $this->return_headers);
        
        //  Устанавливаем таймаут на переданный в параметре timeout
        curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT, $this->connect_timeout);
        curl_setopt($this->curl, CURLOPT_TIMEOUT, $this->timeout);
        
        //  Устанавливаем referrer, если нужно
        if (!is_null($this->referer)) {
            curl_setopt($this->curl, CURLOPT_REFERER, $this->referer);
            curl_setopt($this->curl, CURLOPT_AUTOREFERER, true);
        }
        
        //  Устанавливаем параметр автоматического следования по переходам
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, $this->follow);
        
        //  Устанавливаем параметр автоматического следования по переходам
        if ($this->follow) {
            curl_setopt($this->curl, CURLOPT_MAXREDIRS, $this->max_redirect);
            curl_setopt($this->curl, CURLOPT_UNRESTRICTED_AUTH, true);
        }
        
        //  Устанавливаем cookie, если нужно
        $args['cookie'] .= "; ".session_name()."=".session_id();
        if (isset($args['cookie'])) {
            
            //  Переделываем формат переданных кук из GET в разделённый точками с запятой
            $args['cookie'] = implode("; ", explode("&", $args['cookie']));
            
            //  Генерируем случайный файл для кук, сохраняем в него текущие куки
            file_put_contents($this->cookie_file, $args['cookie']);
            curl_setopt($this->curl, CURLOPT_COOKIEFILE, $this->cookie_file);
            curl_setopt($this->curl, CURLOPT_COOKIEJAR, $this->cookie_file."jo");
        }
        
        //  Устанавливаем useragent
        curl_setopt($this->curl, CURLOPT_USERAGENT, $this->useragent);
        
        //  Обработка POST данных, если нужно
        if ((is_array($args['post']) && sizeof($args['post']) > 0) || (!is_array($args['post']) && strlen($args['post']) > 0)) {
            
            //  Добавляем временный токен к POST данным, если он передан в запросе
            if (is_array($args['post']) && $GLOBALS['pdata']['authtoken'] != "")
                $args['post']['authtoken'] = $GLOBALS['pdata']['authtoken'];
            elseif ($GLOBALS['pdata']['authtoken'] != "" && !preg_match("/[&?]authtoken=/", $args['post']))
                $args['post'] .= (strpos($args['post'], "?") === false ? "?" : "&")."authtoken=".$GLOBALS['pdata']['authtoken'];
            
            //  Указываем, что будет POST запрос и заносим данные для него
            curl_setopt($this->curl, CURLOPT_POST, true);
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $args['post']);
        }
        else {
            curl_setopt($this->curl, CURLOPT_POST, false);
            if ($GLOBALS['pdata']['authtoken'] != "")
                $this->path .= (strpos($this->path, "?") === false ? "?" : "&")."authtoken=".$GLOBALS['pdata']['authtoken'];
        }
        
        //  Выставляем http метод
        curl_setopt($this->curl, $this->http_method, true);
        
        //  Указываем кастомный HTTP запрос, если он указан
        if (isset($args['custom_http_method']))
            curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, $args['custom_http_method']);
        
        //  Будем сохранять отправляемые заголовки
        curl_setopt($this->curl, CURLINFO_HEADER_OUT, true);
        
        //  Устанавливаем адрес
        $this->address = $this->protocol."://".$this->host.$this->path;
        curl_setopt($this->curl, CURLOPT_URL, $this->address);
        
        //  Добавляем поля к заголовку, если они указаны
        if (is_array($this->headers) && sizeof($this->headers))
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->headers);

//    if ($args['auth']!="") {      //  Если указана авторизация
//      curl_setopt($this->curl,CURLOPT_HTTPAUTH,$args['authtype']?$args['authtype']:CURLAUTH_BASIC);
//                                            //  Указываем, что нужно авторизоваться
//      curl_setopt($this->curl,CURLOPT_USERPWD,$args['auth']);
//                                            //  Указываем куку
//    }
        
        //  Выполняем запрос и получаем результат
        $output = curl_exec($this->curl);
        $header = "";
        $header_list = [];
        
        //  Если был выставлен параметр передавать заголовки в теле ответа
        if ($this->return_headers) {
            
            //  Крутим цикл до тех пор, пока в начале тела находится нечто, напоминающее HTTP заголовок
            do {
                //  Находим разделитель заголовка от тела (двойной перенос строк)
                $tmp = strpos($output, "\r\n\r\n");
                
                //  Вырезаем заголовок и тело
                $header = trim(substr($output, 0, $tmp));
                $header_list[] = $header;
                $output = substr($output, $tmp + 4, strlen($output));
            } while (preg_match("/^HTTP\/[\d\.]+ \d+ \w+/", $output));
        }
        
        //  Запоминаем переданные параметры заголовка
        $request_info = curl_getinfo($this->curl);
        $headers_output = explode("\r\n", trim($request_info['request_header']));
        
        //  Постобработка, полученный в последней отправки host выставляем как реальный хост для последующих запросов
        foreach ($headers_output as $value) {
            if (preg_match("/host:(.*)/i", $value, $parse_param))
                $this->host = trim($parse_param[1]);
        }
        
        //  Возвращаем полученные данные
        return array(
            "body"           => $output,
            "headers"        => $header,
            "headers_list"   => $header_list,
            "headers_output" => implode("\n", $headers_output),
            "request_info"   => $request_info,
            "error"          => curl_error($this->curl),
        );
    }
}
