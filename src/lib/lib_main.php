<?php

if (!function_exists('ftok')) {
    /**
     *  Эмуляция функции ftok для windows
     *
     * @param string $filePath  Каталог, который нужно удалить
     * @param string $projectId Идентификатор проекта. Должен состоять из одного символа
     *
     * @return int Идентификатор в памяти
     */
    function ftok($filePath, $projectId)
    {
        $fileStats = @stat($filePath);
        if (!$fileStats) {
            return -1;
        }
        
        return sprintf('%u', ($fileStats['ino'] & 0xffff) | (($fileStats['dev'] & 0xff) << 16) | ((ord($projectId) & 0xff) << 24));
    }
}

/**
 *  Запуск команды в фоне
 *
 * @param string $cmd Команда, которую необходимо выполнить
 */
function exec_nowait($cmd) {
  if (substr(php_uname(), 0, 7) == "Windows"){
    pclose(popen('start "" /B '.$cmd, "r")); 
  } else { //*nix
    exec($cmd . " > /dev/null &");  
  } 
}

/**
 *  Полное удаление каталога
 *
 * @param string $dirPath Каталог, который нужно удалить
 */
function deleteDir($dirPath)
{
    if (!is_dir($dirPath))
        trigger_error("deleteDir({$dirPath}): No such file or directory", E_USER_WARNING);
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/')
        $dirPath .= '/';
    $files = glob($dirPath.'*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file))
            deleteDir($file);
        else
            unlink($file);
    }
    @rmdir($dirPath);
}

/**
 *  Рекурсивное копирование каталога
 *
 * @param string $src Каталог, который нужно скопировать
 * @param string $dst Каталог, куда нужно скопировать
 * @param bool $clean [optional] Нужно ли перед копированием очищать каталог
 */
function copyDir($src, $dst, $clean = false)
{
    if (!is_dir($src))
        trigger_error("copyDir({$src}): No such file or directory", E_USER_WARNING);
    if (file_exists($dst) && $clean)
        deleteDir($dst);
    if (is_dir($src)) {
        @mkdir($dst, 0775, true);
        $files = array_values(array_diff(scandir($src), array('.', '..')));
        foreach ($files as $file)
            copyDir($src."/".$file, $dst."/".$file);
    }
    else if (file_exists($src))
        copy($src, $dst);
}

/**
 *  Функция переиеновывает ключи в массиве с сохранением порядка
 *
 * @param array  $array   Массив, в котором переименовываются ключи
 * @param string $old_key Старое название ключа
 * @param string $new_key Новое название ключа
 *
 * @return array Массив с переименованым ключём
 */
function replace_key($array, $old_key, $new_key): array
{
    $keys = array_keys($array);
    if (false === $index = array_search($old_key, $keys))
        trigger_error("Key {$old_key} does not exit", E_USER_WARNING);
    $keys[$index] = $new_key;
    return array_combine($keys, array_values($array));
}
