<?php

namespace PSData;

define("PSDATA_DIR", realpath(__DIR__."/.."));
define("PSDATA_PROJECT_DIR", realpath(PSDATA_DIR."/../../../.."));
define("PSDATA_LIBRARY_DIR", PSDATA_DIR."/lib");
define("PSDATA_API_DIR", PSDATA_DIR."/lib");

//  Добавляем подкаталоги для мапинга в подкаталогах
$loader = require PSDATA_PROJECT_DIR."/vendor/autoload.php";
$loader->addPsr4('PSData\\',
    [
        PSDATA_DIR.'/Ansible',
        PSDATA_DIR.'/Api',
        PSDATA_DIR.'/Auth',
        PSDATA_DIR.'/Database',
        PSDATA_DIR.'/Facade',
        PSDATA_DIR.'/Factory',
        PSDATA_DIR.'/Vault',
        PSDATA_DIR.'/VCS',
    ]
);


//  Выходим, если у нас нет файла констант
file_exists(PSDATA_PROJECT_DIR."/psdata_constant.php") or die("Восстановите файл psdata_constant.php из каталога psdata/constants");

//  Подгружаем константы среды
require_once(PSDATA_PROJECT_DIR."/psdata_constant.php");

require_once PSDATA_LIBRARY_DIR."/lib_str.php";
require_once PSDATA_LIBRARY_DIR."/lib_main.php";

$logger_config = [
    'name'     => 'EPortal',
    'rotating' => [
        'level'                          => PSDATA_API_LOG_FILE,
        'filename'                       => PSDATA_API_LOG_LEVEL,
        'max_files'                      => 60,
        'ignore_empty_context_and_extra' => true,
    ],
];

Logger::configure($logger_config);


//// экземпляры Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface
//$providers = [];
//
//$authenticationManager = new AuthenticationProviderManager($providers);
//
//try {
//    $authenticatedToken = $authenticationManager
//        ->authenticate($unauthenticatedToken);
//} catch (AuthenticationException $exception) {
//    // аутентификация неудачна
//}
//
//
//
//


//echo "OK";
//exit;

//  Инициализируем модули, сохранённые при последнем сканировании
Api::apiInitialize();
