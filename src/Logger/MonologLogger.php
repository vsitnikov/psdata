<?php
namespace PSData\Logger;

use Psr\Log\LoggerInterface;
use Monolog\Logger;

use Monolog\Handler\HandlerInterface;
use Monolog\Formatter\FormatterInterface;

use PSData\Logger\ArrayHelper;

/**
 * Logger
 */
final class MonologLogger extends Logger implements LoggerInterface
{
    /**
     * @param  array  $config []
     *                          ['name']       string Logger name
     *                          ['file']       array  File handler
     *                          ['rotating']   array  Rotating File handler
     *                          ['slack']      array  Slack handler
     *                          ['elastic']    array  Elastic handler
     *                          ['email']      array  Email handler
     *                          ['phpconsole'] array  PhpConsole handler https://github.com/Seldaek/monolog/blob/master/src/Monolog/Handler/PHPConsoleHandler.php
     *                          ['chromephp']  array  ChromePHP handler https://github.com/Seldaek/monolog/blob/master/src/Monolog/Handler/ChromePHPHandler.php
     *                          ['firephp']    array  FirePHP https://github.com/Seldaek/monolog/blob/master/src/Monolog/Handler/FirePHPHandler.php
     * @return LoggerInterface
     */
    public static function factory(array $config)
    {
        $name = ArrayHelper::findStr('name', $config) ?? 'monolog';

        $logger = new MonologLogger($name);

        foreach ($config as $handler_type => $handler_config) {
            switch ($handler_type) {
                case 'file':
                    $logger->pushHandler(self::createFile($handler_config));
                    break;
                case 'rotating':
                    $logger->pushHandler(self::createRotatingFile($handler_config));
                    break;
                case 'slack':
                    $logger->pushHandler(self::createSlack($handler_config));
                    break;
                case 'elastic':
                    $logger->pushHandler(self::createElastic($handler_config));
                    break;
                case 'email':
                    $logger->pushHandler(self::createEmail($handler_config));
                    break;
                case 'phpconsole':
                    $logger->pushHandler(self::createPHPConsole($handler_config));
                    break;
                case 'chromephp':
                    $logger->pushHandler(self::createChromePHP($handler_config));
                    break;
                case 'firephp':
                    $logger->pushHandler(self::createFirePHP($handler_config));
                    break;
            }
        }

        return $logger;
    }

    /**
     * @param  array  $config []
     *                          ['filename']    string
     *                          ['level']       string
     *                          ['bubble']      bool
     *                          ['format']      array
     * @return HandlerInterface
     */
    private static function createFile(array $config)
    {
        $levels = Logger::getLevels();
        $filename = ArrayHelper::extractStr('filename', $config, null, true);
        $level    = $levels[ArrayHelper::extractStr('level', $config, 'DEBUG')];
        $bubble   = ArrayHelper::extractBool('bubble', $config, true);
        $format   = ArrayHelper::extractArray('format', $config, []);

        $handler = new \Monolog\Handler\StreamHandler($filename, $level, $bubble);

        if (!empty($format)) {
            $handler->setFormatter(self::createFormatter($format));
        }

        return $handler;
    }

    /**
     * @param  array  $config []
     *                          ['filename']    string
     *                          ['level']       string
     *                          ['bubble']      bool
     *                          ['format']      array
     *                          ['max_files']   int
     * @return HandlerInterface
     */
    private static function createRotatingFile(array $config)
    {
        $levels = Logger::getLevels();
        $filename  = ArrayHelper::extractStr('filename', $config, null, true);
        $level     = $levels[ArrayHelper::extractStr('level', $config, 'DEBUG')];
        $bubble    = ArrayHelper::extractBool('bubble', $config, true);
        $format    = ArrayHelper::extractArray('format', $config, []);
        $max_files = ArrayHelper::extractInt('max_files', $config, 0);

        $handler = new  \Monolog\Handler\RotatingFileHandler($filename, $max_files, $level, $bubble);

        if (!empty($format)) {
            $handler->setFormatter(self::createFormatter($format));
        }

        return $handler;
    }

    /**
     * @param  array  $config []
     *                          ['token']                     string
     *                          ['channel']                   string
     *                          ['username']                  string|null
     *                          ['level']                     string
     *                          ['bubble']                    bool
     *                          ['use_attachement']           bool
     *                          ['icon_emoji']                string|null
     *                          ['use_short_attachment']      bool
     *                          ['include_context_and_extra'] bool
     *                          ['exclude_fields']            array
     * @return HandlerInterface
     */
    private static function createSlack(array $config)
    {
        $levels = Logger::getLevels();
        $token                      = ArrayHelper::extractStr('token', $config, null, true);
        $channel                    = ArrayHelper::extractStr('channel', $config, null, true);
        $username                   = ArrayHelper::extractStr('username', $config);
        $level                      = $levels[ArrayHelper::extractStr('level', $config, 'DEBUG')];
        $bubble                     = ArrayHelper::extractBool('bubble', $config, true);
        $use_attachement            = ArrayHelper::extractBool('use_attachement', $config, true);
        $icon_emoji                 = ArrayHelper::extractStr('icon_emoji', $config);
        $use_short_attachment       = ArrayHelper::extractBool('use_short_attachment', $config, false);
        $include_context_and_extra  = ArrayHelper::extractBool('include_context_and_extra', $config, false);
        $exclude_fields             = ArrayHelper::extractArray('exclude_fields', $config, []);

        return new \Monolog\Handler\SlackHandler(
            $token,
            $channel,
            $username,
            $use_attachement,
            $icon_emoji,
            $level,
            $bubble,
            $use_short_attachment,
            $include_context_and_extra,
            $exclude_fields
        );
    }

    /**
     * @param  array  $config []
     *                          ['index']       string
     *                          ['type']        string
     *                          ['host']        string
     *                          ['port']        int
     *                          ['path']        string
     *                          ['url']         string
     *                          ['timeout']     float
     *                          ['username']    string
     *                          ['password']    string
     *                          ['level']       string
     *                          ['bubble']      bool
     * @return HandlerInterface
     */
    private static function createElastic(array $config)
    {
        $levels = Logger::getLevels();
        $clnt_options  = [];
        $clnt_options['host']       = ArrayHelper::extractStr('host', $config, null, true);
        $clnt_options['port']       = ArrayHelper::extractInt('port', $config, null, true);
        $clnt_options['path']       = ArrayHelper::extractStr('path', $config, null);
        $clnt_options['url']        = ArrayHelper::extractStr('url', $config, null);
        $clnt_options['timeout']    = ArrayHelper::extractFloat('timeout', $config, null);
        $clnt_options['username']   = ArrayHelper::extractStr('username', $config);
        $clnt_options['password']   = ArrayHelper::extractStr('password', $config);
        $client = new \Elastica\Client($clnt_options);

        $options = [];
        $options['index']         = ArrayHelper::extractStr('index', $config, 'monolog');
        $options['type']          = ArrayHelper::extractStr('type', $config, 'record');
        $options['ignore_error']  = ArrayHelper::extractBool('ignore_error', $config, false);

        $level    = $levels[ArrayHelper::extractStr('level', $config, 'DEBUG')];
        $bubble   = ArrayHelper::extractBool('bubble', $config, true);

        return new \Monolog\Handler\ElasticSearchHandler($client, $options, $level, $bubble);
    }

    /**
     * @param  array  $config   []
     *                            ['to']                string|array
     *                            ['subject']           string
     *                            ['from']              string
     *                            ['level']             string
     *                            ['bubble']            bool
     *                            ['max_column_width']  int
     * @return HandlerInterface
     */
    private static function createEmail(array $config)
    {
        $levels = Logger::getLevels();
        $to               = ArrayHelper::extract('to', $config, ['string', 'array'], null, true);
        $subject          = ArrayHelper::extractStr('subject', $config, null, true);
        $from             = ArrayHelper::extractStr('from', $config, null, true);
        $level            = $levels[ArrayHelper::extractStr('level', $config, 'DEBUG')];
        $bubble           = ArrayHelper::extractBool('bubble', $config, true);
        $max_column_width = ArrayHelper::extractInt('max_column_width', $config, 70);

        return new \Monolog\Handler\NativeMailerHandler($to, $subject, $from, $level, $bubble, $max_column_width);
    }

    private static function createPHPConsole(array $config)
    {
        $levels = Logger::getLevels();
        $options          = ArrayHelper::extractArray('options', $config, []);
        $level            = $levels[ArrayHelper::extractStr('level', $config, 'DEBUG')];
        $bubble           = ArrayHelper::extractBool('bubble', $config, true);

        return new \Monolog\Handler\PHPConsoleHandler($options, null, $level, $bubble);
    }

    /**
     * @param  array  $config []
     *                          ['level']  string
     *                          ['bubble'] bool
     *
     * @return HandlerInterface
     */
    private static function createChromePHP(array $config)
    {
        $levels = Logger::getLevels();
        $level  = $levels[ArrayHelper::extractStr('level', $config, 'DEBUG')];
        $bubble = ArrayHelper::extractBool('bubble', $config, true);

        return new \Monolog\Handler\ChromePHPHandler($level, $bubble);
    }

    private static function createFirePHP(array $config)
    {
        $levels = Logger::getLevels();
        $level  = $levels[ArrayHelper::extractStr('level', $config, 'DEBUG')];
        $bubble = ArrayHelper::extractBool('bubble', $config, true);

        return new \Monolog\Handler\FirePHPHandler($level, $bubble);
    }

    /**
     * @param  array  $config     []
     *                              ['type']    string (lineformat)
     * @return FormatterInterface
     */
    private static function createFormatter(array $config)
    {
        $type = ArrayHelper::extractStr('type', $config, null, true);

        switch ($type) {
            case 'lineformat':
                return self::createLineFormatter($config);
            break;
        }

        throw new \RuntimeException("Not found formatter type '$type'");
    }

    /**
     * @param  array  $config []
     *                          ['format']                          string
     *                          ['date_format']                     string
     *                          ['allow_inline_line_breaks']        bool
     *                          ['ignore_empty_context_and_extra']  bool
     * @return FormatterInterface
     */
    private static function createLineFormatter(array $config)
    {
        $format                          = ArrayHelper::extractStr('format', $config, null, true);
        $date_foramt                     = ArrayHelper::extractStr('date_format', $config, null);
        $allow_inline_line_breaks        = ArrayHelper::extractBool('allow_inline_line_breaks', $config, false);
        $ignore_empty_context_and_extra  = ArrayHelper::extractBool('ignore_empty_context_and_extra', $config, false);

        return new \Monolog\Formatter\LineFormatter(
            $format,
            $date_foramt,
            $allow_inline_line_breaks,
            $ignore_empty_context_and_extra
        );
    }
}
