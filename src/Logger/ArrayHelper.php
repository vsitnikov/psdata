<?php

namespace PSData\Logger;

abstract class ArrayHelper
{
    /**
     * @param  string         $key
     * @param  array          $array
     * @param  string|array   $type
     * @param  mixed          $default
     * @param  bool           $mandatory
     *
     * @return mixed
     *
     * @throws \RuntimeException
     */
    public static function extract(string $key, array $array, $type, $default = null, bool $mandatory = false)
    {
        if (!array_key_exists($key, $array)) {
            if ($mandatory) {
                throw new \RuntimeException("Not found mandatory key '$key'");
            }
            return $default;
        }

        $value      = $array[$key];
        $value_type = null;

        if (is_callable($value)) {
            $value_type = 'callable';
        } else {
            $value_type = gettype($value);
        }

        self::checkType($value_type, $type);

        return $value;
    }

    /**
     * @param  string   $key
     * @param  array    $array
     * @param  mixed    $default
     * @param  bool  $mandatory
     *
     * @return mixed
     */
    public static function extractStr(string $key, array $array, $default = null, bool $mandatory = false)
    {
        return self::extract($key, $array, 'string', $default, $mandatory);
    }

    /**
     * @param  string   $key
     * @param  array    $array
     * @param  mixed    $default
     * @param  bool  $mandatory
     *
     * @return mixed
     */
    public static function extractInt(string $key, array $array, $default = null, bool $mandatory = false)
    {
        return self::extract($key, $array, 'integer', $default, $mandatory);
    }

    /**
     * @param  string   $key
     * @param  array    $array
     * @param  mixed    $default
     * @param  bool  $mandatory
     *
     * @return mixed
     */
    public static function extractBool(string $key, array $array, $default = null, bool $mandatory = false)
    {
        return self::extract($key, $array, 'boolean', $default, $mandatory);
    }

    /**
     * @param  string   $key
     * @param  array    $array
     * @param  mixed    $default
     * @param  bool  $mandatory
     *
     * @return mixed
     */
    public static function extractFloat(string $key, array $array, $default = null, bool $mandatory = false)
    {
        return self::extract($key, $array, 'double', $default, $mandatory);
    }

    /**
     * @param  string   $key
     * @param  array    $array
     * @param  mixed    $default
     * @param  bool  $mandatory
     *
     * @return mixed
     */
    public static function extractArray(string $key, array $array, $default = null, bool $mandatory = false)
    {
        return self::extract($key, $array, 'array', $default, $mandatory);
    }

    /**
     * @param  string $path
     * @param  array  $array
     *
     * @return mixed
     */
    public static function find(string $path, array $array)
    {
        return self::findElement($array, explode('/', $path));
    }

    /**
     * @param  string $path
     * @param  array  $array
     *
     * @return string|null
     */
    public static function findStr(string $path, array $array): ?string
    {
        try {
            $result = self::find($path, $array);
            self::checkType(gettype($result), 'string');
            return $result;
        } catch (\RuntimeException $e) {
            return null;
        }
    }

    /**
     * @param  string $path
     * @param  array  $array
     *
     * @return int|null
     */
    public static function findInt(string $path, array $array): ?int
    {
        try {
            $result = self::find($path, $array);
            self::checkType(gettype($result), 'integer');
            return $result;
        } catch (\RuntimeException $e) {
            return null;
        }
    }

    /**
     * @param  string $path
     * @param  array  $array
     *
     * @return bool|null
     */
    public static function findBool(string $path, array $array): ?bool
    {
        try {
            $result = self::find($path, $array);
            self::checkType(gettype($result), 'boolean');
            return $result;
        } catch (\RuntimeException $e) {
            return null;
        }
    }

    /**
     * @param  string $path
     * @param  array  $array
     *
     * @return float|null
     */
    public static function findFloat(string $path, array $array): ?float
    {
        try {
            $result = self::find($path, $array);
            self::checkType(gettype($result), 'double');
            return $result;
        } catch (\RuntimeException $e) {
            return null;
        }
    }

    /**
     * @param  array            $node
     * @param  array            $path
     * @return mixed
     *
     * @throws \RuntimeException
     */
    private static function findElement($node, array $path)
    {
        if (!is_array($node)) {
            throw new \RuntimeException('Not found node');
        }

        $next_node = null;
        $next_key  = array_shift($path);

        if (is_null($next_key) || !array_key_exists($next_key, $node)) {
            throw new \RuntimeException('Not found node');
        }

        $next_node = $node[$next_key];

        /*
        if (is_null($next_node)) {
            throw new RuntimeException('Not found node');
        }
        */

        if (empty($path)) {
            return $next_node;
        }

        return self::findElement($next_node, $path);
    }

    /**
     * @param  string             $value_type
     * @param  string|array       $type
     * @return void
     *
     * @throws \RuntimeException
     */
    private static function checkType($value_type, $type)
    {
        if (is_array($type)) {
            if (in_array($value_type, $type)) {
                return;
            }
            $expected = implode(',', $type);
            throw new \RuntimeException("Type mismatch: expected '$expected', but supplied '$value_type'");
        }
        if (is_string($type)) {
            if ($type == $value_type) {
                return;
            }
            throw new \RuntimeException("Type mismatch: expected '$type', but supplied '$value_type'");
        }
        throw new \RuntimeException("The value of 'type' parameter must be string or array");
    }
}
