<?php
namespace PSData\Logger;

/**
 * LoggerFactory
 */
abstract class LoggerFactory
{
    /**
     * @param  array  $config
     * @param  string $adapter
     *
     * @return \Psr\Log\LoggerInterface
     *
     * @throws \RuntimeException
     */
    public static function create(array $config, string $adapter = 'monolog')
    {
        $adapter_service = 'PSData\Logger\\' . ucfirst($adapter) . 'Logger';

        if (!class_exists($adapter_service)) {
            throw new \RuntimeException("Not found adapter '$adapter'");
        }

        return $adapter_service::factory($config);
    }
}
