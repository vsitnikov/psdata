<?php declare(strict_types=1);

namespace PSData\Security\Authentication\Token;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

/**
 * Class JwtToken
 *
 * @package PSData\Authentication\Security\Token
 */
class JwtToken extends AbstractToken
{
    /** @var  string Токен в формате JWT */
    public $token;
    
    /**
     * JwtToken constructor.
     *
     * @param mixed $token Токен
     * @param array $payload Полезные данные
     */
    public function __construct($token, $payload = [])
    
    {
        if ($token instanceof UserInterface) {
            /** @var UserInterface $token */
            $this->setUser($token);
            $roles = $token->getRoles();
            $this->setAttributes(["roles" => $roles]);
            $this->setAttribute("username", $token->getUsername());
            parent::__construct($roles);
            $this->setAuthenticated(\count($roles) > 0);
            return;
        }
        
        //  Запоминаем полезную информацию
        $this->token = $token;
        
        if (!empty($payload)) {
            $this->setAttributes((array)$payload);
        }
    }
    
    /**
     * @inheritdoc
     */
    public function getCredentials()
    {
        return '';
    }
    
    /**
     * @param mixed $default Значение по умолчанию, если аттрибута нет
     * @inheritdoc
     *
     * @return mixed
     */
    public function getAttribute($name, $default = null)
    {
        try {
            return parent::getAttribute($name);
            
        } catch (\InvalidArgumentException $exception) {
            return $default;
        }
    }
}
