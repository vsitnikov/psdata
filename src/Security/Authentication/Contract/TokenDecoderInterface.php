<?php declare(strict_types=1);
/**
 * Created by IntelliJ IDEA.
 * User: Vyacheslav.Sitnikov
 * Date: 05.03.2019
 * Time: 11:55
 */

namespace PSData\Security\Authentication\Contract;

use PSData\Security\Authentication\Token\JwtToken;

/**
 * TokenDecoderInterface предназначен для декодирования токена
 *
 * @package PSData\Contract
 */
interface TokenDecoderInterface
{
    /**
     *  Декодирование токена
     *
     * @param JwtToken|string $token Токен
     * @param array $alg   Алгоритм шифрования
     *
     * @return JwtToken Токен с расшифрованной информацией
     */
    public function decode($token, $alg = ['HS256', 'HS512', 'HS384', 'RS256', 'RS384', 'RS512']): JwtToken;
}
