<?php declare(strict_types=1);
/**
 * Created by IntelliJ IDEA.
 * User: Vyacheslav.Sitnikov
 * Date: 05.03.2019
 * Time: 11:55
 */

namespace PSData\Security\Authentication\Contract;

/**
 * Interface TokenEncoderInterface
 *
 * @package PSData\Authentication\Contract
 */
interface TokenEncoderInterface
{
    /**
     * @param array|object $payload Полезная информация
     * @param string       $alg     Алгоритм шифрования
     * @param string       $keyId   Идентификатор ключа
     * @param array        $head    Дополнительные параметры заголовка
     *
     * @return string JWT Токен
     */
    public function encode($payload, string $alg = 'HS256', $keyId = null, $head = null): string;
}
