<?php declare(strict_types=1);

namespace PSData\Security\Authentication\Provider;

use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Provider\UserAuthenticationProvider;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use PSData\Security\Authentication\Token\JwtToken;
use PSData\Security\Authentication\Contract\TokenDecoderInterface;
use PSData\Security\Authentication\Contract\TokenEncoderInterface;


class JwtProvider implements AuthenticationProviderInterface
{
    /** @var \Symfony\Component\Security\Core\User\UserProviderInterface */
    private $userProvider;
    
    /** @var string Ключ провайдера */
    private $provider_key;
    
    /** @var \Psr\Cache\CacheItemPoolInterface */
    private $cachePool;
    
    /** @var \PSData\Authentication\Contract\TokenDecoderInterface */
    private $decoder;
    
    /** @var \PSData\Authentication\Contract\TokenEncoderInterface */
    private $encoder;
    
    public function __construct(
        UserProviderInterface $userProvider,
        string $provider_key,
        CacheItemPoolInterface $cachePool,
        TokenDecoderInterface $decoder,
        TokenEncoderInterface $encoder
    ) {
        $this->userProvider = $userProvider;
        $this->provider_key = $provider_key;
        $this->cachePool = $cachePool;
        $this->decoder = $decoder;
        $this->encoder = $encoder;
    }
    
    /**
     * @inheritdoc
     */
    public function authenticate(TokenInterface $token)
    {
        //  Выходим, если нам попался чужой токен
        if (!$token instanceof JwtToken)
            throw new AuthenticationException("Invalid token");
        
        //  Получили токен с заполненными данными
        $decoded = $this->decoder->decode($token);
        
        /** @var string $username */
        $username = $decoded->getAttribute("username", "");
        if (empty($username))
            throw new AuthenticationException("Invalid username.");
        
        //  Если пользователь получен и токен не тыква
        //if ($this->validateToken($token->getAttributes())) {
        //
        //}
        
        //  Поскольку access токен живёт несколько минут, то нет смысла обновлять роли в нём, лучше сделать это при обновлении refresh токена
        
        $username = $this->userProvider->loadUserByUsername($username);
        
        /** @var array $roles */
        $roles = $decoded->getAttribute("roles", []);
        return new UsernamePasswordToken($username, '', $this->provider_key, $roles);
        //
        //
        ////  Получаем через провайдера имя пользователя, будем пытаться обновить роли
        //$user = $this->userProvider->loadUserByUsername($username);
        //
        ////  Если пользователь получен и токен не тыква
        //if ($user && $this->validateToken($token->getAttributes())) {
        //    
        //    //  Получаем токен с ролями и запоминаем в нём пользователя
        //    $authenticatedToken = new JwtToken($user->getRoles());
        //    $authenticatedToken->setUser($user);
        //    $authenticatedToken->setAttributes($attributes);
        //    
        //    return $authenticatedToken;
        //}
        //
        ////  В любом другом случае герируем ошибку аутентификации (распознавания по логину/паролю)
        //throw new AuthenticationException('The WSSE authentication failed.');
    }
    
    /**
     * Эта функция характерна исключительно для аутентификации WSSE и используется только для помощи в этом примере
     * Чтобы узнать больше информации об особенной логике здесь, см.
     * https://github.com/symfony/symfony-docs/pull/3134#issuecomment-27699129
     */
    protected function validateDigest($digest, $nonce, $created, $secret)
    {
        // Проверить, чтобы созданное время не было в будущем
        if (strtotime($created) > time()) {
            return false;
        }
        
        // Временная метка истекает через 5 минут
        if (time() - strtotime($created) > 300) {
            return false;
        }
        
        // Попробовать извлечь объкт кеша из пула
        $cacheItem = $this->cachePool->getItem(md5($nonce));
        
        // Валидировать, что nonce *не* в кеше
        // если он там, это может быть повторной атакой
        if ($cacheItem->isHit()) {
            throw new \NonceExpiredException('Previously used nonce detected');
        }
        
        // Сохранить объект в кеше на 5 минут
        $cacheItem->set(null)->expiresAfter(300);
        $this->cachePool->save($cacheItem);
        
        // Валидировать секрет
        $expected = base64_encode(sha1(base64_decode($nonce).$created.$secret, true));
        
        return hash_equals($expected, $digest);
    }
    
    public function supports(TokenInterface $token)
    {
        return $token instanceof JwtToken;
    }
}

