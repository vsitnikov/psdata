<?php
/**
 * Created by IntelliJ IDEA.
 * User: Vyacheslav.Sitnikov
 * Date: 06.03.2019
 * Time: 7:50
 */

namespace PSData\Security\Authentication\Encoder;

use \InvalidArgumentException;
use \UnexpectedValueException;

use PSData\Security\Authentication\Contract\TokenEncoderInterface;
use PSData\Security\Authentication\Contract\TokenDecoderInterface;
use PSData\Security\Authentication\Token\JwtToken;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

use Firebase\JWT\JWT;

/**
 * Class JwtEncoder
 *
 * @package PSData\Authentication\Encoder
 */
class JwtEncoder implements TokenEncoderInterface, TokenDecoderInterface
{
    /** @var int Время жизни токена при генерации */
    public $ttl = 122400;
    
    /** @var string */
    private $publicKey;
    
    /** @var string */
    private $privateKey;
    
    /**
     * JwtEncoder constructor.
     *
     * @param string|null $publicKey  Ключ для кодирования/декодирования HS или публичный ключ для декодирования RSA
     * @param string|null $privateKey [optional] Приватный ключ для кодирования RSA
     */
    public function __construct(string $publicKey, string $privateKey = null)
    {
        $publicKey = trim($publicKey);
        $privateKey = trim($privateKey);
        
        if ($publicKey != "")
            $this->publicKey = $publicKey;
        else
            throw new InvalidArgumentException("Ключ не может быть пустым");
        if ($privateKey != "")
            $this->privateKey = $privateKey;
    }
    
    /**
     * @inheritdoc
     */
    public function encode($payload, $alg = 'HS256', $keyId = null, $head = null): string
    {

        //  Выделяем полезную нагрузку
        if ($payload instanceof AbstractToken)
            $payload = $payload->getAttributes();
        if (!isset($payload['exp']))
            $payload['exp'] = gmdate("U") + $this->ttl;
        $payload['jti'] = uniqid();
    
    
        if (empty(JWT::$supported_algs[$alg]))
            throw new UnexpectedValueException('Algorithm not supported');
        elseif (substr(trim($alg), 0, 2) == "HS")
            $encoded = JWT::encode($payload, $this->publicKey, $alg, $keyId, $head);
        else {
            if (empty($this->privateKey))
                throw new InvalidArgumentException("Необходимо указать приватный ключ");
            $encoded = JWT::encode($payload, $this->privateKey, $alg, $keyId, $head);
        }
        
        return $encoded;
    }
    
    /**
     * @inheritdoc
     */
    public function decode($token, $alg = ['HS256', 'HS512', 'HS384', 'RS256', 'RS384', 'RS512']): JwtToken
    {
        if ($token instanceof JwtToken)
            $token = $token->token;
        try {
            $payload = JWT::decode($token, $this->publicKey, $alg);
        } catch (\Exception $e) {
            $payload = [];
        }
        return new JwtToken($token, $payload);
    }
    
}
