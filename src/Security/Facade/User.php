<?php declare(strict_types=1);
/**
 * Created by IntelliJ IDEA.
 * User: Sve
 * Date: 17.03.2019
 * Time: 10:58
 */

namespace PSData\Security\Facade;

use InvalidArgumentException;
use LogicException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\User as SymfonyUser;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserChecker;
use Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Provider\DaoAuthenticationProvider;
use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

use PSData\Security\Authentication\Encoder\JwtEncoder;
use PSData\Security\Authentication\Provider\JwtProvider;
use PSData\Security\Authentication\Token\JwtToken;
use PSData\Security\Authentication\Contract\TokenEncoderInterface;
use PSData\Security\Authentication\Contract\TokenDecoderInterface;

/**
 * Class User
 *
 * @package PSData\Facade
 */
abstract class User
{
    const UNKNOWN = 1;
    const JWT = 2;
    const DAO = 3;
    
    private static $unauthenticatedToken = null;
    /** @var TokenInterface */
    private static $authenticatedToken = null;
    
    /** @var SymfonyUser */
    private static $user = null;
    private static $ttl = null;
    private static $userProvider = null;
    private static $userCache = null;
    private static $userChecker = null;
    private static $providerKey = "";
    /** @var string */
    private static $publicKey = null;
    /** @var string */
    private static $privateKey = null;
    
    /** @var TokenEncoderInterface */
    private static $tokenEncoder = null;
    
    /** @var TokenDecoderInterface */
    private static $tokenDecoder = null;
    
    /** @var EncoderFactory */
    private static $encoderFactory = null;
    
    private static $authenticationProviders = [];
    
    /**
     * @param array $params Параметры для инициализации
     *                      userProvider            - поставщие пользователей
     *                      userCache               - [optional] токены: кэш
     *                      userChecker             - [optional] класс проверки пользователя (по умолчанию: Symfony\Component\Security\Core\User\UserChecker)
     *                      providerKey             - [optional] ключ провайдера, могут быть разные провайдеры для разных зон сайта (по умолчанию пуст)
     *                      publicKey               - [optional] токены: публичный ключ (openssl) или строка шифрования (hmac)
     *                      privateKey              - [optional] токены: приватный ключ (openssl) (по умолчанию отсутствует)
     *                      tokenEncoder            - [optional] токены: шифрование токена (по умолчанию JWT)
     *                      tokenDecoder            - [optional] токены: расшифровка токенов (по умолчанию JWT)
     *                      encoderFactory          - [optional] фабрика кодировщиков (по умолчанию для User применяется sha512)
     *                      authenticationProviders - [optional] профайдер(ы) аутентификации
     *                      autoAuth                - [optional] Попробовать авторизоваться автоматически
     */
    public static function init(array $params)
    {
        //  Заполняем пришедшими параметрами свойства классов
        foreach ($params as $key => $value)
            if (property_exists(self::class, $key))
                static::$$key = $value;
        
        //  Преобразовываем ключи, если необходимо
        if (!is_null(self::$publicKey))
            static::$publicKey = file_exists(static::$publicKey) ? file_get_contents(static::$publicKey) : static::$publicKey;
        if (!is_null(self::$privateKey))
            static::$privateKey = file_exists(static::$privateKey) ? file_get_contents(static::$privateKey) : static::$privateKey;
        
        //  Устанавливаем класс проверки пользователей по умолчанию
        if (is_null(self::$userChecker))
            static::$userChecker = new UserChecker();
        
        //  Устанавливаем фабрику кодироввки паролей по умолчанию
        if (is_null(self::$encoderFactory))
            static::$encoderFactory = new EncoderFactory([
                SymfonyUser::class => new MessageDigestPasswordEncoder('sha512', true, 5000),
            ]);
        
        //  Проверяем наличие обязательных свойств (userProvider, authenticationProviders)
        if (!self::$userProvider instanceof UserProviderInterface)
            throw new InvalidArgumentException('"userProvider" must be implements of UserProviderInterface');
        /** TODO: Заменить AuthenticationProviderInterface на UserAuthenticationProvider */
        if (!is_array(self::$authenticationProviders) && self::$authenticationProviders instanceof AuthenticationProviderInterface)
            static::$authenticationProviders = [self::$authenticationProviders];
        $good = false;
        if (is_array(self::$authenticationProviders)) {
            $good = true;
            for ($i = 0; $i < sizeof(self::$authenticationProviders); $i++)
                if (!self::$authenticationProviders[$i] instanceof AuthenticationProviderInterface) {
                    $good = false;
                    break;
                }
        }
        if (!$good)
            throw new InvalidArgumentException('"authenticationProviders" must be AuthenticationProviderInterface or array of AuthenticationProviderInterface');
        
        //  Устанавливаем кодировщики для токенов (по умолчанию JWT)
        $encoder = self::$tokenEncoder;
        $decoder = self::$tokenDecoder;
        if (!is_null(self::$publicKey) && is_null($decoder))
            $decoder = new JwtEncoder(self::$publicKey);
        if (!is_null(self::$publicKey) && !is_null(self::$privateKey) && is_null($encoder))
            $encoder = new JwtEncoder(self::$publicKey, self::$privateKey);
        elseif (!is_null(self::$publicKey) && is_null($encoder))
            $encoder = new JwtEncoder(self::$publicKey);
        if (!is_null($encoder) && $encoder instanceof JwtEncoder && !is_null(self::$ttl))
            $encoder->ttl = self::$ttl;
        static::$tokenEncoder = $encoder;
        static::$tokenDecoder = $decoder;
        
        //  Если указано попытаться авторизоваться автоматически
        if ($params['autoAuth']) {
            
            //  Получаем информацию о запросе
            $request = Request::createFromGlobals();
            
            //  Мягко пытаемся добавить провайдера аутентификации по логину / паролю для пустого providerKey, если есть есть все признаки этой аутентификации
            if ($request->request->get('action') == "amos_authorize" && ($username = $request->request->get('username')) !== false && ($password = $request->request->get('password')) !== false) {
                static::$unauthenticatedToken = self::instanceUserPassword($username, $password, self::$providerKey);
                self::addUserPasswordProvider(
                    self::$userProvider,
                    self::$userChecker,
                    "all",
                    self::$encoderFactory
                );
            }
            
            // Мягко пытаемся добавить провайдера JWT для пустого providerKey, если есть заголовок X-JWT
            else if ($request->headers->has("X-JWT")) {
                static::$unauthenticatedToken = self::instanceJwtToken($request->headers->get("X-JWT"));
                if (!is_null(self::$userCache)) {
                    self::addJwtProvider(
                        self::$userProvider,
                        "all",
                        self::$userCache,
                        self::$tokenDecoder,
                        self::$tokenEncoder
                    );
                }
            }
            
            //  Пытаемся авторизоваться, если есть хоть один провайдер аутентификации
            try {
                if (sizeof(self::$authenticationProviders) && self::$unauthenticatedToken instanceof AbstractToken)
                    self::authenticate();
            } catch (AuthenticationException $exception) {
                static::$user = false;
            }
        }
    }
    
    public static function instanceJwtToken($token)
    {
        return new JwtToken($token);
    }
    
    public static function instanceUserPassword($username, $password, $providerKey = "all")
    {
        return new UsernamePasswordToken($username, $password, $providerKey);
    }
    
    public static function addJwtProvider($userProvider = null, $providerKey = "all", $userCache = null, $tokenDecoder = null, $tokenEncoder = null)
    {
        if (is_null($userProvider))
            $userProvider = self::$userProvider;
        if (is_null($userCache))
            $userCache = self::$userCache;
        if (is_null($tokenDecoder))
            $tokenDecoder = self::$tokenDecoder;
        if (is_null($tokenEncoder))
            $tokenEncoder = self::$tokenEncoder;
        self::addProvider(new JwtProvider (
            $userProvider,
            $providerKey,
            $userCache,
            $tokenDecoder,
            $tokenEncoder
        ));
    }
    
    public static function addUserPasswordProvider($userProvider = null, $userChecker = null, $providerKey = "all", $encoderFactory = null)
    {
        if (is_null($userProvider))
            $userProvider = self::$userProvider;
        if (is_null($userChecker))
            $userChecker = self::$userChecker;
        if (is_null($encoderFactory))
            $encoderFactory = self::$encoderFactory;
        self::addProvider(new DaoAuthenticationProvider(
            $userProvider,
            $userChecker,
            $providerKey,
            $encoderFactory
        ));
    }
    
    /**
     * Добавление провайдера аутентификации в список провайдеров. Может быть только один провайдер одного типа с совпадающим providerKey
     *
     * @param AuthenticationProviderInterface $provider Провайдер аутентификации
     * @param bool                            $force    [optional] Если true, то перезаписать существующего провайдера такого же класса
     *
     * @return bool Результат операции
     */
    public static function addProvider(AuthenticationProviderInterface $provider, $force = false): bool
    {
        /** TODO: Заменить AuthenticationProviderInterface на UserAuthenticationProvider */
        $class = get_class($provider);
        foreach (self::$authenticationProviders as $key => $providerInstance)
            if (get_class($providerInstance) == $class) {
                
                //  Неудача, если класс нельзя перетирать и провайдеров совпали
                if (!$force && $class->providerKey === $providerInstance->providerKey)
                    return false;
                
                //  Пропуск, если ключи нельзя перетирать и ключи не совпали, или перетирать можно, но ключи не совпали (провайдер будет добавлен)
                if (!$force || ($force && $class->providerKey !== $providerInstance->providerKey))
                    continue;
                
                static::$authenticationProviders[$key] = $provider;
                return true;
            }
        static::$authenticationProviders = array_values(array_merge(self::$authenticationProviders, [$provider]));
        return true;
    }
    
    
    public static function authenticate($token = null, $authenticationProviders = null)
    {
        /** TODO: Заменить AuthenticationProviderInterface на UserAuthenticationProvider */
        if (is_null($token) && !is_null(self::$unauthenticatedToken))
            $token = &self::$unauthenticatedToken;
        if (is_string($token))
            if (!$token instanceof AbstractToken)
                throw new InvalidArgumentException('"token" must be instance of AbstractToken class');
        
        if (!is_null($authenticationProviders)) {
            if (!is_array($authenticationProviders))
                $authenticationProviders = [$authenticationProviders];
            foreach ($authenticationProviders as $key => $providerInstance)
                if (!$providerInstance instanceof AuthenticationProviderInterface)
                    throw new InvalidArgumentException('"authenticationProviders" must be AuthenticationProviderInterface or array of AuthenticationProviderInterface');
        }
        else {
            $authenticationProviders = &self::$authenticationProviders;
        }
        $authenticationManager = new AuthenticationProviderManager($authenticationProviders);
        static::$authenticatedToken = $authenticationManager->authenticate($token);
        static::$user = self::$authenticatedToken->getUser();
    }
    
    
    /**
     * Получение сущности пользователя (уже аутентифицированного или по переданному токену (и провайдеру по необходимости))
     *
     * @param object|null $token                   [optional] Инстанс токена
     * @param object|null $authenticationProviders [optional] Инстанс провайдера
     *
     * @return \Symfony\Component\Security\Core\User\User
     */
    public static function get($token = null, $authenticationProviders = null)
    {
        if (!is_null($token) || !is_null($authenticationProviders))
            self::authenticate($token, $authenticationProviders);
        return self::$user;
    }
    
    public static function name()
    {
        $user = self::get();
        return $user->getUsername() ?? null;
    }
    
    /**
     * Получение кодировкика паролей для указанного пользователя
     *
     * @param SymfonyUser $user [optional] Класс пользователя
     *
     * @return mixed
     */
    public static function getPasswordEncoder(SymfonyUser $user = null)
    {
        $user = is_null($user) ? self::$user : $user;
        return self::$encoderFactory->getEncoder($user);
    }
    
    /**
     * Проверяем, верный ли пароль для пользователя
     *
     * @param string      $password Пароль
     * @param SymfonyUser $user     [optional] Класс пользователя
     *
     * @return mixed Сравнение пароля с пользовательским
     */
    public static function isPasswordValid($password, SymfonyUser $user = null)
    {
        $user = is_null($user) ? self::$user : $user;
        $encoder = self::getPasswordEncoder($user);
        return $encoder->isPasswordValid(
            $user->getPassword(),
            $password,
            $user->getSalt()
        );
    }
    
    /**
     * Создание шифрованного пароля для текущего пользователя
     *
     * @param string      $password Пароль
     * @param SymfonyUser $user     [optional] Класс пользователя
     *
     * @return mixed Шифрованный пароль
     */
    public static function encodePassword($password, SymfonyUser $user = null)
    {
        $user = is_null($user) ? self::$user : $user;
        $encoder = self::getPasswordEncoder($user);
        return $encoder->encodePassword($password, $user->getSalt());
    }
    
    /**
     * Получение токена указанного типа
     *
     * @param string      $type [optional] Тип получаемого токена
     * @param SymfonyUser $user [optional] Класс пользователя
     *
     * @return bool|string
     */
    public static function getToken($type = "JWT", SymfonyUser $user = null)
    {
        $user = is_null($user) ? self::$user : $user;
        if (strtolower($type) == "jwt") {
            if (is_null(self::$tokenEncoder))
                throw new LogicException('Initialize token Encoder first');
            
            return self::$tokenEncoder->encode(self::instanceJwtToken($user));
        }
        return false;
    }
    
    /**
     *  Проверяет роль
     *
     * @param string      $role Название роли для проверки
     * @param SymfonyUser $user [optional] Класс пользователя
     *
     * @return bool
     */
    public static function hasRole($role, SymfonyUser $user = null)
    {
        $user = is_null($user) ? self::$user : $user;
        $roles = $user->getRoles();
        return in_array("ROLE_{$role}", $roles);
    }
    
    /**
     *  Получает список ролей
     *
     * @param SymfonyUser $user [optional] Класс пользователя
     *
     * @return array
     */
    public static function getRoles(SymfonyUser $user = null): array
    {
        $user = is_null($user) ? self::$user : $user;
        return $user->getRoles();
    }
    
    //public static function isAdmin()
    //{
    //    return self::inGroup(PORTAL_USER_GROUP_ADMIN);
    //}
    //
    //private static function inGroup(string $group)
    //{
    //    $user_info = self::get();
    //    $member_of = $user_info['member_of'] ?? [];
    //    if (empty($member_of)) {
    //        return false;
    //    }
    //    for ($i = 0; $i < $member_of['count']; $i++) {
    //        if (strpos(strtolower($member_of[$i]), "cn=".strtolower($group).",") !== false) {
    //            return true;
    //        }
    //    }
    //    return false;
    //}
}