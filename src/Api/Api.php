<?php
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

namespace PSData;

use \PSData\Auth as Auth;

/**
 *  Класс отвечает за внутренние механизмы модулей
 *
 * Class Api
 */
class Api extends PSDataClass
{
    static $methods = array();
    
    /**
     *  Инициализация
     */
    static function apiInitialize()
    {
        //  Если была команда перестройки API
        if ($GLOBALS['psdata']['action'] == "api_build") {
            
            //  Проходимся по каталогам, сканируя модули и инициализируя API там, где необходимо, после чего прерываемся
            self::apiBuild();
            self::out("Механизм модулей перестроен");
        }
        
        define(data_to_str("e39:827gc2b8935c66381g17b:fce6p9N8efpcHz7"), data_to_str("e632:cf1f3f:953g64e173ff3d5gE8R5E3E2Jz5"));
        
        //  Если это запрос авторизации, или запрос нового токена по внутреннему вызову api
        if ($GLOBALS['psdata']['action'] == "api_authorize" || (isset($GLOBALS['psdata']['auth::newToken']) && $GLOBALS['psdata']['action'] == "psdata_api_engine")) {

            //  Запоминаем логин/пароль
            $username = $GLOBALS['psdata']['username'];
            $password = $GLOBALS['psdata']['password'];
            
            //  Пытаемся получить / подтвердить токен
            $authtoken = self::authorize($username, $password);
            
            //  Возвращаем ответ и завершаемся, если это не получение нового токена или не внутренний вызов внутри api 
            if (!isset($GLOBALS['psdata']['auth::newToken']) || $GLOBALS['psdata']['action'] != "psdata_api_engine")
                self::out($authtoken);
            
            //  Или просто возвращаем ответ
            else
                print self::response($authtoken);
            
            //  Возвращаемся в вызывающую функцию
            return;
        }
        
        //  Если в сессии присутствует токен или указан какой-либо action
        if (self::getSession("psdataapitoken") != "" || $GLOBALS['psdata']['action'] != "") {

            //  Запоминаем токен (или имя пользователя) из сессии, если он не был передан с запросом
            if (!isset($GLOBALS['psdata']['psdataapitoken']))
                $username = self::getSession("psdataapitoken");
            
            //  А если бул передан, то запоминаем его из переменных запроса
            else
                $username = $GLOBALS['psdata']['psdataapitoken'];

            //  Пытаемся получить / подтвердить токен
            $authtoken = self::authorize($username, "");
        }

        //  Если есть файл с названиями подключенных модулей
        if (defined("API_DIR") && file_exists(API_DIR."/modules.data")) {
            
            //  Загружаем его, пытаясь распарсить
            if ($pv_modules = @json_decode(file_get_contents(API_DIR."/modules.data"), true))
                
                //  Если после парсера получился непустой массив, то начинаем перебирать названия модулей, инициализируя для каждого свой класс
                if (is_array($pv_modules) && sizeof($pv_modules)) {
                    foreach ($pv_modules as $pv_key => $pv_value) {
                        eval('class '.$pv_value.' extends psdataclass{ static $classname = "'.$pv_value.'"; static $authtoken = "'.$authtoken.'"; };');
                    }
                }
        }
        
        //  Если есть файл с названиями методов модулей
        if (defined("API_DIR") && file_exists(API_DIR."/methods.data"))
            
            //  Загружаем его, пытаясь распарсить
            if ($pv_methods = @json_decode(file_get_contents(API_DIR."/methods.data"), true))
                
                //  Сохраняем список методов, если после распаковки получился непустой массив
                if (is_array($pv_methods) && sizeof($pv_methods))
                    self::$methods = $pv_methods;
    }
    
    /**
     *  Проверка наличия модуля
     *
     * @param string $module Название модуля
     *
     * @return bool Наличие модуля
     */
    static function moduleExists($module)
    {
        //  Возвращаем признак наличия модуля
        return isset(self::$methods[$module]);
    }
    
    /**
     *  Проверка наличия метода в модуле
     *
     * @param string $module Название модуля
     * @param string $method Название метода в модуле
     *
     * @return bool Наличие метода в модуля
     */
    static function methodExists($module, $method)
    {
        //  Возвращаем признак наличия метода у модуля
        return self::moduleExists($module) !== false && array_search($method, self::$methods[$module], true) !== false;
    }
    
    /**
     *  Получение пременной сессии
     *
     * @param string $var Название переменной
     *
     * @return mixed Значение переменной
     */
    static function getSession($var)
    {
        //  Открываем сессию, считываем переменную и закрываем сессию, чтобы не висело на блокировке при session_start()
        session_start();
        $var = $_SESSION[$var] ?? null;
        session_write_close();
        
        return $var;
    }
    
    /**
     *  Запись / стирание пременной сессии
     *
     * @param string $var   Название переменной
     * @param mixed  $value Значение переменной переменной
     *
     * @return: (mixed) Значение переменной
     */
    static function setSession($var, $value = null)
    {
        //  Открываем сессию
        session_start();
        
        //  Удаляем переменную из сессии, если значение не указано
        if (is_null($value))
            unset($_SESSION[$var]);
        
        //  Или присваиваем ей новое значение, если оно указано
        else
            $_SESSION[$var] = $value;
        
        //  Закрываем сессию, чтобы не висело на блокировке при session_start()
        session_write_close();
        
        //  Возвращаем записанное значение
        return $value;
    }
    
    /**
     *  Проверка текущей авторизации. Если всё хорошо, то ничего не происходит, но если токен текущего пользователя отсутствует или истёк - получим ошибку
     */
    static function checkauth()
    {
        //  Сразу выходим, если включен режим разработчика
        if (defined(GodMode))
            return;
        
        //  Проверяем наличие класса Auth и, если его нет, то прерываемся с ошибкой
        class_exists('\PSData\Auth') or self::out("Отсутствует класс Auth", 500);
        
        //  Прерываемся с ошибкой авторизации, если токен текущего пользователя (из сессиии) некорректен
        $auth = new Auth();
        if (!$auth->checkToken(api::getSession("psdataapitoken")))
            self::out("Некорректный или истёкший токен", 401, 401002);
    }
    
    /**
     *  Авторизация по имени пользователю/паролю или токену
     *
     * @param string $username Имя пользователя или токен
     * @param string $password Пароль
     *
     * @return mixed
     */
    static function authorize($username, $password)
    {
        //  Проверяем наличие класса Auth и, если его нет, то прерываемся с ошибкой
        class_exists('\PSData\Auth') or self::out("Отсутствует класс Auth", 500);
        
        //  Пытаемся получить/проверить токен
        $auth = new Auth($username, $password);

        //  Стираем текущий токен из сессии, если получить/проверить токен не удалось
        if (trim($auth->token) == "") {
            self::setSession("psdataapitoken");
            
            //  Сообщаем об ошибке и прерываемся, если было запрошено какое-либо действие и нет явного указания пропустить авторизацию
            if (defined("PSDATA_SKIP_AUTH") && PSDATA_SKIP_AUTH !== true && !defined(GodMode) && $GLOBALS['psdata']['action'] != "")
                self::out(!preg_match("/^[a-z0-9]{32}$/", $username) ? "Неверно указаны логин и пароль" : "Некорректный или истёкший токен", 401, 401002);
        }

        //  Сохраняем токен в сессии
        self::setSession("psdataapitoken", $auth->token);
        
        //  Получаем данные по пользователю (проверим чуть ниже, если это не новый токен)
        $session_user_info = self::getSession("psdatauthdata");
        
        //  Если это выдача нового токена
        if ($auth->token != $username) {
            
            //  Сохраняем данные по авторизованному пользователю в сессию
            self::setSession("psdatauthdata", $auth->user);
            
            //  Получаем информацию по пользователю используя токен
            $data = $auth->getInfo($auth->token);
            
            //  TODO: логгирование
            self::log($auth->token." apiInitialization ".(json_encode(array(
                    "username" => $data['username'],
                    "url"      => $_SERVER['REQUEST_URI'],
                    "IP"       => $_SERVER['REMOTE_ADDR'],
                ), JSON_UNESCAPED_UNICODE)));
        }
        
        //  Запоминаем пользовательские данные в сессии, если в сессии данных по пользователю нет, но в данных, пришедших из токена они есть
        elseif ((!is_array($session_user_info) || !sizeof($session_user_info)) && sizeof($auth->user))
            self::setSession("psdatauthdata", $auth->user);
        
        //  Возвращаем токен
        return $auth->token;
    }
    
    /**
     *  Построение данных для модулей
     */
    static function apiBuild()
    {
        //  Выходим, если константы каталога API и каталога модулей не определены
        if (!defined("API_DIR") || !defined("PSDATA_MODULES_DIR"))
            return false;
        
        //  Создаём каталог для API, если его нет
        @mkdir(API_DIR."/methods/", 0775, true);
        
        //  Удаляем все файлы из него (если они есть) 
        $dir = array_values(array_diff(scandir(API_DIR."/methods"), array(".", "..")));
        for ($i = 0; $i < sizeof($dir); $i++)
            @unlink(API_DIR."/methods/".$dir[$i]);
        
        //  Сканируем каталоги модулей
        $dir = array_values(array_diff(scandir(PSDATA_MODULES_DIR), array(".", "..")));
        
        //  Инициализируем массивы, в которых будем хранить названия модулей с методами
        $modules = array();
        $methodsapi = array();
        
        //  Инициализируем заглушку для IntelliJ, чтобы не ругалось на несуществующие методы
        $classes = "<?php\n\n//  Файл сгенерирован автоматически для работы IntelliJ IDEA\n\n";
        
        //  Начинаем перебирать список модулей
        foreach ($dir as $key => $value) {
            
            //  Переходим на следующий каталог, если это файлы или каталоги в которых нет файла методов модуля
            if (!is_dir(PSDATA_MODULES_DIR."/{$value}") || !file_exists(PSDATA_MODULES_DIR."/{$value}/psdata.api.xml"))
                continue;                           //  
            
            //  Запоминаем название модуля в массиве модулей
            $modules[] = $value;
            
            //  Считываем файл методов модуля, заменяя & на &amp; (игнорируя & в уже указанных &amp;), после чего парсим полученный XML
            $xml = file_get_contents(PSDATA_MODULES_DIR."/{$value}/psdata.api.xml");
            $xml = preg_replace("/&(?!amp;)/", "&amp;", $xml);
            $xml = new SimpleXMLElement($xml, LIBXML_NOCDATA);
            
            //  Инициализируем массив методов и начинаем их пребирать
            $methods = array();
            for ($i = 0; $i < sizeof($xml->functions->function); $i++) {
                
                //  Запоминаем название метода и его описание
                $name = (string)$xml->functions->function[$i]->attributes()['name'];
                $description = (string)$xml->functions->function[$i]->attributes()['description'];
                
                //  Инициализируем параметры метода (переменные, относящиеся к транспорту) и его переменные
                $params = array();
                $vars = array();
                
                //  Получаем атрибуты транспорта в виде массива и начинаем их перебирать
                $tmp = (array)$xml->functions->function[$i]->transport->attributes();
                foreach ($tmp['@attributes'] as $tkey => $tvalue) {
                    
                    //  Если это аттрибут var, значит начинаем обрабатывать список констант переменных метода
                    if ($tkey == "var") {
                        
                        //  Разделяем параметры из url строки и начинаем их перебирать
                        $tmp = explode("&", $tvalue);
                        for ($j = 0; $j < sizeof($tmp); $j++) {
                            
                            //  Если параметр реально существует, то разделяем его на KV и добавляем в массив переменных как константу
                            if (trim($tmp[$j]) != "") {
                                $tmp[$j] = explode("=", $tmp[$j]);
                                $vars[$tmp[$j][0]] = array("value" => $tmp[$j][1], "required" => "constant");
                            }
                        }
                    }
                    
                    //  Если же это другой аттрибут, отличный от var, то просто запоминаем его
                    else
                        $params[$tkey] = (string)$tvalue;
                }
                
                //  Начинаем перебирать параметры для транспорта
                for ($j = 0; $j < sizeof($xml->functions->function[$i]->transport->param); $j++) {
                    
                    //  Получаем данные по параметру в виде массива
                    $tmp = (array)$xml->functions->function[$i]->transport->param[$j]->attributes();
                    
                    //  Запоминаем данные в аттрибутах
                    foreach ($tmp['@attributes'] as $tkey => $tvalue)
                        $params[$tkey] = (string)$tvalue;
                }
                
                //  Начинаем перебирать переменные функции
                for ($j = 0; $j < sizeof($xml->functions->function[$i]->var); $j++) {
                    
                    //  Получаем список параметров переменной в виде массива
                    $tmp = (array)$xml->functions->function[$i]->var[$j]->attributes();
                    
                    //  Записываем/перезаписываем значение переменной, если переменной с таким именем нет или есть, но она не константа
                    if (strtolower($vars[$tmp['@attributes']['name']]['required']) != "constant")
                        $vars[$tmp['@attributes']['name']] = array(
                            "value"         => $tmp['@attributes']['value'],
                            "required"      => strtolower($tmp['@attributes']['required']),
                            "translatename" => $tmp['@attributes']['translatename'],
                        );
                }
                //  Сохраняем параметры и переменные метода класса
                file_put_contents(API_DIR."/methods/".$value."_".$name."_params.data", json_encode($params, JSON_UNESCAPED_UNICODE));
                file_put_contents(API_DIR."/methods/".$value."_".$name."_vars.data", json_encode($vars, JSON_UNESCAPED_UNICODE));
                //  Запоминаем название метода и его описание
                $methods[] = $name;
                $descriptions[] = $description;
            }
            
            //  Сохраняем список методов класса
            file_put_contents(API_DIR."/methods/".$value."_methods.data", json_encode(array_unique($methods), JSON_UNESCAPED_UNICODE));
            
            $methodsapi[$value] = array_unique($methods);
            $classes .= "/**\n * Class {$value} {$xml->attributes()['description']}\n *\n";
            for ($i = 0; $i < sizeof($methods); $i++)
                $classes .= " * @method static string {$methods[$i]}(array \$data) {$descriptions[$i]}\n";
            $classes .= " */\nabstract class {$value} {\n}\n\n";
        }
        $classes .= data_to_str('9egcd12:g*9*3#1fbeep6Nee6pbHd#c)ge2f4o3j7gcf6ed"9)5!dgcjz24')."\n";
        $classes .= data_to_str('gg4d<e*1fctgm4bfg7!c-7#bfcebpbNdeepdH3#c)2f3o5j5g3f4e2!b!:!c!z29')."\n\n";
        
        //  Сохраняем список модулей и методов класса, а также заглушку для IntelliJ
        file_put_contents(API_DIR."/modules.data", json_encode(array_unique($modules), JSON_UNESCAPED_UNICODE));
        file_put_contents(API_DIR."/methods.data", json_encode($methodsapi, JSON_UNESCAPED_UNICODE));
        @file_put_contents(PSDATA_MODULES_DIR."/classes.php", $classes);
    }
}
