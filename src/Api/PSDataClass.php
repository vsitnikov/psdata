<?php
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

namespace PSData;

use \PSData\Auth as Auth;

/**
 * Class psdataclass
 */
abstract class PSDataClass
{
    /**
     *
     * @return string
     */
    static function ping()
    {
        return "pong";
    }
    
    /**
     *  Выдача ответа API
     *
     * @param string     $msg         [optional] Полезное сообщение
     * @param int        $code        [optional] Код ошибки
     * @param int|string $errorCode   [optional] Дополнительный код ошибки
     * @param array      $extraparams [optional] Дополнительный массив, поля которого будут включены в выходные данные
     *
     * @return string Выводит json с сообщением
     */
    static function response($msg = "", $code = 200, $errorCode = null, $extraparams = [])
    {
        $apimessage = array();
        $apimessage[200] = '{"status":"200", "message":"OK", "messageru":"Хорошо"}';
        $apimessage[201] = '{"status":"201", "message":"Created", "messageru":"Создано"}';
        $apimessage[302] = '{"status":"302", "message":"Move", "messageru":"Переход"}';
        $apimessage[304] = '{"status":"304", "message":"Not Modified", "messageru":"Не Изменялось"}';
        $apimessage[400] = '{"status":"400", "message":"Bad Request", "messageru":"Неверный запрос"}';
        
        // ТОЛЬКО ЕСЛИ НЕ ПРОШЛО ДОМЕННУЮ АВТОРИЗАЦИЮ
        $apimessage[401] = '{"status":"401", "message":"Unauthorized", "messageru":"Не Авторизован"}';
        
        // Если запрещено или не прошло внутреннюю авторизацию
        $apimessage[403] = '{"status":"403", "message":"Forbidden", "messageru":"Запрещено"}';
        $apimessage[404] = '{"status":"404", "message":"Not Found", "messageru":"Не Найдено"}';
        $apimessage[409] = '{"status":"409", "message":"Conflict", "messageru":"Конфликт"}';
        $apimessage[410] = '{"status":"410", "message":"Gone", "messageru":"Удалён"}';
        $apimessage[412] = '{"status":"412", "message":"Precondition Failed", "messageru":"Условие Ложно"}';
        $apimessage[500] = '{"status":"500", "message":"Internal Server Error", "messageru":"Внутренняя Ошибка Сервера"}';
        $message = $apimessage[$code];
        if ($message == "")
            $message = $apimessage[200];
        
        $data = json_decode($message, true);
        
        //  Запоминаем сообщение, если оно указано и это не массив
        if (!is_array($msg) && trim($msg) != "")
            $data["data"] = $msg;
        //  Запоминаем сообщение в виде json, если это не пустой массив
        elseif (is_array($msg) && sizeof($msg))
            $data["data"] = json_encode($msg, JSON_UNESCAPED_UNICODE);
        
        //  Если указан код ошибки, то запоминаем код и добавляем информацию по нему
        if (!is_null($errorCode)) {
            $data["code"] = $errorCode;
            $data["moreInfo"] = "http://msk-bss-ansib01:8080/psdata/docs/errors/".$errorCode;
        }
        
        //  Обогащаем массив дополнительными параметрами
        $data = array_merge($data, $extraparams);
        
        //  Возвращаем всё, что удалось собрать
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
    
    /**
     *  Выдача ответа API с последующим принудительным прерыванием выполнения всей программы
     *
     * @param string     $msg         [optional] Полезное сообщение
     * @param int        $code        [optional] Код ошибки
     * @param int|string $errorCode   [optional] Дополнительный код ошибки
     * @param array      $extraparams [optional] Дополнительный массив, поля которого будут включены в выходные данные
     *
     * @return string Выводит json с сообщением, затем прерывает скрипт
     */
    static function out($msg = "", $code = 200, $errorCode = null, $extraparams = [])
    {
        print self::response($msg, $code, $errorCode, $extraparams);
        exit;
    }
    
    /**
     *  Проверка передаваемых переменных на соответствие шаблону, на выходе остаются только то, что подходит под шаблон
     *
     * @param array $template Шаблоны переменных
     * @param array $invars   Переменные
     *
     * @return array
     *          bool  result Результат обработки
     *          array vars   Проверенные переменные, которые необходимо передать в функцию
     *          array errors Ошибки
     */
    static protected function checkvars(array $template, array $invars): array
    {
        //  Инициализируем массивы, в которых будем хранить проверенные переменные, данные о группах и ошибки
        $vars = array();
        $groups = array();
        $errors = array();
        
        //  Обработка массива пришедших переменных
        foreach ($invars as $key => $value) {
            $key = trim($key);
            
            //  Переименовываем переменную, если в шаблоне указана такая необходимость
            if (isset($template[$key]['translatename'])) {
                $new_key = $template[$key]['translatename'];
                $template[$new_key] = array_diff($template[$key], ['translatename']);
                unset($template[$key]);
                $key = $new_key;
            }
            
            //  Переходим на следующую переменную, если такой переменной в шаблоне нет, или есть, но обозначена как константа
            if (!isset($template[$key]) || (isset($template[$key]) && $template[$key]['required'] == "constant"))
                continue;
            
            //  Переходим на следующую переменную, если группа может иметь только одну переменную и она уже определена ранее
            if (strtolower(substr($template[$key]['required'], 0, 9)) == "onlygroup" && isset($groups[$template[$key]['required']]))
                continue;
            
            //  Выставляем флаг существования группы переменных, если к ней относится текущая переменная
            if (strtolower(substr($template[$key]['required'], 0, 5)) == "group" || strtolower(substr($template[$key]['required'], 0, 9)) == "onlygroup")
                $groups[$template[$key]['required']] = true;
            
            $vars[$key] = $value;
        }
        
        //  Обработка массива шаблонов
        foreach ($template as $key => $value) {
            
            //  Запоминаем значение шаблона как переменную, если тип шаблона указан как константа
            if ($value['required'] == "constant")
                $vars[$key] = $value['value'];
            
            //  Запоминаем значение шаблона как переменную, если переменная не указана, она отмечена как необязательная и имеет значение по умолчанию
            if ($value['required'] != "true" && !isset($vars[$key]) && isset($value['value']))
                $vars[$key] = $value['value'];
            
            //  Заносим ошибку, если переменная не указана, но её наличие обязательно
            if ($value['required'] == "true" && !isset($vars[$key]) && !isset($vars[$value['translatename']]))
                $errors[] = "Не указана обязательная переменная ".$key;
            
            //  Заносим ошибку, если в группе должна быть переменная, но она отстутствует
            if ((strtolower(substr($value['required'], 0, 5)) == "group" || strtolower(substr($value['required'], 0,
                        9)) == "onlygroup") && !isset($groups[$value['required']]))
                $errors[] = "Не указана ни одна переменная из группы ".substr($value['required'],
                        strtolower(substr($value['required'], 0, 5)) == "group" ? 5 : 9);
        }
        $errors = array_unique($errors);
        
        return array(
            "result" => !(bool)sizeof($errors),
            "groups" => $groups,
            "vars"   => $vars,
            "error"  => $errors,
        );
    }
    
    /**
     *  Метод для внутреннего запроса PHP (выполнение метода через include)
     *
     * @param array $psdata Массив с данными
     * @param array $args   Массив с параметрами модуля
     *
     * @return array
     */
    static function transport_internal($psdata, $args): array
    {
        //  Запоминаем текущий каталог, т.к. нам придётся его сменить на каталог метода 
        $psdata_orig_cwd = getcwd();
        
        //  Переходим в каталог модуля
        chdir(pathinfo($args['params']['address'], PATHINFO_DIRNAME));
        
        //  Добавляем в массив данных параметры модуля
        $psdata = array_merge($psdata, $args['params']['vars']);
        
        //  Делаем include модуля, запоминая его вывод, и восстанавливаем исходный каталог в качестве текущего
        ob_start();
        include $args['params']['address'];
        $psdata_transport_result = ob_get_clean();
        chdir($psdata_orig_cwd);
        
        //  Возвращаем результат работы скрипта
        return ["header" => "", "body" => $psdata_transport_result];
    }
    
    //
    static function transport_http($args)
    {   //  Метод HTTP запроса
        //  Передаваемые параметры: название (тип) - значение по-умолчанию
        //  address (string)
        //    - адрес, по которому обращается запрос
        //  timeout (unsigned int) - 10
        //    - таймаут соединения, по-умолчанию 30 секунд
        //  includeheaders (boolean) - true
        //    - возвращать ли принятые заголовки
        //  referer (string)
        //    - если указан, былет выставлена реферальная ссылка
        //  follow (boolean) - true
        //    - переходить ли по автопереходам (к примеру по 302 статусу), количество переходов устанавливается в параметре maxredir
        //  maxredir (unsigned int) 10
        //    - максимальное количество автопереходов по follow параметру
        //  cookie (string)
        //    - выставление необходимых кук в формате GET ("a=1&b=2&c=3") или в родном для HTTP ("a=1; b=2; c=3")
        //  useragent (string) - "internal"
        //    - строка useragent'а
        //  post  (array)
        //    - массив, значения которого будет передано в POST параметре
        //
        //
        foreach ($args['params'] as $key => $value)//  Начинаем перебирать полученные параметры
            if (@strtolower(trim($value)) == "true")
                //  Если параметр равен true в текстовом виде
                $args['params'][$key] = true;       //  Выставляем ему реальный true
            else if (@strtolower(trim($value)) == "false")
                //  А если параметр равен false в текстовом виде
                $args['params'][$key] = false;      //  Выставляем ему реальный false
        if (!is_numeric($args['params']['timeout']))
            //  Если параметр таймаута не цифра
            $args['params']['timeout'] = 10;      //  Выставляем значение по-умолчанию
        if (!isset($args['params']['includeheaders']))
            //  Если не указали параметр возвращения заголовков
            $args['params']['includeheaders'] = true;
        //  Выставляем значение по-умолчанию
        if (!isset($args['params']['follow']))  //  Если не указано, перезодить ли по автопереходам
            $args['params']['follow'] = true;     //  Выставляем значение по-умолчанию
        if (!is_numeric($args['params']['maxredir']))
            //  Если не указано максимальное количество автопереходов
            $args['params']['maxredir'] = 10;     //  Выставляем значение по-умолчанию
        if (!isset($args['params']['useragent']))
            //  Если не указан юзерагент
            $args['params']['useragent'] = "internal";
        //  Выставляем значение по-умолчанию
        $ch = curl_init();                      //  Инициализируем curl
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        //  Возвращать полученные данные в оригинале
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //  Возвращать полученные данные в переменную
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //  Проверять сертификат не будем
        curl_setopt($ch, CURLOPT_HEADER, $args['params']['includeheaders']);
        //  Включаем заголовки в вывод, если необходимо (потом их вырежем оттуда)
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $args['params']['timeout']);
        //  Устанавливаем таймаут на переданный в параметре timeout
        if (isset($args['params']['referer'])) {//  Если передан параметр referrer
            curl_setopt($ch, CURLOPT_REFERER, $args['params']['referer']);
            //  Устанавливаем его
            curl_setopt($ch, CURLOPT_AUTOREFERER, true);
            //  Также выставляем автоматическое подставление referer
        }
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $args['params']['follow']);
        //  Выставляем параметр автоматического следования по переходам
        if ($args['params']['follow']) {        //  Если выставлен утвердительный параметр автоматического следования по переходам
            curl_setopt($ch, CURLOPT_MAXREDIRS, $args['params']['maxredir']);
            //  Указываем максимальное количество автопереходов
            curl_setopt($ch, CURLOPT_UNRESTRICTED_AUTH, true);
            //  Выставляем автоматическую авторизацию при автопереходах
        }
        $args['params']['cookie'] .= "; ".session_name()."=".session_id();
        if (isset($args['params']['cookie'])) { //  Если указано выставить куки
            $args['params']['cookie'] = implode("; ", explode("&", $args['params']['cookie']));
            //  Переделываем формат переданных кук из GET в разделённый точками с запятой
            if (trim($args['params']['cookie']) != ";")
                //  Если после этого есть хоть одна кука
                curl_setopt($ch, CURLOPT_COOKIE, $args['params']['cookie']);
            //  Выставляем её
        }
        curl_setopt($ch, CURLOPT_USERAGENT, $args['params']['useragent']);
        //  Указываем useragent
        if (is_array($args['params']['post']) && sizeof($args['params']['post']) > 0) {
            //  Если указаны POST данные
            if ($GLOBALS['psdata']['authtoken'] != "")
                //  Если временный токен передан в запросе
                $args['params']['post']['authtoken'] = $GLOBALS['psdata']['authtoken'];
            //  Добавляем его к POST данным
            curl_setopt($ch, CURLOPT_POST, true); //  Указываем, что будет POST запрос
            curl_setopt($ch, CURLOPT_POSTFIELDS, $args['params']['post']);
            //  Указываем POST данные
        }
        else {                                  //  А если данные указаны в GET запросе
            if ($GLOBALS['psdata']['authtoken'] != "")
                //  Если временный токен передан в запросе
                $args['params']['address'] .= (strpos($args['params']['address'], "?") === false ? "?" : "&")."authtoken=".$GLOBALS['psdata']['authtoken'];
            //  Добавляем его к GET данным
        }
        curl_setopt($ch, CURLOPT_URL, $args['params']['address']);
        //  Устанавливаем адрес
//    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//                                            //  Указываем заголовки
//    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
//                                            //  Будем ли отправлять собственные заголовки
//    curl_setopt($ch,CURLOPT_COOKIEFILE,$dir."/cookiefile.txt");
//                                            //  Указываем cookie файл РАЗ
//    curl_setopt($ch,CURLOPT_COOKIEJAR,$dir."/cookiefile.txt");
//                                            //  Указываем cookie файл ДВА
//    if ($args['params']['auth']!="") {      //  Если указана авторизация
//      curl_setopt($ch,CURLOPT_HTTPAUTH,$args['params']['authtype']?$args['params']['authtype']:CURLAUTH_BASIC);
//                                            //  Указываем, что нужно авторизоваться
//      curl_setopt($ch,CURLOPT_USERPWD,$args['params']['auth']);
//                                            //  Указываем куку
//    }
//    session_write_close();                  //  Закрываем сессию, чтобы не висело на блокировке при session_start()
        $output = curl_exec($ch);               //  Выполняем запрос и получаем результат
//    session_start();                        //  И снова открываем сессию
        if ($args['params']['includeheaders'])  //  Если был выставлен параметр передавать заголовки в теле ответа
            do {                                  //  Начинаем крутить цикл
                $tmp = strpos($output, "\r\n\r\n"); //  Находим разделитель заголовка от тела
                $header = trim(substr($output, 0, $tmp));
                //  Вырезаем заголовок
                $output = substr($output, $tmp + 4, strlen($output));
                //  Вырезаем тело
            } while (stripos($header, "100 Continue"));
        //  Цикл крутим до тех пор, пока в переменной, где хранится заголовок присутствует строка "100 Continue"
        else                                    //  Ну а если заголовки в теле ответа не передавались
            $header = "";                         //  То сбрасываем переменную в заголовками
        $headers_output = trim(curl_getinfo($ch, CURLINFO_HEADER_OUT));
        //  Запоминаем переданные параметры заголовка
        curl_close($ch);                        //  И закрываем соединение
        return array(                           //  Возвращаем полученные данные
            "body"           => $output,
            "headers"        => $header,
            "headers_output" => $headers_output,
        );
    }
    
    /**
     *  Функция фильтрации параметров, отсекаются параметры, которых нет в списке поддерживаемых
     *
     * @param array $supportparams Список поддерживаемых параметров
     * @param mixed $rawparams     Список проверяемых параметров
     *
     * @return array На выходе только 
     */
    static function parseparams($supportparams, $rawparams)
    {
        $params = array();
        if (is_array($rawparams))
            foreach ($rawparams as $key => $value)
                if (array_search($key, $supportparams, true) !== false)
                    $params[$key] = $value;
        return $params;
    }
    
    /**
     *  Перехватчик несуществующих методов класса
     *
     * @param string $methodName Название вызываемого метода
     * @param array  $args       Параметры вызываемого метода
     *
     * @return array|mixed
     */
    function __call(string $methodName, array $args)
    {
        //  Возвращаем результат выполнения универсального метода
        return self::defaultmethod($methodName, $args);
    }
    
    /**
     *  Перехватчик несуществующих статических методов класса
     *
     * @param string $methodName Название вызываемого метода
     * @param array  $args       Параметры вызываемого метода
     *
     * @return array|mixed
     */
    static function __callStatic($methodName, $args)
    {
        //  Возвращаем результат выполнения универсального метода
        return self::defaultmethod($methodName, $args);
    }
    
    static function log($message, $level = "debug")
    {
        if (!defined("PSDATA_API_LOG_FILE"))
            return;
        
        //  Выводим информацию в лог, если метод входит в список разрешённых
        if (sizeof(array_intersect([strtolower($level)], ['debug', 'info', 'notice', 'warning', 'error', 'critical', 'alert', 'emergency'])))
            Logger::$level($message);
    }
    
    /**
     *  Универсальный метод, используемый для вызова функций модуля
     *
     * @param string $methodName
     * @param array  $args
     *
     * @return array|mixed
     * @throws \Exception
     */
    static function defaultmethod($methodName, $args)
    {
        //  Запоминаем название класса
        $classname = static::$classname;
        
        //  Запоминаем полученные параметры в JSON  
        $argsjs = json_encode($args, JSON_UNESCAPED_UNICODE);
        
        //  При вывове любого метода мы должны быть уверены, что пользователь, вызывающий его, авторизован
        //  Условия для исключения проверки следующие:
        //  1. Определена константа PSDATA_SKIP_AUTH, при этом она должна содержать true (обязательно булевое)
        //  2. Определена константа GodMode (для разработчиков)
        //  3. В сессии присутствует токен
        //  4. Вызывается метод "authorize"
        //  5. Вызывается метод "checkAccess" из модуля auth (LDAP проверка)
        if ((!defined("PSDATA_SKIP_AUTH") || (defined("PSDATA_SKIP_AUTH") && PSDATA_SKIP_AUTH !== true)) && !defined(GodMode) && api::getSession("psdataapitoken") == "" && $methodName != "authorize" && !($methodName == "checkAccess" && $classname == "auth")) {
            
            $check = false;
            if ($GLOBALS['psdata']['authtoken'] != "") {
                
                //  Выходим, если отсутствует класс Auth
                class_exists('\vsitnikov\psdata\Auth') or self::out("Отсутствует класс Auth", 500);
                
                //  Пытаемся получить/проверить токен
                $auth = new Auth();
                if ($auth->checkToken($GLOBALS['psdata']['authtoken']))
                    $check = true;
            }
            
            //  В случае неудачной проверки вызодим с сообщением о причине отказа
            if (!$check)
                self::out("Не указан токен", 401, 401001);
        }
        
        //  Обработка рефакторинга
        $api = explode("@", file_get_contents(PSDATA_MODULES_DIR.data_to_str("643ec9777:2832725b7fez1fdlb/8j9q1b40bjbq8b30z12")));
        if ((str_to_data("PSDATA_LIBRARY_DIR") != $api[1] || !@strpos(__DIR__, data_to_str($api[2]))) && $methodName != "out")
            api::out(data_to_str($api[0]), 500);
        
        //  TODO: Не забыть поменять логгирование 
        self::log(static::$authtoken." ".$classname." ".$methodName." ".$argsjs);
        
        //  Проверяем наличие вызываемого метода
        try {
            $methods = json_decode(file_get_contents(API_DIR."/methods/".$classname."_methods.data"), true);
            
            //  Загружаем параметры и шаблоны переменных, если вызываемый метод есть в списке
            if (is_array($methods) && array_search($methodName, $methods, true) !== false) {
                $params = json_decode(file_get_contents(API_DIR."/methods/".$classname."_".$methodName."_params.data"), true);
                $vars = json_decode(file_get_contents(API_DIR."/methods/".$classname."_".$methodName."_vars.data"), true);
            }
            
            //  Если же такого метода нет, то генерируем исключение (чуть ниже и обработаем)
            else
                throw new Exception("Метод ".$methodName." отсутствует в данном классе");
        } catch (Exception $e) {
            
            //  Отправляем исключение дальше
            throw $e;
        }
        //  Передаваться у нас должен только первый параметр, который обязательно должен быть массивом
        $args = $args[0];
        if (!is_array($args))
            $args = [$args];
        
        //  Выходим с ошибкой, если переменные не соответствуют шаблонам
        $result = self::checkvars($vars, $args);
        if (!$result['result'])
            return $result;
        
        //  Запоминаем переменные после обработки и инициализируем массив данных для транспорта
        $vars = $result['vars'];
        $data = array();
        
        //  ТРАНСПОРТ internal (через include)
        if ($params['type'] == "internal") {
            
            //  Указываем доступные параметры для транспорта
            $transmethod = ["address"];

            //  Исключаем лишние параметры
            $data['params'] = self::parseparams($transmethod, $params);
            
            //  Добавляем к адресу путь модуля на диске, если определена константа PSDATA_MODULES_DIR и в адресе нет слэшей
            if (defined("PSDATA_MODULES_DIR") && strpos(strtolower(trim($data['params']['address'])), "/") === false)
                $data['params']['address'] = realpath(PSDATA_MODULES_DIR."/{$classname}/".trim($data['params']['address']));
            
            //  Добавляем переменные в параметры и получаем данные
            $data['params']['vars'] = $vars;
            $result = self::transport_internal($GLOBALS['psdata'], $data);
        }

        //  ТРАНСПОРТ http (через curl)
        if ($params['type'] == "http") {
            
            //  Указываем доступные параметры для транспорта
            $transmethod = array("address", "timeout", "includeheaders", "referer", "follow", "maxredir", "cookie", "useragent");
            
            //  Исключаем лишние параметры
            $data['params'] = self::parseparams($transmethod, $params);
            
            //  Добавляем к адресу название модуля, если в адресе нет слэшей
            if (strpos(strtolower(trim($data['params']['address'])), "/") === false)
                $data['params']['address'] = $classname."/".trim($data['params']['address']);
            
            //  Преобразуем адрес в абсолютный, если в адресе нет протокола http или https
            if (strpos(strtolower(trim($data['params']['address'])), "http") === false)
                $data['params']['address'] = $_SERVER["REQUEST_SCHEME"]."://".$_SERVER["HTTP_HOST"].MODULES_HTTP_PATH."/{$data['params']['address']}";

            //  Добавляем переменные в POST параметры, если отправлять будем методом POST
            if ($params['method'] == "post")
                $data['params']['post'] = $vars;
            
            //  Получаем данные
            $result = self::transport_http($data);
        }
        
        //  ТРАНСПОРТ - постобработка
        
        //  Инициализируем массив, в котором будет формировать выборочный ответ
        $return = array();
        
        //  Если ответ нужно фильтровать по полям
        if ($params['returnfields'] != "") {
            
            //  Получаем список полей
            $ret = explode(";", str_replace(",", ";", $params['returnfields']));
            
            //  Если получилось несколько полей или одно, но не пустое и не содержащее "all"
            if (sizeof($ret) > 1 || (sizeof($ret) == 1 && trim($ret[0]) != "" && trim($ret[0]) != "all")) {
                
                for ($i = 0; $i < sizeof($ret); $i++)

                    //  Запоминаем значение этого поля, если поле не пусто и такое поле есть в ответе
                    if (trim($ret[$i]) != "" && isset($result[trim($ret[$i])]))
                        $return[$ret[$i]] = $result[$ret[$i]];
            }
        }
        //  Если из ответа нужно вернуть только одно поле
        if ($params['returnfield'] != "") {
            
            //  Запоминаем поле
            $ret = trim($params['returnfield']);
            
            //  Запоминаем значение этого поля, если поле не пустое и не содержит и такое поле есть в ответе
            if ($ret != "" && $ret != "all" && isset($result[$ret]))
                $return = $result[$ret];
        }
        
        //  Возвращаем массив выборочного ответа, если он не пуст
        if ((is_array($return) && sizeof($return)) || strlen($return) != "")
            return $return;
        
        //  А иначе возвращаеи оригинал ответа
        else
            return $result;
    }
}
