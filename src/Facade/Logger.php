<?php

namespace PSData;

/**
 * Class Logger
 *
 * @package PSData
 */
abstract class Logger
{
  private static $logger = null;

  public static function configure(array $config)
  {
    if (is_null(static::$logger)) {
      static::$logger = Logger\LoggerFactory::create($config);
    }
  }

  public static function __callStatic(string $method, array $args)
  {
      return is_null(static::$logger) ? null : static::$logger->$method(...$args);
  }
}
