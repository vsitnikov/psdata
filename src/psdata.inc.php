<?php
/******************************************************************************
 *
 * (C) 2015-2017 by Vyacheslav Sitnikov (vyacheslav.sitnikov@megafon.ru)
 *
 ******************************************************************************/

namespace MegaFon\PSData;

header("Content-Type: text/html; charset=utf-8");

//  Выставляем кодировку в UTF-8, если не выставлена по-умолчанию
if (!defined("ENCODE"))
    define("ENCODE", "UTF-8");

//  Абсолютный путь к корню сайта
if (!defined("ROOT_DIR"))
    define("ROOT_DIR", realpath(dirname(__FILE__)."/.."));

//  Путь к базовому каталогу PSDATA
if (!defined("PSDATA_DIR"))
    define("PSDATA_DIR", realpath(dirname(__FILE__)));

//  Путь к каталогу библиотек
if (!defined("PSDATA_LIBRARY_DIR"))
    define("PSDATA_LIBRARY_DIR", PSDATA_DIR."/lib");

//  Путь к каталогу классов
if (!defined("PSDATA_CLASSES_DIR"))
    define("PSDATA_CLASSES_DIR", PSDATA_DIR."/classes");

//  Путь к каталогу модулей
if (!defined("PSDATA_MODULES_DIR"))
    define("PSDATA_MODULES_DIR", PSDATA_DIR."/modules");

//  Игнорирование запроса авторизации при вызове модулей
if (!defined("PSDATA_SKIP_AUTH"))
    define("PSDATA_SKIP_AUTH", false);

//  Кэшировать весь вывод
if (!defined("PSDATA_CACHE_BUFFER_OUTPUT"))
    define("PSDATA_CACHE_BUFFER_OUTPUT", true);

//  http путь к каталогу psdata
if (!defined("PSDATA_HTTP_PATH"))
    define("PSDATA_HTTP_PATH", "/psdata");

//  http путь к каталогу модулей
if (!defined("MODULES_HTTP_PATH"))
    define("MODULES_HTTP_PATH", PSDATA_HTTP_PATH."/modules");

//  Полный путь к каталогу JavaScript                                        
if (!defined("JS_DIR"))
    define("JS_DIR", PSDATA_MODULES_DIR."/js");

//  Полный путь к каталогу API
if (!defined("API_DIR"))
    define("API_DIR", PSDATA_MODULES_DIR."/api");

//  Хост почтовика
if (!defined("MAIL_HOST"))
    define("MAIL_HOST", "vlg-ums.megafon.ru");

//  Выходим, если у нас нет файла констант
file_exists(PSDATA_DIR."/constant.php") or die("Восстановите файл psdata/constant.php из каталога psdata/constants");

//  Подгружаем константы среды
require_once(PSDATA_DIR."/constant.php");

//  Регистрируем функцию автозагрузки
spl_autoload_register(function ($className) {
    $classPath = PSDATA_CLASSES_DIR."/".implode("/", explode("\\", $className)).".php";
    if (file_exists($classPath))
        require_once($classPath);
});

//  Регистрируем функцию автозагрузки в нижнем регистре
spl_autoload_register(function ($className) {
    $classPath = PSDATA_CLASSES_DIR."/".implode("/", explode("\\", strtolower($className))).".php";
    if (file_exists($classPath))
        require_once($classPath);
});

//  Проводим инициализацию
require_once(PSDATA_DIR."/start.php");

//  Подгружаем библиотеки внутренних функций
require_once(PSDATA_LIBRARY_DIR."/lib_main.php");
require_once(PSDATA_LIBRARY_DIR."/lib_str.php");

//  Подгружаем API
require_once(API_DIR."/api.php");          
