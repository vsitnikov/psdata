<?php
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/
 
namespace PSData;

use Api as api;

require_once(PSDATA_LIBRARY_DIR."/lib_str.php");

/**
 * Класс запроса к БД MySQL
 *
 * @author    Vyacheslav Sitnikov <sitnikov@gmail.com>
 * @version   1.0
 * @package   PSData
 * @copyright Copyright (c) 2019 Vyacheslav Sitnikov
 */
class MySQLQuery
{
    /** @var string тип БД */
    const type = "mysql";

    /** @var string Имя БД */
    private $tns = null;
    
    /** @var string Пользователь (схема) БД */
    private $scheme = null;
    
    /** @var string Сервер БД */
    private $server = null;
    
    /** @var bool Успешность соединения с БД */
    public $init = false;
    
    /** @var \mysqli Ресурс соединения с БД */
    public $conn;
    
    /** @var resource Ресурс курсора БД */
    public $cursor;
    
    /** @var string Лог работы с БД */
    public $log;
    
    /** @var string Очищать лог при каждом запросе*/
    public $log_autoclear = false;
    
    /** @var string Текст запроса к БД */
    public $query;
    
    /** @var array Массив передаваемых запросу параметров */
    public $param = array();
    
    /** @var array Массив возвращаемых запросом параметров */
    public $return = array();
    
    /** @var array Результат запроса (отсутствует, если нужно вернуть данные в short формате) */
    public $result = array();
    
    /** @var bool Необходимо ли возвращать результат запроса в формате fetch_all */
    public $short_result = false;
    
    /** @var bool Флаг отображения SQL запросов */
    public $debug = false;
    
    /** @var string Путь к файлу, в который необходимо записывать результат */
    public $save_to_file = "";
    
    /** @var string Разделитель полей при записи в файл */
    public $save_to_file_separator = ";";
    
    /** @var bool Признак вывода ошибки на экран */
    public $show_error = true;
    
    /** @var bool Признак прекращения работы при ошибке */
    public $stop_on_error = true;
    
    /**
     *  Отчёт об ошибке
     *
     * Функция вызывается при ошибке и, в зависимости от переменных класса $show_error и $stop_on_error, при ошибке выводит её и/или останавливает скрипт
     *
     * @return void
     */
    function reportError()
    {
        $out = "Код ошибки: ".mysqli_errno($this->conn)."\n";
        $out .= "Сообщение ошибки: ".mysqli_error($this->conn)."\n";
        $out .= "Запрос: ".$this->query."\n";
        $this->log .= $out;
        if ($this->show_error)
            print mysqli_error($this->conn)."\n";
        if ($this->stop_on_error)
            exit;
    }
    
    /**
     * MySQLQuery конструктор
     *
     * Все параметры не обязательны, в случае их отсутствия они (кроме пароля) выставятся из констант (указаны в скобках)
     *
     * @param string $tns           (optional) Название БД (MYSQL_DATABASE_NAME)
     * @param string $scheme        (optional) Имя пользователя (MYSQL_DATABASE_LOGIN)
     * @param string $passwd        (optional) Пароль пользователя
     * @param string $server        (optional) Адрес сервера (MYSQL_DATABASE_SERVER)
     * @param string $character_set (optional) Кодировка
     */
    function __construct($tns = "", $scheme = "", $passwd = "", $server = "", $character_set = null)
    {
        //  Выставляем кодировку в зависимости от константы, если была выставлена дефолтная кодировка
        $character_set = $character_set ?? "utf8";
        
        //  Обработка ситуации, когда параметры переданы в массиве (возвращаемый функцией GetSQLDataFromUniversalData)
        if (is_array($tns) && (isset($tns['scheme']) || isset($tns['username'])) && isset($tns['tns']) && isset($tns['server']) && (isset($tns['passwd']) || isset($tns['password']) || isset($tns['cpasswd']))) {
            if ($scheme != "")
                $character_set = $scheme;
            $server = $tns['server'];
            $passwd = isset($tns['cpasswd']) ? data_to_str($tns['cpasswd']) : (isset($tns['passwd']) ? $tns['passwd'] : $tns['password']);
            $scheme = isset($tns['scheme']) ? $tns['scheme'] : $tns['username'];
            $tns = $tns['tns'];
        }
        elseif (is_array($tns))
            $tns = $tns['tns'];
        
        //  Обработка модуля KeePass
        if (substr(strtolower($tns), 0, 15) == "module:keepass:") {
            api::moduleExists("keepass") or api::out("Отсутствует модуль KeePass", 500);
            
            //  Получаем путь в базе KeePass, отрезая префикс "module:keepass:"
            $path = trim(substr($tns, 15));
            
            //  Получаем данные из KeePass
            $data = json_decode(\keepass::getSqlData(["path" => $path]), true);
            list($tns, $scheme, $passwd, $server) = array($data['tns'], $data['scheme'], $data['passwd'], $data['server']);
        }
        
        //  Обработка универсального метода
        if (substr(strtolower($tns), 0, 10) == "universal:") {
            
            //  Получаем путь к данным, отрезая префикс "universal:"
            $path = trim(substr($tns, 10));
            
            $data = $this->GetSQLDataFromUniversalData($path);
            list($tns, $scheme, $server, $passwd) = array($data['tns'], $data['scheme'], $data['server'], data_to_str($data['cpasswd']),);
        }
        if ($tns != "")
            $this->tns = $tns;
        if ($scheme != "")
            $this->scheme = $scheme;
        if ($server != "")
            $this->server = $server;
        $this->conn = new \mysqli($this->server, $this->scheme, $passwd, $this->tns);
        if (mysqli_connect_errno()) {
            $this->log .= "Connection failed! ".mysqli_connect_error();
            $this->init = false;
        }
        else
            $this->init = true;
        if (DEFAULT_SQL_DEBUG === true)
            $this->debug = true;
        $this->exec("SET NAMES {$character_set}");
    }
    
    /**
     *  Выполнение запроса на базе данных
     *
     * @param string $query [optional] Текст запроса (если не задан, воьмётся из переменной класса $this->query)
     * @param array  $param [optional] Переменные запроса
     *
     * @return array|bool
     */
    function exec($query = "", $param = array())
    {
        $this->result = [];
        if ($this->log_autoclear)
            $this->log = "";
        if ($query != "")
            $this->query = $query;
        $this->query = str_replace("\r", "", $this->query);
        $this->param = $param;
        if ($this->debug) {
            print "Begin: ".date("Y-m-d h:i:s")."<br>\n";
            print "&lt;<pre style='color: red'>".$this->query."</pre>&gt;<br>";
            print "&lt;<pre style='color: green'>";
            print_r($this->param);
            print "</pre>&gt;<br>";
        }
        $stmt = $this->conn->stmt_init();
        $stmt->prepare($this->query);
        
        //  Обработка переменных запроса
        $ref_array = [''];
        if (is_array($this->param) && sizeof($this->param)) {
            for ($i = 0; $i < sizeof($this->param); $i++) {
                if (is_float($this->param[$i]))
                    $ref_array[0] .= 'd';
                elseif (is_int($this->param[$i]))
                    $ref_array[0] .= 'i';
                else
                    $ref_array[0] .= 's';
                $ref_array[] = $this->param[$i];
            }
            $ref_class = new \ReflectionClass('mysqli_stmt');
            $method = $ref_class->getMethod("bind_param");
            $method->invokeArgs($stmt, $ref_array);
        }
        
        //  Выполнение запроса и обработка ошибок
        if (!$execute_result = $stmt->execute()) {
            if ($this->debug)
                print "End: ".date("Y-m-d h:i:s")."<br>\n";
            $this->reportError();
        }
        $result = $stmt->get_result();
        
        //  Убираем возможные комментарии в запросе перед проверкой на select
        $sql_text = preg_replace("/\-\-.*?$/mi", "", $this->query);
        if (substr(strtolower(trim($sql_text)), 0, 6) == "select") {
            $handle = null;
            if ($this->save_to_file) {
                @unlink($this->save_to_file);
                $handle = fopen($this->save_to_file, "a");
            }
            $count_row = 0;
            $return_data = array();
            while ($record = @$result->fetch_array(MYSQLI_ASSOC)) {
                $line = "";
                $count_column = 0;
                foreach ($record as $key => $value) {
                    if ($this->save_to_file)
                        $line .= ($line != "" ? $this->save_to_file_separator : "").$value;
                    else {
                        if (!$this->short_result)
                            $this->result[$key][] = $value;
                        if ($count_row)
                            $return_data[$count_row][$count_column] = $value;
                        else {
                            $return_data[$count_row][$count_column] = $key;
                            $return_data[$count_row + 1][$count_column] = $value;
                        }
                    }
                    $count_column++;
                }
                $line .= "\r\n";
                if ($this->save_to_file)
                    fwrite($handle, $line);
                $count_row += !$count_row ? 2 : 1;
            }
            
            //  Высвобождаем память после запроса и закрываем курсор
            @$stmt->free_result();
            @$stmt->close();
            
            if ($this->save_to_file) {
                fclose($handle);
                return true;
            }
            //  Возвращаем результат, если он есть, если нет, то возвращаем логический результат запроса
            return sizeof($return_data) ? $return_data : $execute_result;
        }
        return true;
    }
    
    /**
     *  Получение пароля универсальным способом (не рекомендуется)
     *
     * На примере USER@BASE
     * Сначала проверяется константа BASE_USER_USERNAME, если такая найдена, то из неё берётся схема пользователя, из BASE_USER_PASSWORD - пароль,
     * BASE_USER_DATABASE - название БД, BASE_USER_SERVER - адрес сервера БД Если же такой константы нет, то ту же проверку проходит константа USER_USERNAME (и
     * USER_PASSWORD, USER_DATABASE и USER_SERVER)
     *
     * @param string $path            Указатель на БД, состоящий из идентификаторов пользователя и БД, разделённых @
     * @param bool   $return_oly_pass [optional] Признак возврата только пароля
     *
     * @return array|bool При совпадении возвращается массив, содержащий схему пользователя и шифрованный пароль (или просто пароль, если установлен
     *                    соответствующий флаг), если же совпадения не было, возвращается false
     */
    private function GetSQLDataFromUniversalData($path, $return_oly_pass = false)
    {
        list($scheme, $tns) = explode("@", $path);
        if (defined(strtoupper($tns."_".$scheme."_USERNAME"))) {
            $username = constant(strtoupper($tns."_".$scheme."_USERNAME"));
            $pass = constant(strtoupper($tns."_".$scheme."_PASSWORD"));
            $tns = constant(strtoupper($tns."_".$scheme."_DATABASE"));
            $server = constant(strtoupper($tns."_".$scheme."_SERVER"));
        }
        else if (defined($scheme."_USERNAME")) {
            $username = constant($scheme."_USERNAME");
            $pass = constant($scheme."_PASSWORD");
            $tns = constant(strtoupper($scheme."_DATABASE"));
            $server = constant(strtoupper($scheme."_SERVER"));
        }
        else
            return false;
        if ($return_oly_pass)
            return $pass;
        return array("tns" => $tns, "scheme" => $username, "cpasswd" => $pass, "server" => $server);
    }
}
