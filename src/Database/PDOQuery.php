<?php
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

namespace PSData;

//  Будем использовать класс Api
use Api as api;
use PDO;
use PDOException;

require_once(PSDATA_LIBRARY_DIR."/lib_str.php");

/**
 * Класс запроса к БД (через PDO драйвер)
 *
 * @author    Vyacheslav Sitnikov <vsitnikov@gmail.com>
 * @version   1.1
 * @package   vsitnikov
 * @copyright Copyright (c) 2018
 */
class PDOQuery
{
    /** @var string тип БД */
    const type = "pdo";
    
    /** @var string Имя БД */
    public $dsn;
    
    /** @var string Пользователь БД */
    public $username;
    
    /** @var bool Успешность соединения с БД */
    public $init = false;
    
    /** @var \PDO Ресурс соединения с БД */
    public $conn;
    
    /** @var \PDOStatement Ресурс курсора БД */
    public $cursor;
    
    /** @var string Лог работы с БД */
    public $log;
    
    /** @var string Очищать лог при каждом запросе */
    public $log_autoclear = false;
    
    /** @var string Текст запроса к БД */
    public $query;
    
    /** @var array Массив передаваемых запросу параметров */
    public $param = array();
    
    /** @var array Массив возвращаемых запросом параметров */
    public $return = array();
    
    /** @var array Результат запроса в формате fetch_all (отсутствует, если нужно вернуть данные в short формате) */
    public $result = array();
    
    /** @var array Список полей, их тип и размерность */
    public $fields = [];
    
    /** @var bool Необходимо ли возвращать результат запроса в формате fetch_all */
    public $short_result = false;
    
    /** @var bool Флаг отображения SQL запросов */
    public $debug = false;
    
    /** @var string Путь к файлу, в который необходимо записывать результат */
    public $save_to_file = "";
    
    /** @var string Разделитель полей при записи в файл */
    public $save_to_file_separator = ";";
    
    /** @var bool Признак вывода ошибки на экран */
    public $show_error = true;
    
    /** @var bool Признак прекращения работы при ошибке */
    public $stop_on_error = true;
    
    /** @var string Вывод dbms_output */
    public $dbms_output;
    
    /** @var string Информация для поля client_info в v$session Oracle */
    public $client_info = "";
    
    /** @var string Информация для поля client_identifier в v$session Oracle */
    public $client_identifier = "";
    
    /** @var string Информация для поля action в v$session Oracle */
    public $action = "";
    
    /** @var string Информация для поля module в v$session Oracle */
    public $module = "";
    
    /** @var bool Признак включения трассировки */
    public $trace = false;
    
    /** @var int Уровень трассировки */
    public $trace_level = 12;
    
    /** @var string Идентификатор трассировки (присутствует в названии файла трассировки) */
    public $trace_ident = "psdata";
    
    /** @var bool Признак, делать ли трассировку различных запросов в одной сессии (true) или в разных (false) */
    public $trace_separate = false;
    
    /**
     *  Отчёт об ошибке
     *  Функция вызывается при ошибке и, в зависимости от переменных класса $show_error и $stop_on_error, при ошибке выводит её и/или останавливает скрипт
     *
     * @param \PDOException $e          Exception
     * @param bool          $on_connect [optional] [false] Если флаг установлен, то при коннекте не выводить ничего, но при этом сохранить в лог
     *
     * @return void
     */
    function reportError($e, $on_connect = false)
    {
        $code = $e->getCode();
        $message = $e->getMessage();
        //$trace = $e->getTraceAsString();
        //$file = $e->getFile();
        //$line = $e->getLine();
        $this->log .= "{$code}: {$message}\n";
        if ($on_connect)
            return;
        if ($this->show_error && trim($message) != "")
            echo "{$code}: {$message}\n";
        if ($this->stop_on_error)
            exit;
    }
    
    /**
     * Конструктор
     *
     * Все параметры не обязательны, в случае их отсутствия они (кроме пароля) выставятся из констант (указаны в скобках)
     *
     * @param string|array $dsn         Название БД (ORACLE_TNS)
     * @param string       $username    [optional] Имя пользователя (ORACLE_SCHEME)
     * @param string       $password    [optional] Пароль пользователя
     * @param string       $dsn_options [optional] Кодировка
     */
    function __construct($dsn, $username = null, $password = null, $dsn_options = null)
    {
        $this->connect($dsn, $username, $password, $dsn_options);
    }
    
    /**
     * Коннект к БД
     *
     * @param string|array $dsn      DSN БД
     * @param string       $username [optional] Имя пользователя
     * @param string       $password [optional] Пароль пользователя
     * @param string       $options  [optional] Опции для DSN соединения
     */
    function connect($dsn, $username = null, $password = null, $options = null)
    {
        
        //  Хитрожопый метод заполнения переданных параметров функции в массиве первого элемента (копия переданного значения остаётся в $reflectParams)
        //  Если параметр паредан и в массиве и в качестве параметра функции, то значение, переданное в качестве параметра функции, имеет приоритет
        //  Способ не быстрый, но универсальный
        try {
            $reflectionMethod = new \ReflectionMethod(__CLASS__, __FUNCTION__);
        } catch (\ReflectionException $e) {
            $this->init = false;
            return;
        }
        $reflectParams = $reflectionMethod->getParameters();
        $reflectFirstParam = $reflectParams[0]->getName();
        if (is_array($$reflectFirstParam)) {
            for ($i = sizeof($reflectParams) - 1; $i; $i--) {
                $reflectParam = $reflectParams[$i];
                $reflectParamName = $reflectParam->getName();
                
                //  Задаём значение параметру, если он указан в массиве, а его значение не передано, или равно дефолтовому, если это опциональный параметр
                if (isset($$reflectFirstParam[$reflectParamName]) && (is_null($$reflectParamName) || ($reflectParam->isOptional() && $$reflectParamName == $reflectParam->getDefaultValue())))
                    $$reflectParamName = $$reflectFirstParam[$reflectParamName];
            }
        }
        $reflectParams = $$reflectFirstParam;
        if (isset($$reflectFirstParam[$reflectFirstParam]))
            $$reflectFirstParam = $$reflectFirstParam[$reflectFirstParam];
        
        $options = $options ?? [];
        
        //  Обработка модуля KeePass
        if (substr(strtolower($dsn), 0, 15) == "module:keepass:") {
            api::moduleExists("keepass") or api::out("Отсутствует модуль KeePass", 500);
            
            //  Получаем путь в базе KeePass, отрезая префикс "module:keepass:"
            $path = trim(substr($dsn, 15));
            
            //  Получаем данные из KeePass
            $data = json_decode(\keepass::getSqlData(["path" => $path]), true);
            list($dsn, $username, $password) = array($data['tns'], $data['scheme'], $data['passwd']);
        }
        
        //  Обработка универсального метода
        if (substr(strtolower($dsn), 0, 10) == "universal:") {
            
            //  Получаем путь к данным, отрезая префикс "universal:"
            $path = trim(substr($dsn, 10));
            
            $data = $this->GetSQLDataFromUniversalData($path);
            list($dsn, $username, $password) = array($data['tns'], $data['scheme'], data_to_str($data['cpasswd']));
        }
        
        $options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        
        //  Заполняем свойства класса, если есть чем
        if ($dsn != "")
            $this->dsn = $dsn;
        if ($username != "")
            $this->username = $username;
        try {
            $this->conn = new PDO($this->dsn, $this->username, $password, $options);
            $this->init = true;
        } catch (PDOException $e) {
            $this->init = false;
            $this->reportError($e, true);
        }
        if (defined("DEFAULT_SQL_DEBUG") && DEFAULT_SQL_DEBUG === true)
            $this->debug = true;
    }
    
    /**
     *  Выполнение запроса на базе данных
     *
     * @param string $query  [optional] Текст запроса (если не задан, воьмётся из переменной класса $this->query)
     * @param array  $param  [optional] Переменные запроса
     * @param array  $return [optional] Переменные ответа
     *
     * @return array|bool
     */
    function exec($query = "", $param = [], $return = [])
    {
        unset($this->result);
        unset($this->fields);
        if ($this->log_autoclear)
            $this->log = "";
        if ($query != "")
            $this->query = $query;
        $this->query = str_replace("\r", "", $this->query);
        $this->param = $param;
        $this->return = $return;
        if ($this->debug) {
            echo "Begin: ".date("Y-m-d h:i:s")."<br>\n";
            echo "&lt;<pre style='color: red'>".$this->query."</pre>&gt;<br>";
            echo "&lt;<pre style='color: green'>";
            print_r($this->param);
            echo "</pre>&gt;<br>";
        }
        
        try {
            $this->cursor = $this->conn->prepare($this->query);
        } catch (\PDOException $e) {
            $this->reportError($e);
        }
        
        //  Если переданы параметры запроса
        if (is_array($this->param) && sizeof($this->param))
            for ($i = 0; $i < sizeof($this->param); $i++)
                if (strpos($this->query, $this->param[$i]['name']) !== false) {
                    
                    //  Биндим значение, если помимо значения переменной указан её тип и размерность
                    if (isset($this->param[$i]['type']) && is_numeric($this->param[$i]['length']))
                        $this->cursor->bindParam($this->param[$i]['name'], $this->param[$i]['value'], $this->param[$i]['type'], $this->param[$i]['length']);
                    //  Биндим значение, если помимо значения переменной указан только её тип
                    else if (isset($this->param[$i]['type']))
                        $this->cursor->bindParam($this->param[$i]['name'], $this->param[$i]['value'], $this->param[$i]['type']);
                    //  Биндим значение, если помимо значения переменной ничего не указано
                    else
                        $this->cursor->bindParam($this->param[$i]['name'], $this->param[$i]['value']);
                }
        
        //  Если указаны параметры для ответа
        if (is_array($this->return) && sizeof($this->return))
            for ($i = 0; $i < sizeof($this->return); $i++)
                
                @$this->cursor->bindParam($this->return[$i]['name'], $this->return[$i]['value'], $this->return[$i]['type'], 1000000000);
        try {
            $execute_result = @$this->cursor->execute();
        } catch (\PDOException $e) {
            if ($this->debug)
                echo "End: ".date("Y-m-d h:i:s")."<br>\n";
            $this->reportError($e);
        }
        if ($this->debug)
            echo "End: ".date("Y-m-d h:i:s")."<br>\n";
        
        //  Если указаны параметры для ответа
        if (is_array($this->return) && sizeof($this->return)) {
            $return_vars = array();
            for ($i = 0; $i < sizeof($this->return); $i++)
                
                ////  Запоминаем результат работы метода load, если на выходе объект
                //if (is_object($this->return[$i]['value']))
                //    $return_vars[substr($this->return[$i]['name'], 1)] = $this->return[$i]['value']->load();
                ////  А на выходе обычная переменная, то запоминаем её значение
                //else
                $return_vars[substr($this->return[$i]['name'], 1)] = $this->return[$i]['value'];
            
            $this->return = $return_vars;
        }
        
        //  Убираем возможные комментарии в запросе перед проверкой на select
        $sql_text = preg_replace("/\-\-.*?$/mi", "", $this->query);
        if (substr(strtolower(trim($sql_text)), 0, 6) == "select") {
            $handle = null;
            if ($this->save_to_file) {
                @unlink($this->save_to_file);
                $handle = fopen($this->save_to_file, "a");
            }
            $count_row = 0;
            $return_data = array();
            //$this->cursor->setFetchMode(\PDO::FETCH_ASSOC);
            while ($record = @$this->cursor->fetch(\PDO::FETCH_ASSOC)) {
                $line = "";
                $count_column = 0;
                foreach ($record as $key => $value) {
                    if ($this->save_to_file)
                        $line .= ($line != "" ? $this->save_to_file_separator : "").$value;
                    else {
                        if (!$this->short_result)
                            $this->result[$key][] = $value;
                        if ($count_row)
                            $return_data[$count_row][$count_column] = $value;
                        else {
                            $return_data[$count_row][$count_column] = $key;
                            $return_data[$count_row + 1][$count_column] = $value;
                        }
                    }
                    $count_column++;
                }
                $line .= "\r\n";
                if ($this->save_to_file)
                    fwrite($handle, $line);
                $count_row += !$count_row ? 2 : 1;
            }
            
            //  Заполняем данные по колонкам
            $num_cols = $this->cursor->rowCount();
            // if ($num_cols) {
            //     $this->fields = [];
            //     for ($i = 1; $i <= $num_cols; $i++) {
            //         //$this->fields[oci_field_name($this->cursor, $i)] = [
            //         //    "name" => oci_field_name($this->cursor, $i),
            //         //    "type" => strtolower(oci_field_type($this->cursor, $i)),
            //         //    "size" => oci_field_size($this->cursor, $i),
            //         //];
            //         print_r($this->cursor->getColumnMeta($i));
            //     }
            // }
            
            //  Освобождаем память
            @$this->cursor->closeCursor();
            
            if ($this->save_to_file) {
                fclose($handle);
                return true;
            }
            //  Возвращаем результат, если он есть, если нет, то возвращаем логический результат запроса
            return sizeof($return_data) ? $return_data : $execute_result;
        }
        return true;
    }
    
    /**
     *  Получение пароля универсальным способом (не рекомендуется)
     *
     * На примере USER@BASE
     * Сначала проверяется константа BASE_USER_USERNAME, если такая найдена, то из неё берётся схема пользователя, а из BASE_USER_PASSWORD - пароль
     * Если же такой константы нет, то ту же проверку проходит константа USER_USERNAME (и USER_PASSWORD)
     *
     * @param string $path      Указатель на БД, состоящий из идентификаторов пользователя и БД, разделённых @
     * @param bool   $pass_only [optional] Признак возврата только пароля
     *
     * @return array|bool При совпадении возвращается массив, содержащий схему пользователя и шифрованный пароль (или просто пароль, если установлен
     *                    соответствующий флаг), если же совпадения не было, возвращается false
     */
    private function GetSQLDataFromUniversalData($path, $pass_only = false)
    {
        list($scheme, $tns) = explode("@", $path);
        if (defined(strtoupper($tns."_".$scheme."_USERNAME"))) {
            $username = constant(strtoupper($tns."_".$scheme."_USERNAME"));
            $pass = constant(strtoupper($tns."_".$scheme."_PASSWORD"));
        }
        else if (defined($scheme."_USERNAME")) {
            $username = constant($scheme."_USERNAME");
            $pass = constant($scheme."_PASSWORD");
        }
        else
            return false;
        if ($pass_only)
            return $pass;
        return array("tns" => $tns, "scheme" => $username, "cpasswd" => $pass);
    }
}
