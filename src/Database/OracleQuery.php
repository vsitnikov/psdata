<?php
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

namespace PSData;

use Api as api;                             //  Будем использовать класс Api

require_once(PSDATA_LIBRARY_DIR."/lib_str.php");

/**
 * Класс запроса к БД Oracle
 *
 * @author    Vyacheslav Sitnikov <sitnikov@gmail.com>
 * @version   1.0
 * @package   PSData
 * @copyright Copyright (c) 2019 Vyacheslav Sitnikov
 */
class OracleQuery
{
    /** @var string тип БД */
    const type = "oracle";
    
    /** @var string Имя БД */
    public $tns = null;
    
    /** @var string Пользователь (схема) БД */
    public $scheme = null;
    
    /** @var bool Успешность соединения с БД */
    public $init = false;
    
    /** @var resource Ресурс соединения с БД */
    public $conn;
    
    /** @var resource Ресурс курсора БД */
    public $cursor;
    
    /** @var string Лог работы с БД */
    public $log;
    
    /** @var string Очищать лог при каждом запросе */
    public $log_autoclear = false;
    
    /** @var string Текст запроса к БД */
    public $query;
    
    /** @var array Массив передаваемых запросу параметров */
    public $param = array();
    
    /** @var array Массив возвращаемых запросом параметров */
    public $return = array();
    
    /** @var array Результат запроса в формате fetch_all (отсутствует, если нужно вернуть данные в short формате) */
    public $result = array();
    
    /** @var array Список полей, их тип и размерность */
    public $fields = [];
    
    /** @var bool Необходимо ли возвращать результат запроса в формате fetch_all */
    public $short_result = false;
    
    /** @var bool Флаг отображения SQL запросов */
    public $debug = false;
    
    /** @var string Путь к файлу, в который необходимо записывать результат */
    public $save_to_file = "";
    
    /** @var string Разделитель полей при записи в файл */
    public $save_to_file_separator = ";";
    
    /** @var bool Признак вывода ошибки на экран */
    public $show_error = true;
    
    /** @var bool Признак прекращения работы при ошибке */
    public $stop_on_error = true;
    
    /** @var string Вывод dbms_output */
    public $dbms_output;
    
    /** @var string Информация для поля client_info в v$session Oracle */
    public $client_info = "";
    
    /** @var string Информация для поля client_identifier в v$session Oracle */
    public $client_identifier = "";
    
    /** @var string Информация для поля action в v$session Oracle */
    public $action = "";
    
    /** @var string Информация для поля module в v$session Oracle */
    public $module = "";
    
    /** @var bool Признак включения трассировки */
    public $trace = false;
    
    /** @var int Уровень трассировки */
    public $trace_level = 12;
    
    /** @var string Идентификатор трассировки (присутствует в названии файла трассировки) */
    public $trace_ident = "psdata";
    
    /** @var bool Признак, делать ли трассировку различных запросов в одной сессии (true) или в разных (false) */
    public $trace_separate = false;
    
    /**
     *  Отчёт об ошибке
     *  Функция вызывается при ошибке и, в зависимости от переменных класса $show_error и $stop_on_error, при ошибке выводит её и/или останавливает скрипт
     *
     * @param bool $on_connect [optional] [false] Если флаг установлен, то при коннекте не выводить ничего, но при этом сохранить в лог
     *
     * @return void
     */
    function reportError($on_connect = false)
    {
        $e = @oci_error();
        if (!$e)
            $e = @oci_error($this->conn);
        if (!$e)
            $e = @oci_error($this->cursor);
        $this->log .= $e['message']."\n";
        if ($on_connect)
            return;
        if ($this->show_error && trim($e['message']) != "")
            echo "{$e['message']}\n";
        if ($this->stop_on_error)
            exit;
    }
    
    /**
     * OracleQuery конструктор
     *
     * Все параметры не обязательны, в случае их отсутствия они (кроме пароля) выставятся из констант (указаны в скобках)
     *
     * @param string $tns           [optional] Название БД (ORACLE_TNS)
     * @param string $scheme        [optional] Имя пользователя (ORACLE_SCHEME)
     * @param string $passwd        [optional] Пароль пользователя
     * @param string $character_set [optional] Кодировка
     */
    function __construct($tns = "", $scheme = "", $passwd = "", $character_set = "default")
    {
        //  Выставляем кодировку в зависимости от константы, если была выставлена дефолтная кодировка
        if ($character_set == "default")
            $character_set = ENCODE == "UTF-8" ? "AL32UTF8" : "";
        
        //  Обработка ситуации, когда параметры переданы в массиве
        if (is_array($tns) && (isset($tns['scheme']) || isset($tns['username'])) && isset($tns['tns']) && (isset($tns['passwd']) || isset($tns['password']) || isset($tns['cpasswd']))) {
            if ($scheme != "")
                $character_set = $scheme;
            $passwd = isset($tns['cpasswd']) ? data_to_str($tns['cpasswd']) : (isset($tns['passwd']) ? $tns['passwd'] : $tns['password']);
            $scheme = isset($tns['scheme']) ? $tns['scheme'] : $tns['username'];
            $tns = $tns['tns'];
        }
        elseif (is_array($tns))
            $tns = $tns['tns'];
        
        //  Обработка модуля KeePass
        if (substr(strtolower($tns), 0, 15) == "module:keepass:") {
            api::moduleExists("keepass") or api::out("Отсутствует модуль KeePass", 500);
            
            //  Получаем путь в базе KeePass, отрезая префикс "module:keepass:"
            $path = trim(substr($tns, 15));
            
            //  Получаем данные из KeePass
            $data = json_decode(\keepass::getSqlData(["path" => $path]), true);
            list($tns, $scheme, $passwd) = array($data['tns'], $data['scheme'], $data['passwd']);
        }
        
        //  Обработка универсального метода
        if (substr(strtolower($tns), 0, 10) == "universal:") {
            
            //  Получаем путь к данным, отрезая префикс "universal:"
            $path = trim(substr($tns, 10));
            
            $data = $this->GetSQLDataFromUniversalData($path);
            list($tns, $scheme, $passwd) = array($data['tns'], $data['scheme'], data_to_str($data['cpasswd']));
        }
        if ($tns != "")
            $this->tns = $tns;
        if ($scheme != "")
            $this->scheme = $scheme;
        if (!($this->conn = oci_connect($this->scheme, $passwd, $this->tns, $character_set))) {
            $this->init = false;
            $this->reportError(true);
        }
        else
            $this->init = true;

        if (DEFAULT_SQL_DEBUG === true)
            $this->debug = true;
        
        //  По-умолчанию выставляем client_info равный названию скрипта
        $this->client_info = $_SERVER['SCRIPT_NAME'];
    }
    
    /**
     *  Включение/отключение dbms_output буфера
     *
     * @param bool $on_off Признак включения буфера
     *
     * @return bool Результат операции
     */
    function SetServerOutput($on_off)
    {
        $parse_query = oci_parse($this->conn, $on_off ? "BEGIN DBMS_OUTPUT.ENABLE(null); END;" : "BEGIN DBMS_OUTPUT.DISABLE(); END;");
        if ($parse_query) {
            $result = oci_execute($parse_query);
            @oci_free_statement($parse_query);
            return $result;
        }
        return false;
    }
    
    /**
     *  Получение содержимого dbms_output буфера
     *
     * @param string $filename  [optional] [null] Название файла для записи в него вывода (если большой объём)
     * @param string $linebreak [optional] [null] Строка в тексе, все вхождения которой необходимо заменить на строку $repalce
     * @param string $replace   [optional] [null] Строка, на которую нужно заменить все вхождения строки $linebreak
     *
     * @return array
     */
    function GetDbmsOutput($filename = null, $linebreak = null, $replace = null)
    {
        if ($filename)
            $file = fopen($filename, "w");
        else {
            $this->dbms_output = [];
            $file = null;
        }
        $parse_query = oci_parse($this->conn, "BEGIN DBMS_OUTPUT.GET_LINE(:LN, :ST); END;");
        if ($parse_query) {
            
            // Будем считывать буфер построчно, биндим переменные в максимальный размер строки Oracle   
            if (oci_bind_by_name($parse_query, ":LN", $ln, 32768) && oci_bind_by_name($parse_query, ":ST", $st, null)) {
                $row1 = $row2 = null;
                while ($execute_result = oci_execute($parse_query)) {
                    if ($st)
                        break;
                    
                    //  Из-за того, что строка для замены может попасть на пересечение блоков чтения, мы считываем блок и заменяем данные на стыке двух блоков,
                    //  после чего записываем первый блок, считываем следующий, объединяем с предыдущим, заменяем и т.д. 
                    $row2 = $row1;
                    $row1 = $ln;
                    if (!is_null($linebreak))
                        list($row2, $row1) = str_split(str_replace($linebreak, $replace, $row2.$row1), strlen($ln));
                    if ($filename)
                        fwrite($file, $row2);
                    else
                        $this->dbms_output[] = $row2;
                }
                if (!$execute_result)
                    $this->dbms_output = false;
                if (!is_null($linebreak))
                    $row1 = str_replace($linebreak, $replace, $row1);
                
                //  Записываем информацию из последнего считанного блока
                if ($filename)
                    fwrite($file, $row1);
                $this->dbms_output[] = $row1;
            }
            @oci_free_statement($parse_query);
        }
        if ($filename)
            fclose($file);
        return ($this->dbms_output);
    }
    
    /**
     *  Установка значения поля CLIENT_INFO (64 байта) из сессии (при последующем запросе)
     *
     * @param string $info       Информация, которая будет записана в поле сессии CLIENT_INFO
     * @param bool   $persistent [optional] [false] Запомнить информацию в поле класса и принудительно выставлять при каждом запроса
     */
    function setClientInfo($info, $persistent = false)
    {
        @oci_set_client_info($this->conn, $info);
        if ($persistent)
            $this->client_info = $info;
    }
    
    /**
     *  Установка значения поля CLIENT_IDENTIFIER (64 байта) из сессии (при последующем запросе)
     *
     * @param string $info       Информация, которая будет записана в поле сессии CLIENT_IDENTIFIER
     * @param bool   $persistent [optional] [false] Запомнить информацию в поле класса и принудительно выставлять при каждом запроса
     */
    function setClientIdentifier($info, $persistent = false)
    {
        @oci_set_client_identifier($this->conn, $info);
        if ($persistent)
            $this->client_identifier = $info;
    }
    
    /**
     *  Установка значения поля ACTION (32 байта) из сессии (при последующем запросе)
     *
     * @param string $info       Информация, которая будет записана в поле сессии ACTION
     * @param bool   $persistent [optional] [false] Запомнить информацию в поле класса и принудительно выставлять при каждом запроса
     */
    function setAction($info, $persistent = false)
    {
        @oci_set_action($this->conn, $info);
        if ($persistent)
            $this->action = $info;
    }
    
    /**
     *  Установка значения поля MODULE (48 байт) из сессии (при последующем запросе)
     *
     * @param string $info       Информация, которая будет записана в поле сессии MODULE
     * @param bool   $persistent [optional] [false] Запомнить информацию в поле класса и принудительно выставлять при каждом запроса
     */
    function setModule($info, $persistent = false)
    {
        @oci_set_module_name($this->conn, $info);
        if ($persistent)
            $this->module = $info;
    }
    
    /**
     *  Включение трассировки
     *
     * @return void
     */
    function trace_on()
    {
        //  Если стоит признак включить трассировку или установлен признак того, что трассировка включена, но включать всё равно нужно
        //  (отдельная трассировка для каждого запроса)
        if ($this->trace === true || ($this->trace && $this->trace_separate)) {
            
            if ($this->debug)
                echo "Включаем трассировку ".$this->trace_level."-го уровня с идентификатором '".$this->trace_ident."': ".date("Y-m-d h:i:s")."<br>\n";
            
            //  Устанавливаем размер трейс файла
            $trace_sql_text = "ALTER SESSION SET MAX_DUMP_FILE_SIZE = 'UNLIMITED'";
            $this->cursor = oci_parse($this->conn, $trace_sql_text);
            if (!@oci_execute($this->cursor, OCI_DEFAULT)) {
                if ($this->debug)
                    echo "Ошибка выставления размера трейс файла: ".date("Y-m-d h:i:s")."<br>\n";
                $this->reportError();
            }
            //  Устанавливаем идентификатора трассировки
            $trace_sql_text = "ALTER SESSION SET TRACEFILE_IDENTIFIER = '".$this->trace_ident."'";
            $this->cursor = oci_parse($this->conn, $trace_sql_text);
            if (!@oci_execute($this->cursor, OCI_DEFAULT)) {
                if ($this->debug)
                    echo "Ошибка выставления идентификатора трейса: ".date("Y-m-d h:i:s")."<br>\n";
                $this->reportError();
            }
            //  Включаем трассировку
            $trace_sql_text = "ALTER SESSION SET EVENTS '10046 TRACE NAME CONTEXT FOREVER, LEVEL {$this->trace_level}'";
            $this->cursor = oci_parse($this->conn, $trace_sql_text);
            if (!@oci_execute($this->cursor, OCI_DEFAULT)) {
                if ($this->debug)
                    echo "Ошибка включения трассировки: ".date("Y-m-d h:i:s")."<br>\n";
                $this->reportError();
            }
            
            //  Установливаем количество трассировок в данной сессии в 1, если стоит признак включения трассировки
            //  Если же трассировка уже включена, но стоит признак включения трассировки по каждому запросу, увеличиваем счётчик трассировок
            if ($this->trace === true)
                $this->trace = 1;
            else if ($this->trace_separate)
                $this->trace++;
        }
    }
    
    /**
     *  Выключение трассировки на базе данных
     *
     * @return void
     */
    function trace_off()
    {
        if ($this->trace) {
            
            //  Выключаем трассировку
            $trace_sql_text = "ALTER SESSION SET EVENTS '10046 TRACE NAME CONTEXT OFF'";
            $this->cursor = oci_parse($this->conn, $trace_sql_text);
            @oci_execute($this->cursor, OCI_DEFAULT);
            if ($this->debug)
                echo "Выключаем трассировку ".$this->trace_level."-го уровня с идентификатором '".$this->trace_ident."': ".date("Y-m-d h:i:s")."<br>\n";
        }
    }
    
    /**
     *  Выполнение запроса на базе данных
     *
     * @param string $query  [optional] Текст запроса (если не задан, воьмётся из переменной класса $this->query)
     * @param array  $param  [optional] Переменные запроса
     * @param array  $return [optional] Переменные ответа
     *
     * @return array|bool
     */
    function exec($query = "", $param = array(), $return = array())
    {
        unset($this->result);
        unset($this->fields);
        if ($this->log_autoclear)
            $this->log = "";
        if ($query != "")
            $this->query = $query;
        $this->query = str_replace("\r", "", $this->query);
        $this->param = $param;
        $this->return = $return;
        if ($this->debug) {
            echo "Begin: ".date("Y-m-d h:i:s")."<br>\n";
            echo "&lt;<pre style='color: red'>".$this->query."</pre>&gt;<br>";
            echo "&lt;<pre style='color: green'>";
            print_r($this->param);
            echo "</pre>&gt;<br>";
        }
        
        //  Установка пользовательских полей сессии
        if ($this->client_info != "")
            $this->setClientInfo($this->client_info);
        if ($this->client_identifier != "")
            $this->setClientIdentifier($this->client_identifier);
        if ($this->action != "")
            $this->setAction($this->action);
        if ($this->module != "")
            $this->setModule($this->module);

        $this->trace_on();
        if (!($this->cursor = @oci_parse($this->conn, $this->query)))
            $this->reportError();
        
        //  Если переданы параметры запроса
        if (is_array($this->param) && sizeof($this->param))
            for ($i = 0; $i < sizeof($this->param); $i++)
                if (strpos($this->query, $this->param[$i]['name']) !== false)
                    
                    //  Обработка CLOB переменной
                    if (strtolower($this->param[$i]['type']) == "clob") {
                        $clob = oci_new_descriptor($this->conn, OCI_D_LOB);
                        $clob->writeTemporary($this->param[$i]['value'], OCI_TEMP_CLOB);
                        @oci_bind_by_name($this->cursor, $this->param[$i]['name'], $clob, -1, OCI_B_CLOB);
                    }
                    //  Обработка остальных переменных
                    else
                        @oci_bind_by_name($this->cursor, $this->param[$i]['name'], $this->param[$i]['value']);
        
        //  Если указаны параметры для ответа
        if (is_array($this->return) && sizeof($this->return))
            for ($i = 0; $i < sizeof($this->return); $i++)
                
                //  Обработка переменных ответа с type_lob
                if ($this->return[$i]['type_lob']) {
                    $this->return[$i]['value'] = oci_new_descriptor($this->conn, $this->return[$i]['type']);
                    @oci_bind_by_name($this->cursor, $this->return[$i]['name'], $this->return[$i]['value'], -1, $this->return[$i]['type_lob']);
                }
                //  Обработка остальных переменных ответа
                else
                    @oci_bind_by_name($this->cursor, $this->return[$i]['name'], $this->return[$i]['value'], 1000000000, $this->return[$i]['type']);
        if (!$execute_result = @oci_execute($this->cursor, OCI_DEFAULT)) {
            if ($this->debug)
                echo "End: ".date("Y-m-d h:i:s")."<br>\n";
            $this->reportError();
        }
        if ($this->debug)
            echo "End: ".date("Y-m-d h:i:s")."<br>\n";
        
        //  Если указаны параметры для ответа
        if (is_array($this->return) && sizeof($this->return)) {
            $return_vars = array();
            for ($i = 0; $i < sizeof($this->return); $i++)
                
                //  Запоминаем результат работы метода load, если на выходе объект
                if (is_object($this->return[$i]['value']))
                    $return_vars[substr($this->return[$i]['name'], 1)] = $this->return[$i]['value']->load();
                //  А на выходе обычная переменная, то запоминаем её значение
                else
                    $return_vars[substr($this->return[$i]['name'], 1)] = $this->return[$i]['value'];
            
            $this->return = $return_vars;
        }
        
        //  Убираем возможные комментарии в запросе перед проверкой на select
        $sql_text = preg_replace("/\-\-.*?$/mi", "", $this->query);
        if (substr(strtolower(trim($sql_text)), 0, 6) == "select") {
            $handle = null;
            if ($this->save_to_file) {
                @unlink($this->save_to_file);
                $handle = fopen($this->save_to_file, "a");
            }
            $count_row = 0;
            $return_data = array();
            while ($record = @oci_fetch_array($this->cursor, OCI_ASSOC | OCI_RETURN_NULLS | OCI_RETURN_LOBS)) {
                $line = "";
                $count_column = 0;
                foreach ($record as $key => $value) {
                    if ($this->save_to_file)
                        $line .= ($line != "" ? $this->save_to_file_separator : "").$value;
                    else {
                        if (!$this->short_result)
                            $this->result[$key][] = $value;
                        if ($count_row)
                            $return_data[$count_row][$count_column] = $value;
                        else {
                            $return_data[$count_row][$count_column] = $key;
                            $return_data[$count_row + 1][$count_column] = $value;
                        }
                    }
                    $count_column++;
                }
                $line .= "\r\n";
                if ($this->save_to_file)
                    fwrite($handle, $line);
                $count_row += !$count_row ? 2 : 1;
            }
            
            //  Заполняем данные по колонкам
            $num_cols = @oci_num_fields($this->cursor);
            if ($num_cols) {
                $this->fields = [];
                for ($i = 1; $i <= $num_cols; $i++)
                    $this->fields[oci_field_name($this->cursor, $i)] = [
                        "name" => oci_field_name($this->cursor, $i),
                        "type" => strtolower(oci_field_type($this->cursor, $i)),
                        "size" => oci_field_size($this->cursor, $i),
                    ];
            }
            
            //  Освобождаем память
            @oci_free_statement($this->cursor);
            $this->trace_off();
            
            if ($this->save_to_file) {
                fclose($handle);
                return true;
            }
            //  Возвращаем результат, если он есть, если нет, то возвращаем логический результат запроса
            return sizeof($return_data) ? $return_data : $execute_result;
        }
        $this->trace_off();
        return true;
    }
    
    /**
     *  Получение пароля универсальным способом (не рекомендуется)
     *
     * На примере USER@BASE
     * Сначала проверяется константа BASE_USER_USERNAME, если такая найдена, то из неё берётся схема пользователя, а из BASE_USER_PASSWORD - пароль
     * Если же такой константы нет, то ту же проверку проходит константа USER_USERNAME (и USER_PASSWORD)
     *
     * @param string $path      Указатель на БД, состоящий из идентификаторов пользователя и БД, разделённых @
     * @param bool   $pass_only [optional] Признак возврата только пароля
     *
     * @return array|bool При совпадении возвращается массив, содержащий схему пользователя и шифрованный пароль (или просто пароль, если установлен
     *                    соответствующий флаг), если же совпадения не было, возвращается false
     */
    private function GetSQLDataFromUniversalData($path, $pass_only = false)
    {
        list($scheme, $tns) = explode("@", $path);
        if (defined(strtoupper($tns."_".$scheme."_USERNAME"))) {
            $username = constant(strtoupper($tns."_".$scheme."_USERNAME"));
            $pass = constant(strtoupper($tns."_".$scheme."_PASSWORD"));
        }
        else if (defined($scheme."_USERNAME")) {
            $username = constant($scheme."_USERNAME");
            $pass = constant($scheme."_PASSWORD");
        }
        else
            return false;
        if ($pass_only)
            return $pass;
        return array("tns" => $tns, "scheme" => $username, "cpasswd" => $pass);
    }
}
