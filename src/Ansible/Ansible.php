<?php
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

namespace PSData;                   //  Указываем пространство имён

use \SensioLabs\AnsiConverter\AnsiToHtmlConverter;
                                            //  Будем использовать класс AnsiToHtmlConverter
use Api as api;                             //  Будем использовать класс Api

define(__NAMESPACE__.'\DEBUG', false);      //  Нужно ли сохранять каталоги для отладки (true) или подчищать всё за собой (!true)

class Ansible
{
  
  private $res = null;
  public $config = <<<INI
[defaults]
host_key_checking = False
retry_files_enabled = False
accept_hostkey = True

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no

INI;

/** Методы:
*  setProject: Распаковка проекта (из zip файла) - если используется, должна быть первой операцией
*  setPlaybook: Установка файла плейбука
*  setInventory: Установка инвентори файла
*  setLimit: Установка лимита для инвентори
*  setBecomeUser: Установка sudo пользователя [пароля]
*  setRemoteUser: Добавление ssh пользователя [пароля]
*  setVars: Добавление/сброс переменных
*  prepareVars: Подготовка шифрованных файлов переменных для запуска ансибля
*  setPasswords: Установка пароля для авторизации по ssh и sudo
*  allowBG: Разрешить выполнять плейбук в фоне
*  checkToRun: Проверка наличия данных к запуску плейбука (скрипта)
*  setDebug: Указать уровень отладки
*  play: Выполнить плейбук/команду
*  getBGInfo: Получение информации по запущенному в фоне процессу
*  parseTasks: Распарсивание сырого вывода ансибля из метода play (должен быть включён setDebug уровня 1 и выше)
*  getAuthFromVault: Подготовка шифрованных файлов аутентификации для ансибля
*/

/** Использование:
 *  Старт команды:
 *  $ansible->setInventory($inventory);
 *  $ansible->setRemoteUser($username, $userpassword);
 *  if ($becomename != "")
 *    $ansible->setBecomeUser($becomename, $becomepassword);
 *  $ansible->play("shell: ifconfig");
 *
 *  Старт плейбука:
 *  $ansible->setPlaybook($playbook);
 *  $ansible->setInventory($inventory);
 *  $ansible->setRemoteUser($username, $userpassword);
 *  if ($becomename != "")
 *    $ansible->setBecomeUser($becomename, $becomepassword);
 *  $ansible->play();
 *
 *  Старт проекта:
 *  $ansible->setProject("/path/to/project/in/file.zip");
 *  $ansible->setPlaybook($playbook);
 *  $ansible->setInventory($inventory);
 *  $ansible->setRemoteUser($username, $userpassword);
 *  if ($becomename != "")
 *    $ansible->setBecomeUser($becomename, $becomepassword);
 *  $ansible->play();
 */
   
   
   
  /**
   *  Инициализация класса
   *  @return array Массив, содержащий поле принадлежности классу
   */
  function __construct()
  {
    return $this->res = array("classdescription" => "ansible", "debuglevel" => 1);
  }
  
  function play($cmd = "")
  {
    if (!$this->checkToRun($cmd) || !$this->prepareVars())
                                            //  Если скрипт не прошёл проверку необходимых параметров или произошла ошибка при формировании переменных
      api::out("Не полностью указаны исходные данные / Ошибка подготовки данных", 400);
                                            //  Возвращаем ошибку
    if ($cmd != "") {                       //  Если была передана прямая команда
      if (!@yaml_parse($cmd)) {             //  Проверяем её на валидность и, если она её не прошла
        api::out("Ошибка YAML распарсивания", 400);
                                            //  Возвращаем ошибку
      }
      $cmd = "- name: cmd translation script\n  hosts: all\n".(isset($this->res['becomeuser'])?"  become: yes\n":"")."  tasks:\n      - name: run script\n        ".$cmd."\n        register: out\n        ignore_errors: yes\n";
                                            //  Формируем заголовок плейбука для команды
      $pbfile = tempnam(sys_get_temp_dir(), 'ansfw');
                                            //  Получаем имя для файла плейбука
      @file_put_contents($pbfile, $cmd);    //  Записываем в него получившуюся команду
      $cmd = $pbfile;                       //  И запоминаем путь до этого файла
    }
    $debug = $this->res['debuglevel'] > 0?" -".str_pad($input, $this->res['debuglevel'], "v", STR_PAD_RIGHT):"";
                                            //  Указываем подробность дебага
    $config_dir = dirname($cmd != "" ? $cmd : $this->res['playbook']);
    
    $playbook = ($cmd != "" ? $cmd : $this->res['playbook']);
    $inventory = $this->res['inventory'];
    $limit = (isset($this->res['limit']) ? "-l ".escapeshellarg(file_get_contents($this->res['limit']))." " : "");
    $vaultfile = $this->res['vaultfile'];
    $passfile = $this->res['passfile'];

    //  Конвертируем пути в cygwin совместимые, если установлена переменная окружения CYGWIN
    if ($_SERVER['CYGWIN'] != "") {
        $playbook = preg_replace("/\s*([a-zA-Z]{1}):(?:\/|\\\)(.*)/", "/cygdrive/\\1/\\2", str_replace("\\", "/", $playbook));
        $inventory = preg_replace("/\s*([a-zA-Z]{1}):(?:\/|\\\)(.*)/", "/cygdrive/\\1/\\2", str_replace("\\", "/", $inventory));
        $vaultfile = preg_replace("/\s*([a-zA-Z]{1}):(?:\/|\\\)(.*)/", "/cygdrive/\\1/\\2", str_replace("\\", "/", $vaultfile));
        $passfile = preg_replace("/\s*([a-zA-Z]{1}):(?:\/|\\\)(.*)/", "/cygdrive/\\1/\\2", str_replace("\\", "/", $passfile));
    }
    
    $cmd = ANSIBLEPLAYBOOK." ".escapeshellarg($playbook)." -i ".escapeshellarg($inventory)." ".$limit."-u ".escapeshellarg($this->res['remoteuser'])." ".(trim($this->res['becomeuser']) != "" ? "-U ".escapeshellarg($this->res['becomeuser'])." ":"")."--extra-vars ".escapeshellarg("@".$vaultfile)." --vault-password-file ".escapeshellarg($passfile).$debug;
                                            //  Формируем скрипт запуска ansible
    if ($this->res['allowbg']) {            //  Если скрипт нужно запустить в бэкграунде
      $outfile = tempnam(sys_get_temp_dir(), 'ansfw');
                                            //  Формируем временный файл, в котором будем хранить pid процесса
      $outname = preg_replace("/.*\/(.*)/", "\\1", $outfile);
                                            //  Запоминаем его имя, без пути
      $errfile = tempnam(sys_get_temp_dir(), 'ansfw');
                                            //  Формируем временный файл, в котором будем хранить лог ошибок
      $startfile = tempnam(sys_get_temp_dir(), 'ansfw');
                                            //  Получаем имя для файла стартера
      @file_put_contents($startfile, $cmd."\necho 'returncodebegin:'\$?':returncodeend'");
                                            //  Записываем в него получившуюся команду
      chmod($startfile, 0755);              //  Запускаем скрипт
      $cmd = "unbuffer ".$startfile." > ".$outfile.".out 2>".$errfile." & echo $! & disown";
                                            //  Слегка изменяем строку запуска, чтобы она запустилась в фоне и работала даже после окончания сессии
#      file_put_contents("xxx", $cmd);
    }
    $current_dir = getcwd();
    if (!file_exists("{$config_dir}/ansible.cfg"))
      file_put_contents("{$config_dir}/ansible.cfg", (substr(php_uname(), 0, 7) != "Windows" ? $this->config : str_replace("-o ControlMaster=auto ", "", $this->config)));
    chdir($config_dir);
    exec($cmd, $out);                       //  Запускаем скрипт
    chdir($current_dir);
    if ($this->res['allowbg']) {            //  Если был запуск в фоне
      file_put_contents($outfile, $out[0]."\n".json_encode(array($errfile, $this->res['limit'], $this->res['playbook'], $this->res['inventory'], $this->res['playbook'], $this->res['vaultfile'], $this->res['passfile'], $startfile, $this->res['projectbase'] != "" ? $this->res['projectbase'] : "nothing"), JSON_UNESCAPED_UNICODE));
                                            //  Сохраняем pid запущенного процесса
      return $outname;                   //  И возвращаем код процесса (имя файла)
    }
    if (DEBUG !== true)                     //  Если не нужно отлаживать и нужно всё стереть
      @unlink($pbfile);                     //  Если же запуск был обычный, удаляем временный плейбук для командной строки, если он был указан
    return $out;         //  Возвращаем информацию по результатам выполнения задач
  }
  
  /**
   *  Получение информации по запущенному в фоне процессу
   *  @param $key: Строка, содержащая ключ (идентификатор) процесса
   *  @param $shift: Указание, с какого места нужно читать файл вывода (если строка равна "clean", то почистить всё за собой и вернуть распарсенный результат)
   *  @param $colored: Указание, нужно ли раскрашивать текст в цвета терминала
   *  @param $crlf: Указание, на что заменять символ перевода строки
   *  @return (array): Возвращает массив данных, с информацией о выводе программы, и наличии процесса, пишущего в этот вывод 
   */
  function getBGInfo($key, $shift = 0, $colored = false, $crlf = "\n")
  {
    $tmpdir = sys_get_temp_dir()."/";       //  Получаем путь до временного каталога
    if ($shift == "clean") {                //  Если пришёл сигнал подчистить за собой
      sleep(5);                             //  Подождём, чтобы другие могли дочитать из файла вывода
      if (file_exists($tmpdir.$key)) {      //  И, если файл ключа присутствует
        $code = @json_decode(preg_replace("/.*?\s+(.*)/", "\\1", file_get_contents($tmpdir.$key)), true);
                                            //  Получаем список всех использованных файлов
        if (is_array($code)) {              //  Если получился массив
          $errout = file_get_contents($code[0]);
                                            //  Запоминаем содержимое файла ошибок (он первый по счёту)
          if (DEBUG !== true)               //  Если не нужно отлаживать и нужно всё стереть
            foreach ($code as $file)        //  Начинаем перебирать и смотреть
              if (file_exists($file))       //  Присутствует ли файл/каталог, и если да
                exec("rm -rf ".$file);      //  Удаляем его
        }
        $out = file_get_contents($tmpdir.$key.".out");
        if (DEBUG !== true) {               //  Если не нужно отлаживать и нужно всё стереть
          @unlink($tmpdir.$key);            //  Удаляем его
          @unlink($tmpdir.$key.".out");     //  И файл вывода
        }
        $returncode = preg_replace("/(.*returncodebegin\:)(\d+)(\:returncodeend.*)/si", "\\2", $out);
                                            //  Получаем код возврата
        $out = preg_replace("/(.*)(returncodebegin\:\d+\:returncodeend.*)/si", "\\1", $out);
                                            //  И вырезаем его
        $converter = new AnsiToHtmlConverter();
                                            //  Наследуем класс подсветки
        return array("out" => !(int)$returncode ? $this->parseTasks($out):$converter->convert($out), "returncode" => $returncode, "err" => $errout);
                                            //  Вовращаем результат распарсивания вывода
      }
      return true;                          //  Возвращаем успех
    }
    if (!file_exists($tmpdir.$key))         //  Если файла ключа в каталоге нет
      return array("processed" => false);   //  Возвращаем флаг, что файла больше нет
    else                                    //  А если файл есть
      $pid = preg_replace("/(.*?)\s+.*/", "\\1", file_get_contents($tmpdir.$key));
                                            //  Считываем из него pid процесса
    $exists = file_exists("/proc/".$pid);   //  И запоминаем работает ли процесс сейчас
    if (!($fp = fopen($tmpdir.$key.".out", "r")))
                                            //  Открываем на чтение файл с выводом программы и, если не удалось
      return array("processed" => false);   //  Возвращаем флаг, что файла больше нет
    fseek($fp, $shift);                     //  Смещаемся на указанное смещение
    $out = fread($fp, 1048576);             //  Считываем блок текста
    $shift = ftell($fp);                    //  Запоминаем текущее смещение
    fclose($fp);                            //  И закрываем файл
    $out = preg_replace("/(.*)(returncodebegin\:\d+\:returncodeend.*)/si", "\\1", $out);
                                            //  Вырезаем из вывода код возврата, если он есть
    if ($colored && $colored != "raw") {    //  Если нужно подсветить вывод в html (и не выводить сырой вывод)
      $converter = new AnsiToHtmlConverter();
                                            //  Наследуем класс подсветки
      $out = $converter->convert($out);     //  Конвертируем ANSI подсветку в HTML
    } elseif (!$colored)                    //  Если же от подсветки нужно избавиться вообще
      $out = preg_replace("/\x1b\[[0-9;]*[a-zA-Z]/", "", $out);
                                            //  Удаляем все упоминания о ней
    $out = str_replace("\n", $crlf, $out);  //  Заменяем переводы строк на то, что необходимо
    return array("out" => $out, "shift" => $shift, "processed" => $exists);
                                            //  Возвращаем ключ файла, текущее смещение и признак наличия процесса, в этот файл пишушего
  }
  
  /**
   *  Распарсивание сырого вывода ансибля для получения результатов выполнения тасков
   *  @param $out: строка, содержащая массив с сырым выводом ансибля
   *  @return (array): Возвращает массив ценных данных, полученный из сырого вывода
   */
  function allowBG($allow)
  {
    if ($allow === true)                    //  Если нужно выполнить скрипт в фоне
      $this->res['allowbg'] = true;         //  Выставляем параметр
    else                                    //  А во всех остальных случаях
      unset($this->res['allowbg']);         //  Этот параметр удаляем
    return true;                            //  Возвращаем успех
  }
  
  /**
   *  Установка уровня отладки при выполнении плейбука
   *  @param $level: Число, указывающее на уровень отладки (от 0 до 4)
   *  @return (bool): Возвращает успешность операции
   */
  function setDebug($level)
  {
    $level = intval($level);                //  Конвертируем полученный уровень отладки в целове число
    if ($level < 0 || $level > 4)           //  Если число не лежит в диапазоне 0..4
      api::out("Неверно указан уровень отладки (0..4)", 400);
                                            //  Возвращаем ошибку
    $this->var['debuglevel'] = $level;      //  Если же всё ок, сохраняем уровень отладки
    return true;                            //  Возвращаем успех
  }
  
  /**
   *  Распарсивание сырого вывода ансибля для получения результатов выполнения тасков
   *  @param $out: строка, содержащая массив с сырым выводом ансибля
   *  @return (array): Возвращает массив ценных данных, полученный из сырого вывода
   */
  function parseTasks($out)
  {
    if (!is_array($out) && $out != "")
      $out = explode("\n", $out);
    $data = array();                        //  Инициализируем массив, в котором будем хранить распарсенный вывод
    for ($i = 0; $i < sizeof($out); $i++) { //  Начинаем перебирать строки вывода ансибля
      if (!preg_match("/TASK:\s+\[([^\]]*)\]\s+\**/", $out[$i], $tmp))
                                            //  Пока не встретим строку с таском
        continue;                           //  Будем продолжать искать
      $taskname = $tmp[1];                  //  Запоминаем название таска
      $task = array();                      //  И инициализируем массив, в котором будем хранить строки с выводом этого таска
      for ($j = ++$i; $j < sizeof($out); $j++, $i++)
                                            //  Начинаем пребирать вывод таска
        if (trim($out[$j]) != "")           //  И пока не встретится пустая строка
          $task[] = $out[$j];               //  Запоминаем весь вывод
        else                                //  А когда встретится
          break;                            //  Прерываемся
      for ($j = 0; $j < sizeof($task); $j++) {
                                            //  Начнём перебирать строки таска
        if (!preg_match("/changed:\s+\[([^\]]*)\]\s+=>\s+(.*)/s", preg_replace("/\x1b\[[0-9;]*[a-zA-Z]/", "", $task[$j]), $tmp))
                                            //  Если это не информация о изменениях
          continue;                         //  Будем искать информацию и как только найдём
        $data[$taskname][$tmp[1]] = @json_decode($tmp[2], true);
                                            //  Сразу занесём в выходной массив
      }
      continue;                             //  Переходим на поиск следующего таска
    }
    return $data;                           //  Возвращаем выходной массив с данными, полученными по резултату выполнения тасков
  }
  
  /**
   *  Проверка наличия данных к запуску плейбука (скрипта)
   *  @param [$cmd] (sting): строка, содержащая команду, если используется команда, а не плейбук
   *  @return (bool):
   *    false: в случае недостаточности данных для запуска
   *    true: во всех остальных случаях
   */
  function checkToRun($cmd = "")
  {
    if (($cmd == "" && !isset($this->res['playbook'])) || (!isset($this->res['playbook']) && isset($this->res['projectbase'])))
                                            //  Если это не одиночная команда и не указан плейбук, особенно при указанном проекте
      api::out("Не указан плейбук / команда", 400);
                                            //  Возвращаем ошибку
    if (!isset($this->res['remoteuser']) || !isset($this->res['userpass']))
                                            //  Если это не указан пользователь ssh или его пароль
      api::out("Не указан ssh пользователь", 400);
                                            //  Возвращаем ошибку
    if (!isset($this->res['inventory']))    //  Если это не указан инвертарный файл
      api::out("Не указан inventory файл", 400);
                                            //  Возвращаем ошибку
    return true;                            //  Иначе возвращаем успех
  }
  
  /**
   *  Распаковка проекта (из zip файла)
   *  @param $project: Строка с путём к архиву проекта
   *  @return (bool): Успешность распаковки
   */
  function setProject($project)
  {
    if (!file_exists($project) && substr($project, -4) != ".zip" && !is_dir($project))
                                            //  Если zip файла/каталога  проекта нет
      api::out("Отсутствует zip файл/каталог проекта", 400);
                                            //  Возвращаем ошибку
    $pjdir = $bsdir = tempnam(sys_get_temp_dir(), 'ansfw');
                                            //  Получаем название временного файла
    @unlink($pjdir);                        //  Удаляем файл
    @mkdir($pjdir, 0700, true);             //  А на его месте создаём каталог
    if (is_dir($project))                   //  Если проект каталог
      exec("cp -R ".escapeshellarg($project)."* ".escapeshellarg($pjdir));
                                            //  Копируем его в каталог назначения
    else                                    //  А если zip файл
      exec("unzip -o ".escapeshellarg($project)." -d ".escapeshellarg($pjdir));
                                            //  Распаковываем его в этот же каталог
    while (true) {                          //  И начинаем проверку пустых каталогов
      $tmp = array_values(array_diff(scandir($pjdir), array(".", "..")));
                                            //  Сканируем текущий каталог, отбрасывая служебные сущности
      if (sizeof($tmp) != 1 || !is_dir($pjdir."/".$tmp[0]))
                                            //  Если в каталоге не единственный файл или файл единственный, но не является каталогом
        break;                              //  Прерываемся
      $pjdir .= "/".$tmp[0];                //  А если распаковался одиночный каталог, то пересместим рабочий катаорг в него и повторим сканирование
    }
    if (!sizeof($tmp))                      //  Если в каталоге вообще не оказалось файлов
      api::out("Архив проекта пуст", 400);  //  Возвращаем ошибку
    $this->res['projectbase'] = $bsdir;     //  Запоминаем базовый каталог (чтобы потом его стереть вместе со всем содержимым)
    $this->res['projectdir'] = $pjdir;      //  И каталог проекта (в котором есть хоть что-то)
    if (isset($this->res['playbook'])) {    //  Если плейбук был указан до этого
      @unlink($this->res['playbook']);      //  Удаляем его файл
      unset($this->res['playbook']);        //  И упоминание о нём
    }
    if (isset($this->res['inventory'])) {   //  Если inventory фай был указан до этого
      @unlink($this->res['inventory']);     //  Удаляем этот файл
      unset($this->res['inventory']);       //  И упоминание о нём
    }
    return true;                            //  Возвращаем успех
  }
  
  /**
   *  Добавление плейбука
   *  @param $playbook (array): массив, содержащий поле принадлежности классу
   *  @return (bool):
   *    false: в случае синтаксической ошибки в тексте плейбука
   *    true: во всех остальных случаях
   */
  function setPlaybook($playbook)
  {
    if (isset($this->res['projectbase'])) { //  Если плейбука добавляется при наличии проекта
      $playbook = $this->res['projectdir']."/".$playbook;
                                            //  Устанавливаем путь плейбуки относительно проекта
      if (!file_exists($playbook) || !@yaml_parse(file_get_contents($playbook)))
                                            //  Проверяем, если файла на этом месте нет, или содержимое файла - не yaml
        api::out("Некорректный / отсутствующий плейбук в проекте", 400);
                                            //  Возвращаем ошибку
      $this->res['playbook'] = $playbook;   //  Запоминаем имя файла с плейбуком
      return true;                          //  И возвращаем успех
    }
    if (file_exists($playbook))             //  Если был передан путь на файл плейбука
      $playbook = file_get_contents($playbook);
                                            //  Загружаем содержимое плейбука из файла
    if (!@yaml_parse($playbook))            //  Если плейбук некорректен
      api::out("Некорректный плейбук", 400);//  Возвращаем ошибку
    $pbfile = tempnam(sys_get_temp_dir(), 'ansfw');
                                            //  Получаем имя для файла плейбука
    if (isset($this->res['playbook']))      //  Если файл плейбука был выставлен ранее
      @unlink($this->res['playbook']);      //  Удаляем его
    @file_put_contents($pbfile, $playbook); //  Записываем в него получившийся плейбук
    $this->res['playbook'] = $pbfile;       //  Сохраняем имя файла с плейбуком
    return true;                            //  Возвращаем успех
  }
  
  /**
   *  Добавление файла инвентаря
   *  @param (array): Массив, содержащий поле принадлежности классу
   *  @return (bool): результат выполнения операции
   */
  function setInventory($inventory)
  {
    if (isset($this->res['projectbase'])) { //  Если инвентарный файл добавляется при наличии проекта
      if (file_exists($inventory) && !file_exists($this->res['projectdir']."/hosts/".basename($inventory))) {
        rename($inventory, $this->res['projectdir']."/hosts/".basename($inventory));
        $inventory = basename($inventory);
      }
      $inventory = $this->res['projectdir']."/hosts/".basename($inventory);
                                            //  Устанавливаем путь плейбуки относительно проекта
      if (!file_exists($inventory))         //  Проверяем, если файла на этом месте нет
        api::out("Отсутствующий inventory в проекте", 400);
                                            //  Возвращаем ошибку
      $this->res['inventory'] = $inventory;
                                            //  Запоминаем имя файла инвентаря
      return true;                          //  И возвращаем успех
    }
    if (file_exists($inventory))            //  Если был передан путь на файл инвентаря
      $inventory = file_get_contents($inventory);
                                            //  Загружаем содержимое из файла инвентаря
#      if (!parse_ini_string($inventory))    //  Если инвертарный файл некорректен
#        api::out("Некорректный inventory файл", 400);
                                            //  Возвращаем ошибку
    $ivfile = tempnam(sys_get_temp_dir(), 'ansfw');
                                            //  Получаем имя для файла инвентаря
    if (isset($this->res['inventory']))     //  Если файл инвентаря был выставлен ранее
      @unlink($this->res['inventory']);     //  Удаляем его
    @file_put_contents($ivfile, $inventory);//  Записываем в него получившийся инвентарь
    $this->res['inventory'] = $ivfile;      //  Сохраняем имя файла с инвентарём
    return true;                            //  Возвращаем успех
  }
  
  /**
   *  Добавление файла ограничений для инвентаря
   *  @param (array / string): массив / строка, содержащие список ограничений по хостам из инвентарного файла
   *  @return (bool): результат выполнения операции
   */
  function setLimit($limit)
  {
    if (file_exists($limit))                //  Если был передан путь на файл ограничений (retry файл)
      $limit = file_get_contents($limit);   //  Загружаем содержимое из файла огрничений
    $lfile = tempnam(sys_get_temp_dir(), 'ansfw');
                                            //  Получаем имя для файла ограничений
    if (isset($this->res['limit']))         //  Если файл ограничений был выставлен ранее
      @unlink($this->res['limit']);         //  Удаляем его
    @file_put_contents($lfile, $limit);     //  Записываем в него получившийся список ограничений
    $this->res['limit'] = $lfile;           //  Сохраняем имя файла ограничений
  }
  
  /**
   *  Добавление sudo пользователя
   *  @param [$username]: Имя пользователя. Если параметр не передан или передано пустое значение, то sudo пользователь удаляется
   *  @param [$password]: Пароль пользователя для соединения по ssh
   *  @return (bool): успешность операции
   */
  function setBecomeUser($username = "", $password = "")
  {
    if (is_array($username)) {              //  Если вместо имени пользователя передали массив
      if (isset($username['username']) && isset($username['password']))
                                            //  Смотрим, если это ассоциативный массив с нужными полями
        $username = array($username['username'], $username['password']);
                                            //  Располагаем поля в необходимом порядке
      list($username, $password) = array_values($username);
                                            //  И преобразуем массив в переменные
    }
    if (trim($username) == "")              //  Если имени пользователя нет
      unset($this->res['becomeuser']);      //  Удаляем имя sudo пользователя
    else                                    //  А если имя пользователя передали
      $this->res['becomeuser'] = trim($username);
                                            //  Запоминаем его
    return ($password == "")?true:$this->setPasswords("", $password);
                                            //  Возвращаем успех, если пароль задавать не нужно или результат операции установки пароля
  }
  
  /**
   *  Добавление ssh пользователя
   *  @param $username: Имя пользователя для соединения по ssh
   *  @param [$password]: Пароль пользователя для соединения по ssh
   *  @return (bool): успешность операции
   */
  function setRemoteUser($username, $password = "")
  {
    if (is_array($username)) {              //  Если вместо имени пользователя передали массив
      if (isset($username['username']) && isset($username['password']))
                                            //  Смотрим, если это ассоциативный массив с нужными полями
        $username = array($username['username'], $username['password']);
                                            //  Располагаем поля в необходимом порядке
      list($username, $password) = array_values($username);
                                            //  И преобразуем массив в переменные
    }
    if (trim($username) == "")              //  Если имени пользователя нет
      api::out("Не указано имя пользователя", 400);
                                            //  Возвращаем ошибку
    else                                    //  А если имя пользователя ssh передали
      $this->res['remoteuser'] = trim($username);
                                            //  Запоминаем его
    return ($password == "")?true:$this->setPasswords($password);
                                            //  Возвращаем успех, если пароль задавать не нужно или результат операции установки пароля
  }
  
  /**
   *  Добавление пароля пользователя
   *  @param $userpass: Пароль пользователя для соединения по ssh
   *  @param [$becomepass]: Пароль пользователя для перехода по sudo. При этом пользователь sudo обязательно должен быть объявлен заранее
   *  @return (bool): успешность операции
   */
  function setPasswords($userpass, $becomepass = "")
  {
    if (trim($userpass) == "" && $becomepass == "")
                                            //  Если пароля ssh и sudo нет
      api::out("Не указано имя пользователя и пароль", 400);
                                            //  Возвращаем ошибку
    if (trim($userpass) != "")              //  Если пароль ssh задан
      $this->res['userpass'] = trim($userpass);
                                            //  Запоминаем пароль
    if (trim($becomepass) == "" || !isset($this->res['becomeuser']))
                                            //  Если пароль для sudo не указан или не указан sudo пользователь
      unset($this->res['becomepass']);      //  Стираем его поле, при надобности, возьмём его из пароля для ssh
    else                                    //  А если указан
      $this->res['becomepass'] = trim($becomepass);
                                            //  Запоминаем пароль
    return true;                            //  Выходим с успехом  
  }
  
  /**
   *  Добавление/сброс переменных
   *  @param $name (string / array): Название переменной / ассоциативный массив ключ-значение
   *  @param [$value] (string/bool): Значение переменной, но если значение передано в $name, то указывает на флаг $clean
   *  @param [$clean] (bool): Флаг, указывающий, очищать переменные или нет
   *  @return (bool): true всегда
   */
  function setVars($name, $value = "", $clean = false)
  {
    if (is_array($name)) {                  //  Если первый параметр массив
      if ($value != "")                     //  Смотрим, был ли указан второй параметр, и если был
        $clean = $value;                    //  То тогда это передан флаг очистки
    }
    else                                    //  Если же передали название пермеменной
      $name = array($name => $value);       //  Формируем в ней массив
    if ($clean || !is_array($this->res['vars']))
                                            //  Если стоит флаг очистки массива переменных или массива ещё нет
      $this->res['vars'] = array();         //  Инициализируем его
    foreach ($name as $key => $value)       //  Начинаем перебирать переданные переменные
      $this->res['vars'][$key] = $value;    //  Запоминаем их в массиве переменных
    return true;                            //  Возвращаем успех
  }
  
  /**
   *  Подготовка шифрованных файлов переменных для ансибля
   *  @return:
   *    (bool) false: если шифрование закончилось неудачно
   *    (array): массив, содержащий поля vaultfile и passfile, непосредственно указывающие на файлы хранилища и пароля
   */
  function prepareVars()
  {
    $passcrypt = bin2hex(openssl_random_pseudo_bytes(10));
                                            //  Генерируем случайный пароль шифрования в 20 символов
    $passfile = tempnam(sys_get_temp_dir(), 'ansfw');
                                            //  Получаем имя для файла с паролем шифрования
    @file_put_contents($passfile, $passcrypt);
                                            //  Записываем в него получившийся пароль
    $vaultfile = tempnam(sys_get_temp_dir(), 'ansfw');
                                            //  Получаем имя файла для хранилища
    $vars = "";                             //  Инициализируем переменную списка переменных :)
    if (isset($this->res['userpass']))      //  Если указана переменная для коннекта по ssh
      $vars .= "ansible_ssh_pass: ".$this->res['userpass']."\n";
                                            //  Запоминаем её
#    if (isset($this->res['becomeuser']))    //  Если указан пользователь для sudo
      $vars .= "ansible_sudo_pass: ".((isset($this->res['becomepass']))?$this->res['becomepass']:$this->res['userpass'])."\n";
                                            //  Указываем ему свой пароль, если задан, или, если не задан, берём пароль ssh
    if (is_array($this->res['vars']))
      foreach ($this->res['vars'] as $key => $value)
                                            //  Начинаем перебирать переменные
        $vars .= $key.": ".$value."\n";     //  Заносим их в список по-одной
    @file_put_contents($vaultfile, $vars);  //  Записываем в файл хранилище список переменных

    //  Конвертируем пути в cygwin совместимые, если установлена переменная окружения CYGWIN
    if ($_SERVER['CYGWIN'] != "") {
        $vaultfile = preg_replace("/\s*([a-zA-Z]{1}):(?:\/|\\\)(.*)/", "/cygdrive/\\1/\\2", str_replace("\\", "/", $vaultfile));
        $passfile = preg_replace("/\s*([a-zA-Z]{1}):(?:\/|\\\)(.*)/", "/cygdrive/\\1/\\2", str_replace("\\", "/", $passfile));
        @exec("c:/etc/cygwin64/bin/chmod.exe 0600 ".escapeshellarg($passfile));
    }
        
    $cmd = ANSIBLEVAULT." encrypt ".escapeshellarg($vaultfile)." --vault-password-file ".escapeshellarg($passfile)." 2>&1";
    $out = exec($cmd);                      //  Шифруем файл хранилища
#    $out = exec($cmd, $outt);               //  Шифруем файл хранилища
#    file_put_contents("zzz", $cmd."\n\n\n".$out."\n\n\n".print_r($outt, true));
#    if (stripos(trim($out), "ERROR!") !== false) {
    if (trim($out) != "Encryption successful" && !($_SERVER['CYGWIN'] != "" && trim($out) == "")) {
                                            //  Смотрим, если шифрование прошло неудачно
      @unlink($passfile);                   //  Удаляем файл пароля шифрования
      @unlink($vaultfile);                  //  И файл хранилища
      api::out("Ошибка шифрования переменных для playbook", 400);
                                            //  Возвращаем ошибку
    }
    if (isset($this->res['vaultfile']))     //  Если файл хранилища был указан ранее
      @unlink($this->res['vaultfile']);     //  Удаляем его
    if (isset($this->res['passfile']))      //  Если файл с паролем шифрования был указан ранее
      @unlink($this->res['passfile']);      //  Удаляем его
    $this->res['vaultfile'] = $vaultfile;   //  Запоминаем путь к файлу хранилища
    $this->res['passfile'] = $passfile;     //  И к файлу с паролем шифрования
    return true;                            //  Возвращаем успех
  }
  
  /**
   *  Подготовка шифрованных файлов аутентификации для ансибля
   *  @param  $key:  Ключ, по которому будут извлечены данные из хранилища
   *  @return:
   *    (string): строка пароля из хранилища, если там хранилась строка
   *    (array): массив с данными из хранилища, если там хранилось множество данных
   */
  function getAuthFromVault($key)
  {
    class_exists("Vault") or die("class Vault is missing");
                                            //  Проверяем наличие класса Vault и, если его нет, то выходим
    $vault = new Vault();
    $password = $vault->decryptFromVault($key);
                                            //  Получаем данные из хранилища и
    $arr = @json_decode($password, true);   //  Пробуем их десериализовать
    return is_array($arr)?$arr:$password;   //  Если на выходе получился массив - возвращаем его, если нет, возвращаем оригинальную строку
  }
}
