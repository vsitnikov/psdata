<?php
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

namespace PSData;

/**
 * Класс хранилища с возможностью шифрования
 *
 * property bool   $init         Флаг инициализации класса<br>
 * property string $public_key   Публичный ключ<br>
 * property string $private_key  Приватный ключ<br>
 * property string $subdirectory Подкаталог для возможности разделения хранилища в пределах одного каталога<br>
 * <br>
 * <br>
 * Методы:<br>
 * method bool   setSubDirectory         Установка подкаталога, с которым будет идти работа (должен быть расположен в основном рабочем каталоге)<br>
 * method bool   setPrivateKey           Установка приватного ключа шифрования<br>
 * method bool   setPublicKey            Установка публичного ключа шифрования<br>
 * method bool   isCorrectRecordName     Проверка имени в хранилище на корректность<br>
 * method string cryptData               Шифрование данных<br>
 * method string decryptData             Расшифровка данных<br>
 * method bool   changeEncryptedData     Изменение шифрованных данных<br>
 * method bool   changeData              Изменение данных<br>
 * method bool   cryptToVault            Сохранение шифрованных данных в хранилище<br>
 * method bool   cryptToVaultWithCheck   Сохранение шифрованных данных в хранилище с проверкой на возможность перезаписи, если внутри контейнер vault<br>
 *                                         (см. описание класса) и у него нет блокирующей записи в поле access (оно пусто, либо содержит full или rw)
 * method string decryptFromVault        Получение шифрованных данных из хранилища (с расшифровкой)<br>
 * method bool   storeData               Сохранение записи в каталог хранилища (учитывается подкаталог в $this->subdirectory, если он указан)<br>
 * method string restoreData             Считывание записи из каталога хранилища<br>
 * method bool   isVaultKey              Проверка записи в хранилище<br>
 * method bool   deleteVaultKey          Удаление записи из хранилища<br>
 * method bool   deleteVaultKeyWithCheck Удаление записи из хранилища с проверкой на возможность удаления, если внутри контейнер vault<br>
 *                                         (см. описание класса) и у него нет блокирующей записи в поле access (оно пусто, либо содержит full или rw)
 * <br>
 *<br>
 *  Типы записей:<br>
 *  - любой текст<br>
 *  - любой массив в json<br>
 *  - контейнер<br>
 *    array(<br>
 *      "type"    => vaultcontainer<br>
 *      "access"  => full (по-умолчанию, всем), ro (только чтение), rw (чтение и изменение)<br>
 *      "return"  => all (по-умолчанию, вернуть весь массив), onlydata (только данные)<br>
 *      "data"    => данные, которые нужно зашифровать<br>
 *      ...       => любые другие поля<br>
 *    )<br>
 *<br>
 * @author    Vyacheslav Sitnikov <vyacheslav.sitnikov@megafon.ru>
 * @version   1.0
 * @package   MegaFon\PSData
 * @copyright Copyright (c) 2016-2017 ПАО "МегаФон"
 */
class Vault
{
  /** @var bool Флаг инициализации класса */
  public $init = false;
  /** @var string Публичный ключ */
  private $public_key = "";
  /** @var string Приватный ключ */
  private $private_key = "";
  /** @var string Подкаталог для возможности разделения хранилища в пределах одного каталога*/
  private $subdirectory = "";
  
  /**
   * Vault конструктор.
   *
   * @param array|string $private_key_file [optional] Путь к файлу приватного ключа, если не указан, будет взят из константы VAULT_PRIVATE_KEY_FILE<br>
   *                                       Может быть KV массивом, в котором передаются параметры функции
   * @param string       $public_key_file  [optional] Путь к файлу публичного ключа, если не указан, будет взят из константы VAULT_PUBLIC_KEY_FILE
   */
  function __construct($private_key_file = null, $public_key_file = null)
  {
    if (is_null($private_key_file))
      $private_key_file = VAULT_PRIVATE_KEY_FILE;
    if (is_null($public_key_file))
      $public_key_file = VAULT_PUBLIC_KEY_FILE;
    
    //  Обработка ситуации, когда параметры переданы в массиве
    if (is_array($private_key_file)) {
      if (trim($private_key_file['public_key_file']) != "")
        $public_key_file = $private_key_file['public_key_file'];
      if (trim($private_key_file['private_key_file']) != "")
        $private_key_file = $private_key_file['private_key_file'];
      else
        $private_key_file = "";
    }
    $this->init = $this->setPrivateKey($private_key_file) && $this->setPublicKey($public_key_file);
  }
  
  /**
   *  Установка подкаталога, с которым будет идти работа (должен быть расположен в основном рабочем каталоге)
   *
   * @param string $subdirectory_path Название подкаталога
   *
   * @return bool Результат операции (всегда)
   */
  function setSubDirectory($subdirectory_path)
  {
    //  Убираем всё левое из пути, оставляя только буквы, цифры, значи подчёркивания и тире
    $subdirectory_path = preg_replace("/(.*?)([\w\d\-_]+)/", "\\2", $subdirectory_path);
    
    $this->subdirectory = trim($subdirectory_path != "") ? $subdirectory_path."/" : "";
    return true;
  }
  
  /**
   *  Установка приватного ключа шифрования
   *
   * @param string $private_key_file Путь к приватному ключу шифрования
   *
   * @return bool Результат операции
   */
  function setPrivateKey($private_key_file)
  {
    if (!file_exists($private_key_file))
      return false;
    $this->private_key = file_get_contents($private_key_file);
    return true;
  }
  
  /**
   *  Установка публичного ключа шифрования
   *
   * @param string $public_key_file Путь к публичному ключу шифрования
   *
   * @return bool Результат операции
   */
  function setPublicKey($public_key_file)
  {
    if (!file_exists($public_key_file))
      return false;
    $this->public_key = file_get_contents($public_key_file);
    return true;
  }
  
  /**
   *  Проверка имени в хранилище на корректность
   *
   * @param string $name Название записи в хранилище (английские буквы, цифры, знаки - и _)
   *
   * @return bool Результат проверки
   */
  function isCorrectRecordName($name)
  {
    $name = strtoupper(trim($name));
    
    //  Возвращаем результат проверки на неправильные символы
    return preg_match("/^[0-9A-Z\-_]+$/", $name) ? $name : false;
  }
  
  /**
   *  Шифрование данных
   *
   * @param string $data_for_crypt Данные, которые необходимо зашифровать
   * @param string $public_key     [optional] Содержимое публичного ключа, если не указан, будет взят из переменной класса $this->public_key
   *
   * @return string Зашифрованные данные
   */
  function cryptData($data_for_crypt, $public_key = null)
  {
    if (is_null($public_key))
      $public_key = $this->public_key;
    $encrypted_data = "";
    openssl_public_encrypt($data_for_crypt, $encrypted_data, $public_key);
    return $encrypted_data;
  }
  
  /**
   *  Расшифровка данных
   *
   * @param string $encrypted_data Данные, которые необходимо расшифровать
   * @param string $private_key    [optional] Содержимое приватного ключа, если не указан, будет взят из переменной класса $this->private_key
   *
   * @return string Расшифрованные данные
   */
  function decryptData($encrypted_data, $private_key = null)
  {
    if (is_null($private_key))
      $private_key = $this->private_key;
    $decrypted_data = "";
    openssl_private_decrypt($encrypted_data, $decrypted_data, $private_key);
    return $decrypted_data;
  }
  
  /**
   *  Изменение шифрованных данных
   *
   * @param string $record_name Название записи в хранилище (английские буквы, цифры, знаки - и _)
   * @param string $field_name  Название поля (ключ массива), значение которое необходимо перезаписать
   * @param string $data        Данные, которые необходимо записать в поле массива. Если значение пусто, то поле удаляется
   * @param string $vault_path  [optional] Путь к каталогу храналища, если не указан, то берётся из константы VAULT_PATH с подкаталогом в $this->subdirectory
   *
   * @return bool Результат операции
   */
  function changeEncryptedData($record_name, $field_name, $data, $vault_path = null)
  {
    if (is_null($vault_path))
      $vault_path = VAULT_PATH."/".$this->subdirectory;
    
    //  Выходим по ошибке, если название содержит неправильные символы
    if (!($record_name = $this->isCorrectRecordName($record_name)))
      return false;
    
    //  Выходим по ошибке, если файла записи нет, или файл есть, но внутри него хранится не массив
    if (!$this->isVaultKey($record_name, $vault_path) || !is_array($dat = json_decode($this->decryptFromVault($record_name, $vault_path), true)) ||
      $dat['type'] != "vaultcontainer"
    )
      return false;
    
    if (trim($data) != "")
      $dat[$field_name] = $data;
    else
      unset($dat[$field_name]);
    return $this->cryptToVault(json_encode($dat, JSON_UNESCAPED_UNICODE), $record_name, $vault_path);
  }
  
  /**
   *  Изменение данных
   *
   * @param string $record_name Название записи в хранилище (английские буквы, цифры, знаки - и _)
   * @param string $field_name  Название поля (ключ массива), значение которое необходимо перезаписать
   * @param string $data        Данные, которые необходимо записать в поле массива. Если значение пусто, то поле удаляется
   * @param string $vault_path  [optional] Путь к каталогу храналища, если не указан, то берётся из константы VAULT_PATH с подкаталогом в $this->subdirectory
   *
   * @return bool Результат операции
   */
  function changeData($record_name, $field_name, $data, $vault_path = null)
  {
    if (is_null($vault_path))
      $vault_path = VAULT_PATH."/".$this->subdirectory;
    
    //  Выходим по ошибке, если название содержит неправильные символы
    if (!($record_name = $this->isCorrectRecordName($record_name)))
      return false;
    
    //  Выходим по ошибке, если файла записи нет, или файл есть, но внутри него хранится не массив
    if (!$this->isVaultKey($record_name, $vault_path) || !is_array($dat = json_decode($this->decryptFromVault($record_name, $vault_path),
        true)) || $dat['type'] != "vaultcontainer"
    )
      return false;
    
    if (trim($data) != "")
      $dat[$field_name] = $data;
    else
      unset($dat[$field_name]);
    return $this->cryptToVault(json_encode($dat, JSON_UNESCAPED_UNICODE), $record_name, $vault_path);
  }
  
  /**
   *  Сохранение шифрованных данных в хранилище
   *
   * @param string $data        Данные, которые необходимо зашифровать
   * @param string $record_name Название записи в хранилище (английские буквы, цифры, знаки - и _)
   * @param string $vault_path  [optional] Путь к каталогу храналища, если не указан, то берётся из константы VAULT_PATH с подкаталогом в $this->subdirectory
   *
   * @return bool Результат операции
   */
  function cryptToVault($data, $record_name, $vault_path = null)
  {
    if (is_null($vault_path))
      $vault_path = VAULT_PATH."/".$this->subdirectory;
    
    //  Если размер данных не превышает 300 байт, шифруем открытым ключём, если превышает, генерируем случайный пароль из 16 байт и шифруем его
    $initialization_vector = random_bytes(16);
    $full_data = "";
    if (strlen($data) < 300)
      $crypt_data = $this->cryptData($data);
    else {
      $crypt_pass = bin2hex(random_bytes(16));
      $crypt_data = $this->cryptData($crypt_pass."PSDATASEPARATOR".$initialization_vector);
      
      //  Шифруем основные данные паролем из md5 имени файла, зашифрованного функцией Crypto и используя случайный пароль в качестве вектора инициализации 
      $full_data = openssl_encrypt($data, "AES-256-CBC", $crypt_pass, OPENSSL_RAW_DATA, $initialization_vector);
    }
    
    //  Сохраняем зашифрованные данные 
    $result = (bool)$this->storeData($crypt_data, $record_name, $vault_path);
    
    //  Сохраняем основные шифрованные данные, если они есть
    if ($full_data != "")
      $result &= (bool)$this->storeData($full_data, "I{$record_name}", $vault_path);
    return $result;
  }
  
  /**
   *  Сохранение шифрованных данных в хранилище с проверкой на возможность перезаписи, если внутри контейнер vault (см. описание класса) и у него нет<br>
   * блокирующей записи в поле access (оно пусто, либо содержит full или rw)
   *
   * @param string $data        Данные, которые необходимо зашифровать
   * @param string $record_name Название записи в хранилище (английские буквы, цифры, знаки - и _)
   * @param string $vault_path  [optional] Путь к каталогу храналища, если не указан, то берётся из константы VAULT_PATH с подкаталогом в $this->subdirectory
   *
   * @return bool Результат операции
   */
  function cryptToVaultWithCheck($data, $record_name, $vault_path = null)
  {
    if (is_null($vault_path))
      $vault_path = VAULT_PATH."/".$this->subdirectory;
    
    //  Выходим по ошибке, если название содержит неправильные символы
    if (!($record_name = $this->isCorrectRecordName($record_name)))
      return false;
    
    //  Выходим по ошибке, если файл записи присутствует,  и содержит в описании, что это объект с защитой записи
    if ($this->isVaultKey($record_name, $vault_path) && is_array($dat = json_decode($this->decryptFromVault($record_name, $vault_path), true)) &&
      $dat['type'] == "vaultcontainer" && !($dat['access'] == "" || $dat['access'] == "full" || $dat['access'] == "rw")
    ) {
      return false;
    }
    return $this->cryptToVault($data, $record_name, $vault_path);
  }
  
  /**
   *  Получение шифрованных данных из хранилища (с расшифровкой)
   *
   * @param string $record_name Название записи в хранилище (английские буквы, цифры, знаки - и _)
   * @param string $vault_path  [optional] Путь к каталогу храналища, если не указан, то берётся из константы VAULT_PATH с подкаталогом в $this->subdirectory
   *
   * @return string Расшифрованные данные
   */
  function decryptFromVault($record_name, $vault_path = null)
  {
    if (is_null($vault_path))
      $vault_path = VAULT_PATH."/".$this->subdirectory;
    
    //  Восстанавливаем данные из хранилища, расшифровываем и возвращаем их в открытом виде
    $data = $this->decryptData($this->restoreData($record_name, $vault_path));
    $base_data = $this->restoreData("I{$record_name}", $vault_path);
    if ($base_data !== false) {
      list($crypt_pass, $initialization_vector) = explode("PSDATASEPARATOR", $data);
      $data = $newdata = openssl_decrypt($base_data, "AES-256-CBC", $crypt_pass, OPENSSL_RAW_DATA, $initialization_vector);
    }
    return $data;
  }
  
  /**
   *  Сохранение записи в каталог хранилища (учитывается подкаталог в $this->subdirectory, если он указан)
   *
   * @param string $data        Данные, который необходимо зашифровать
   * @param string $record_name Название записи в хранилище (английские буквы, цифры, знаки - и _)
   * @param string $vault_path  [optional] Путь к каталогу храналища, если не указан, то берётся из константы VAULT_PATH с подкаталогом в $this->subdirectory
   *
   * @return bool Результат операции
   */
  function storeData($data, $record_name, $vault_path = null)
  {
    if (is_null($vault_path))
      $vault_path = VAULT_PATH."/".$this->subdirectory;
    
    //  Возвращаем ошибку, если название содержит неправильные символы
    if (!($record_name = $this->isCorrectRecordName($record_name)))
      return false;
    
    @mkdir($vault_path, 0775, true);
    
    //  Запись файла через промежуточный файл
    $temporary_record_name = $record_name.bin2hex(random_bytes(2));
    $result = file_put_contents($vault_path."/".$temporary_record_name, $data);
    rename($vault_path."/".$temporary_record_name, $vault_path."/".$record_name);
    return $result;
  }
  
  /**
   *  Считывание записи из каталога хранилища
   *
   * @param string $record_name Название записи в хранилище (английские буквы, цифры, знаки - и _)
   * @param string $vault_path  [optional] Путь к каталогу храналища, если не указан, то берётся из константы VAULT_PATH с подкаталогом в $this->subdirectory
   *
   * @return string Сырая запись из хранилища
   */
  function restoreData($record_name, $vault_path = null)
  {
    if (is_null($vault_path))
      $vault_path = VAULT_PATH."/".$this->subdirectory;
    
    //  Возвращаем ошибку, если название содержит неправильные символы
    if (!($record_name = $this->isCorrectRecordName($record_name)))
      return false;
    
    $result = @file_get_contents($vault_path."/".$record_name);
    return $result;
  }
  
  /**
   *  Проверка записи в хранилище
   *
   * @param string $record_name Название записи в хранилище (английские буквы, цифры, знаки - и _)
   * @param string $vault_path  [optional] Путь к каталогу храналища, если не указан, то берётся из константы VAULT_PATH с подкаталогом в $this->subdirectory
   *
   * @return bool Результит операции
   */
  function isVaultKey($record_name, $vault_path = null)
  {
    if (is_null($vault_path))
      $vault_path = VAULT_PATH."/".$this->subdirectory;
    
    //  Возвращаем ошибку, если название содержит неправильные символы
    if (!($record_name = $this->isCorrectRecordName($record_name)))
      return false;
    
    $result = file_exists($vault_path."/".$record_name);
    return $result;
  }
  
  /**
   *  Удаление записи из хранилища
   *
   * @param string $record_name Название записи в хранилище (английские буквы, цифры, знаки - и _)
   * @param string $vault_path  [optional] Путь к каталогу храналища, если не указан, то берётся из константы VAULT_PATH с подкаталогом в $this->subdirectory
   *
   * @return bool Результит операции
   */
  function deleteVaultKey($record_name, $vault_path = null)
  {
    if (is_null($vault_path))
      $vault_path = VAULT_PATH."/".$this->subdirectory;
    
    //  Возвращаем ошибку, если название содержит неправильные символы
    if (!($record_name = $this->isCorrectRecordName($record_name)))
      return false;
    
    @unlink($vault_path."/I".$record_name);
    $result = @unlink($vault_path."/".$record_name);
    return $result;
  }
  
  /**
   *  Удаление записи из хранилища с проверкой на возможность удаления, если внутри контейнер vault (см. описание класса) и у него нет<br>
   * блокирующей записи в поле access (оно пусто, либо содержит full или rw)
   *
   * @param string $record_name Название записи в хранилище (английские буквы, цифры, знаки - и _)
   * @param string $vault_path  [optional] Путь к каталогу храналища, если не указан, то берётся из константы VAULT_PATH с подкаталогом в $this->subdirectory
   *
   * @return bool Результит операции
   */
  function deleteVaultKeyWithCheck($record_name, $vault_path = null)
  {
    if (is_null($vault_path))
      $vault_path = VAULT_PATH."/".$this->subdirectory;
    
    //  Возвращаем ошибку, если название содержит неправильные символы
    if (!($record_name = $this->isCorrectRecordName($record_name)))
      return false;
    
    //  Если файла с таким ключём в хранилище нет, то всё равно возвращаем успех, чтобы нельзя было перебором узнать названия ключей в хранилище
    if (!$this->isVaultKey($record_name, $vault_path))
      return true;
    
    //  Возвращаем результат удаления файла из хранилища, если данные - просто текст или объект, но без зашиты
    if (!is_array($dat = json_decode($this->decryptFromVault($record_name, $vault_path), true)) ||
      $dat['type'] != "vaultcontainer" || $dat['access'] == "" || $dat['access'] == "full"
    ) {
      return $this->deleteVaultKey($record_name, $vault_path);
    }
    return false;
  }
}
