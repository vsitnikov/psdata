<?php
/******************************************************************************
 *
 * (C) 2019 by Vyacheslav Sitnikov (vsitnikov@gmail.com)
 *
 ******************************************************************************/

//  Указываем пространство имён
namespace PSData;

//  Будем использовать класс KeePassPHP
use \KeePassPHP\KeePassPHP;

/**
 * Класс работы с базой KeePass
 *
 * property bool                     $init         Флаг инициализации класса<br>
 * property string                   $public_key   Путь к файлу базы<br>
 * property bool                     $private_key  Флаг того, что данные расположены в sharedmemory<br>
 * property string                   $subdirectory Путь к файлу ключа, зашифрованному средствами хранилища<br>
 * property string                   $subdirectory Пароль базы<br>
 * property KeePassPHP\Database      $subdirectory Объект базы данных<br>
 * property \KeePassPHP\CompositeKey $subdirectory Объект ключа доступа к базе<br>
 * property \KeePassPHP\Group[]      $subdirectory Структура базы (массив групп)<br>
 * property int                      $subdirectory Указатель на объект в памяти<br>
 * <br>
 * <br>
 * Методы:<br>
 * method bool         setDatabase      Установка пути к базе KeePass<br>
 * method bool         setPassword      Установка пароля для открытия базы KeePass<br>
 * method bool         setKeyFile       Установка пути к файлу ключа для открытии базы KeePass<br>
 * method bool         openDatabase     Открытие файла базы KeePass<br>
 * method bool         storeInMemory    Сохранение базы паролей в shared memory<br>
 * method bool         deleteFromMemory Удаление данных из shared memory<br>
 * method array        getStructure     Получение иерархического массива логинов/паролей<br>
 * method bool|array   getEntryList     Получение информации о списке записей по указанному пути<br>
 * method bool|array   getEntryInfo     Получение информации о записи по указанному пути<br>
 * method string|array getEntry         Получение информации о записи/списке записей по указанному пути<br>
 * method string       getPassword      Получение пароля записи<br>
 * <br>
 *<br>
 *  Типы записей:<br>
 *  - любой текст<br>
 *  - любой массив в json<br>
 *  - контейнер<br>
 *    array(<br>
 *      "type"    => vaultcontainer<br>
 *      "access"  => full (по-умолчанию, всем), ro (только чтение), rw (чтение и изменение)<br>
 *      "return"  => all (по-умолчанию, вернуть весь массив), onlydata (только данные)<br>
 *      "data"    => данные, которые нужно зашифровать<br>
 *      ...       => любые другие поля<br>
 *    )<br>
 *<br>
 *
 * @author    Vyacheslav Sitnikov <vsitnikov@gmail.com>
 * @version   1.0
 * @package   PSData
 * @copyright Copyright (c) 2019
 */
class KeePass
{
    /** @var bool Флаг инициализации класса */
    public $init = false;
    
    /** @var string Путь к файлу базы */
    public $path_to_database_file = "";
    
    /** @var bool Флаг того, что данные расположены в sharedmemory */
    public $in_memory = false;
    
    /** @var string Путь к файлу ключа, зашифрованному средствами хранилища */
    private $key_file = null;
    
    /** @var string Пароль базы */
    private $password = null;
    
    /** @var \KeePassPHP\Database Объект базы данных */
    private $db = "";
    
    /** @var array Объект базы данных */
    private $db_structure = "";
    
    /** @var \KeePassPHP\CompositeKey Объект ключа доступа к базе */
    private $composite_key = "";
    
    /** @var \KeePassPHP\Group[] Структура базы (массив групп) */
    private $array_of_groups = "";
    
    /** @var int Указатель на объект в памяти */
    private $memory_database_id = null;
    
    /** @var bool Признак отладки */
    private $debug = false;
    
    /** @var string Признак отладки */
    private $debug_file = null;
    
    
    /**
     *  KeePass constructor.
     *
     * Вызывает метод открытия базы KeePass
     *
     * @param string|array $database_file Путь до файла базы KeePass. Может быть KV массивом, в котором передаются параметры функции
     * @param string       $password      Пароль к базе KeePass
     * @param null         $key_file      Путь к ключу доступа к базе KeePass
     *
     * @return bool|KeePass Возвращает результат вызова метода открытия базы KeePass
     */
    function __construct($database_file = "", $password = "", $key_file = null)
    {
        $class = basename(str_replace("\\", "/", __CLASS__));
        if (defined("{$class}_debug") && constant("{$class}_debug") === true) {
            $this->debug = true;
            if (defined("{$class}_debug_file"))
                $this->debug_file = constant("{$class}_debug_file");
            else
                $this->debug_file = dirname(__FILE__)."/debug.log";
        }
        return $this->openDatabase($database_file, $password, $key_file);
    }
    
    /**
     *  KeePass destructor
     */
    function __destruct()
    {
        //  Закрываем дескриптор в памяти, если в момент уничтожения объекта он почему-то оставался открытым
        if ($this->memory_database_id)
            @shmop_close($this->memory_database_id);
    }
    
    /**
     *  Установка пути к базе KeePass
     *
     * @param string $path_to_database_file Путь к базе KeePass
     *
     * @return bool
     */
    function setDatabase($path_to_database_file)
    {
        if (!file_exists($path_to_database_file))
            return false;
        $this->path_to_database_file = $path_to_database_file;
        return true;
    }
    
    /**
     *  Установка пароля для открытия базы KeePass
     *
     * @param string $password Пароль к базе KeePass
     *
     * @return bool
     */
    function setPassword($password)
    {
        $this->password = $password;
        return true;
    }
    
    /**
     *  Установка пути к файлу ключа для открытии базы KeePass
     *
     * @param string $key_file Путь к файлу ключа для базы KeePass
     *
     * @return bool
     */
    function setKeyFile($key_file)
    {
        if ($key_file != "" && $key_file != null && !file_exists($key_file))
            return false;
        $this->key_file = ($key_file == "") ? null : $key_file;
        return true;
    }
    
    /**
     *  Открытие файла базы KeePass
     *
     * @param string|array $database_file Путь до файла базы KeePass. Может быть KV массивом, в котором передаются параметры функции
     * @param string       $password      Пароль к базе KeePass
     * @param string       $key_file      Путь к ключу доступа к базе KeePass
     *
     * @return bool Возвращает результат открытия базы KeePass
     */
    function openDatabase($database_file = "", $password = "", $key_file = "")
    {
        $extra = "";
        //  Обработка ситуации, когда параметры в функцию переданны в KV массиве
        if (is_array($database_file)) {
            if (trim($database_file['password']) != "")
                $password = $database_file['password'];
            if (trim($database_file['key_file']) != "")
                $key_file = $database_file['key_file'];
            if (trim($database_file['extra']) != "")
                $extra = $database_file['extra'];
            if (trim($database_file['database_file']) != "")
                $database_file = $database_file['database_file'];
            else
                $database_file = "";
        }
        if ($extra == "")
            $extra = '/.*/';
        
        //  Если не указан пароль, но файл базы существует
        if ($password == "" && is_file($database_file)) {
            
            //  Генерируем ключ shared memory (полный путь к БД с идентифткатором "k" (KeePass)) и открываем память на чтение
            $database_key = ftok($database_file, "k");
            $memory_database_id = @shmop_open($database_key, "a", 0, 0);
            if ($this->debug)
                file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [openDatabase] Attempt to open memory access for data".PHP_EOL, FILE_APPEND);
            if (!is_resource($memory_database_id)) {
                if ($this->debug)
                    file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [openDatabase] FAIL - {$database_key}".PHP_EOL, FILE_APPEND);
                return false;
            }
            if ($this->debug)
                file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [openDatabase] SUCCESS - {$database_key}".PHP_EOL, FILE_APPEND);
            
            //  Генерируем ключ shared memory (путь к БД с идентификатором "t" (time) - время занесения в память) и открываем память на чтение
            $time_key = ftok($database_file, "t");
            $memory_time_id = shmop_open($time_key, "a", 0, 0);
            if ($this->debug)
                file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [openDatabase] Attempt to open memory access for time".PHP_EOL, FILE_APPEND);
            if (!is_resource($memory_time_id)) {
                if ($this->debug)
                    file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [openDatabase] FAIL - {$time_key}".PHP_EOL, FILE_APPEND);
                @shmop_close($memory_database_id);
                return false;
            }
            if ($this->debug)
                file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [openDatabase] SUCCESS - {$time_key}".PHP_EOL, FILE_APPEND);
            
            //  Считываем время занесения информации из базы в память
            $time = shmop_read($memory_time_id, 0, shmop_size($memory_time_id));
            
            //  Выходим с неудачей, если с момента занесения прошло больше времени, чем нужно 
            if (time() - $time > 600) {
                if ($this->debug)
                    file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [openDatabase] Time expiried - FALSE".PHP_EOL, FILE_APPEND);
                @shmop_close($memory_database_id);
                @shmop_close($memory_time_id);
                if ($this->debug)
                    file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [openDatabase] Attempt to delete".PHP_EOL, FILE_APPEND);
                $this->deleteFromMemory($time_key);
                $this->deleteFromMemory($database_key);
                if ($this->debug)
                    file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [openDatabase] Delete complete".PHP_EOL, FILE_APPEND);
                return false;
            }
            
            //  Сохраняем идентификатор памяти в параметре класса, выставляем признак того, что база в памяти и выходим с успехом
            $this->memory_database_id = $memory_database_id;
            
            //  Заносим данные из памяти в переменную класса
            $this->db_structure = json_decode(shmop_read($this->memory_database_id, 0, shmop_size($this->memory_database_id)), true);
            
            @shmop_close($memory_database_id);
            @shmop_close($memory_time_id);
            return ($this->in_memory = true);
        }
        
        //  Возвращаем неудачу, если были указаны параметры (файл БД, пароль или ключ), но их не удалось установить
        if (($database_file != "" && !$this->setDatabase($database_file)) || ($password != "" && !$this->setPassword($password)) || ($key_file != "" && !$this->setKeyFile($key_file)))
            return false;
        
        //  Инициализируем объект пароля и формируем его из самого пароля и ключа доступа (если он указан)
        $this->composite_key = KeePassPHP::masterKey();
        KeePassPHP::addPassword($this->composite_key, $this->password);
        if (!KeePassPHP::addKeyFile($this->composite_key, $this->key_file))
            return false;
        
        //  Открываем базу KeePass с помощью объекта пароля
        $this->db = KeePassPHP::openDatabaseFile($this->path_to_database_file, $this->composite_key, $error, $extra);
        if (is_null($this->db))
            return false;
        
        //  Получаем список групп, выставляем отметку, что база инициализировалась и выходим с успехом
        $this->array_of_groups = $this->db->getGroups();
        $this->db_structure = $this->getStructure();
        return ($this->init = true);
    }
    
    /**
     *  Сохранение базы паролей в shared memory
     *
     * @return bool Результат операции
     */
    function storeInMemory()
    {
        //  Выходим, если БД не инициализирована
        if (!$this->init)
            return false;
        
        //  Получаем данные и сохраняем массив данных в JSON
        $database_in_json = json_encode($this->db_structure, JSON_UNESCAPED_UNICODE);
        
        $current_time = (string)time();
        
        //  Генерируем ключ (полный путь к БД с идентификатором "t" (time)) и открываем память для создания (предварительно удалив всё по данному ключу)
        $time_key = ftok($this->path_to_database_file, "t");
        @$this->deleteFromMemory($time_key);
        $memory_time_id = @shmop_open($time_key, "n", 0600, strlen($current_time));
        if ($this->debug)
            file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [StoreInMemory] Attempt to open memory allocate for time".PHP_EOL, FILE_APPEND);
        if (!is_resource($memory_time_id)) {
            if ($this->debug)
                file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [StoreInMemory] FAIL - {$time_key}".PHP_EOL, FILE_APPEND);
            return $this->in_memory = false;
        }
        if ($this->debug)
            file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [StoreInMemory] SUCCESS - {$time_key}".PHP_EOL, FILE_APPEND);
        
        //  Генерируем ключ (полный путь к БД с идентификатором "k" (KeePass)) и открываем память для создания (предварительно удалив всё по данному ключу)
        $database_key = ftok($this->path_to_database_file, 'k');
        @$this->deleteFromMemory($database_key);
        $memory_database_id = @shmop_open($database_key, "n", 0600, strlen($database_in_json));
        if ($this->debug)
            file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [StoreInMemory] Attempt to open memory allocate for data".PHP_EOL, FILE_APPEND);
        if (!is_resource($memory_database_id)) {
            if ($this->debug)
                file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [StoreInMemory] FAIL - {$database_key}".PHP_EOL, FILE_APPEND);
            @shmop_close($memory_time_id);
            return $this->in_memory = false;
        }
        if ($this->debug)
            file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [StoreInMemory] SUCCESS - {$database_key}".PHP_EOL, FILE_APPEND);
        
        $memory_bytes_written = shmop_write($memory_database_id, $database_in_json, 0);
        if ($this->debug)
            file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [StoreInMemory] Attempt to write memory for data".PHP_EOL, FILE_APPEND);
        if ($memory_bytes_written != strlen($database_in_json)) {
            if ($this->debug)
                file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [StoreInMemory] FAIL - {$database_key}".PHP_EOL, FILE_APPEND);
            @shmop_close($memory_database_id);
            @shmop_close($memory_time_id);
            return $this->in_memory = false;
        }
        if ($this->debug)
            file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [StoreInMemory] SUCCESS - {$database_key}".PHP_EOL, FILE_APPEND);
        @shmop_write($memory_time_id, $current_time, 0);
        @shmop_close($memory_database_id);
        @shmop_close($memory_time_id);
        
        //  Сохраняем идентификатор памяти в переменной класса и выходим с успехом, выставив флаг того, что данные сохранены в shared memory
        $this->memory_database_id = $memory_database_id;
        return $this->in_memory = true;
    }
    
    /**
     *  Удаление данных из shared memory
     *
     * @param int $memory_key Идентификатор блока памяти
     *
     * @return bool результат операции
     */
    function deleteFromMemory($memory_key)
    {
        //  Открываем память на запись, удаляем её, закрываем и выходим
        $memory_id = @shmop_open($memory_key, "w", 0600, 0);
        if ($this->debug)
            file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [DeleteFromMemory] Attempt to open memory write".PHP_EOL, FILE_APPEND);
        if (!is_resource($memory_id)) {
            if ($this->debug)
                file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [DeleteFromMemory] FAIL - {$memory_key}".PHP_EOL, FILE_APPEND);
            return false;
        }
        if ($this->debug)
            file_put_contents($this->debug_file, date("d.m.Y H:i:s")." [DeleteFromMemory] SUCCESS - {$memory_key}".PHP_EOL, FILE_APPEND);
        @shmop_delete($memory_id);
        @shmop_close($memory_id);
        return true;
    }
    
    /**
     *  Получение иерархического массива логинов/паролей
     *
     * @param \KeePassPHP\Group[] $groups [optional] Массив со структурой
     * @param null|array          $access [optional] Массив со списком доступов (_allow, _deny, _order, _tns, _server)
     *
     * @return array Массив с данными по текущему уровню
     */
    private function getStructure($groups = null, $access = null)
    {
        if (is_null($access))
            $access = ["allow" => [], "deny" => [], "order" => [], "tns" => [], "server" => []];
        if (is_null($groups))
            $groups = $this->array_of_groups;
        $storage = [];
        
        //  Если это первичный вызов ($groups может быть массивом только в этом случае)
        //  Запоминаем то, что получилось после рекурсивного вызова с идентификатором группы
        if (is_array($groups))
            foreach ($groups as &$group_id)
                /** @var $group_id \KeePassPHP\Group */
                $storage[$group_id->name] = $this->getStructure($group_id, $access);
        
        //  Если в группе присутствуют данные, берём родительские списки доступов, запоминаем имя пользователя, пароль и uuid
        /** @var $groups \KeePassPHP\Group */
        if ($groups->entries != null) {
            $local_data = [];
            foreach ($access as $access_key => $access_value)
                $local_data[$access_key] = $access_value[sizeof($access_value) - 1];
            foreach ($groups->entries as &$entry_id) {
                /** @var $entry_id \KeePassPHP\Entry */
                if (substr(trim($entry_id->title), 0, 1) == "_") {
                    $local_data[substr(strtolower(trim($entry_id->title)), 1)] = $entry_id->username;
                    $access[substr(strtolower(trim($entry_id->title)), 1)][] = $entry_id->username;
                    continue;
                }
                $storage[$entry_id->title] = array(
                    "username" => $entry_id->username,
                    "password" => $this->getPassword($entry_id->uuid),
                    "uuid"     => $entry_id->uuid,
                    "extra"    => $entry_id->extra,
                );
                
                //  Обогащаем поля правами, которые указаны в самой записи (если указаны)
                if (is_array($entry_id->extra)) {
                    foreach ($entry_id->extra as $item_key => $item_value) {
                        if (substr(trim($item_key), 0, 1) == "_")
                            $local_data[substr(strtolower(trim($item_key)), 1)] = $item_value;
                    }
                }
            }
            foreach ($storage as $storage_key => $storage_value) {
                /** @var $entry_id \KeePassPHP\Entry */
                $storage[$storage_key] = array_merge($storage_value, $local_data);
            }
        }
        
        //  Если в группе присутствуют другие группы, запоминаем информацию по ним, рекурсивно вызывая себя на каждом элементе 
        /** @var $groups \KeePassPHP\Group */
        if ($groups->groups != null)
            foreach ($groups->groups as &$group_id)
                /** @var $group_id \KeePassPHP\Group */
                $storage[$group_id->name] = $this->getStructure($group_id, $access);
        
        //  Возвращаем данные, полученные на текущем уровне
        return $storage;
    }
    
    /**
     *  Получение информации о списке записей по указанному пути
     *
     * @param string $path Путь до записи в формате: "Group1/Group2/.../GroupN/Entry" или "Group1/Group2/.../GroupN"
     *
     * @return bool|array
     *   false, если путь отсутствует <br>
     *   массив с информацией по записям в группе (не содержит пароль)
     */
    function getEntryList($path)
    {
        $database_data = $this->db_structure;
        
        //  Начинаем перебирать составляющие пути, но поскольку нас интересует группа, отрезая последний сегмент (в котором название записи, а не группа)
        $path = explode("/", $path);
        for ($i = 0; $i < sizeof($path) - 1; $i++) {
            
            //  Заглушка Мегафон (не забыть исправить, условие ВСЕГДА работает, а не только на базах ГФ
            //  Подменяем название сегмента, если используется база KeePass, хранящая данные для БД Oracle ЕБ и нужно делать маппинг
            if (USEGFDATABASEKEEPASS && substr($path[$i], 0, 6) == "GFBIS_")
                $path[$i] = "GFBIS";
            
            //  Создаём новый массив, из указанного сегмента
            $database_data = $database_data[$path[$i]];
        }
        
        //  Выходим с ошибкой, если полученный сегмент пуст 
        if (!is_array($database_data) || !sizeof($database_data[$path[$i]]))
            return false;
        
        return $database_data;
    }
    
    /**
     *  Получение информации о записи по указанному пути
     *
     * @param string $path Путь до записи в формате: "Group1/Group2/.../GroupN/Entry" или "Group1/Group2/.../GroupN"
     *
     * @return bool|array
     *   false, если путь указывает на группу или несуществующий путь;
     *   массив с информацией о записи
     */
    function getEntryInfo($path)
    {
        $database_data = $this->db_structure;
        
        //  Начинаем перебирать составляющие пути
        $path = explode("/", $path);
        for ($i = 0; $i < sizeof($path); $i++) {
            
            //  Создаём новый массив, из указанного сегмента
            $database_data = $database_data[$path[$i]];
        }
        
        return $database_data;
    }
    
    /**
     *  Поиск пользователя по базе KeePass
     *
     * @param string $username Имя пользователя для поиска
     * @param null|array  Массив    со структурой базы KeePass (результат работы $this->getStructure)
     * @param null|string Значение  текущего пути в базе KeePass
     *
     * @return array Массив с информацией о записях
     */
    function search($username, $database = null, $database_path = null)
    {
        $result = [];
        if (is_null($database))
            $database = $this->db_structure;
        foreach ($database as $key => $value) {
            if ($key == $username && $value['username'] != "") {
                $result[$database_path."/".$key] = $value;
                continue;
            }
            if (is_array($value))
                $result = array_merge($result, $this->search($username, $value, $database_path.(is_null($database_path) ? "" : "/").$key));
        }
        return $result;
    }
    
    /**
     * @deprecated
     * Получение информации о записи/списке записей по указанному пути
     *
     * @param string              $path      Путь до записи в формате: "Group1/Group2/.../GroupN/Entry" или "Group1/Group2/.../GroupN"
     * @param \KeePassPHP\Group[] $groups    Массив групп, полученный функцией getGroups() класса keepass
     * @param null|array          $access    Массив со списком доступов
     * @param bool                $list_only [optional] Возвращать только список записей
     *
     * @return string|array
     *    Если путь указывает на запись, возвращается её uuid (если не выставлен флаг $list_only), если на группу, возвращается массив со списком uuid группы,
     *    если же такого пути нет, возвращается пустая строка
     */
    private function getEntry($path, $groups = null, $access = null, $list_only = false)
    {
        if (is_null($access))
            $access = ["allow" => [], "deny" => [], "order" => [], "tns" => [], "server" => []];
        if (is_null($groups))
            $groups = $this->array_of_groups;
        
        //  Если есть возможность отрезать от пути первый элемент, отделяем название от пути, а если нет, значит всё и есть название
        if (preg_match("/(.*?)\/(.*)/", $path, $tmp))
            list(, $name, $path) = $tmp;
        else
            list($name, $path) = [$path, ""];
        
        //  Заглушка Мегафон (не забыть исправить, условие ВСЕГДА работает, а не только на базах ГФ
        //  Подменяем название сегмента, если используется база KeePass, хранящая данные для БД Oracle ЕБ и нужно делать маппинг
        if (USEGFDATABASEKEEPASS && substr($name, 0, 6) == "GFBIS_")
            $name = "GFBIS";
        
        //  Если это первичный вызов ($groups может быть массивом только в этом случае)
        //  Ищем нужную группу и возвращаем по ней или информацию (если найдено), если не найдено, то возвращаем пустую строку
        if (is_array($groups)) {
            foreach ($groups as &$group_id)
                /** @var $group_id \KeePassPHP\Group */
                if ($group_id->name == $name)
                    return $this->getEntry($path, $group_id, $access, $list_only);
            return "";
        }
        
        //  Если в группе присутствуют данные, стираем из них историю и пароль, а затем запоминаем оставшееся
        $entries = array();
        /** @var $groups \KeePassPHP\Group */
        if ($groups->entries != null) {
            $local_data = [];
            foreach ($access as $access_key => $access_value)
                $local_data[$access_key] = $access_value[sizeof($access_value) - 1];
            foreach ($groups->entries as &$entry_id) {
                /** @var $entry_id \KeePassPHP\Entry */
                if (substr(trim($entry_id->title), 0, 1) == "_") {
                    $local_data[substr(strtolower(trim($entry_id->title)), 1)] = $entry_id->username;
                    $access[substr(strtolower(trim($entry_id->title)), 1)][] = $entry_id->username;
                    continue;
                }
                $clone_entry = clone $entry_id;
                unset($clone_entry->password);
                unset($clone_entry->history);
                $entries[$clone_entry->title] = (array)$clone_entry;
                
                //  Обогащаем поля правами, которые указаны в самой записи (если указаны)
                if (is_array($entry_id->extra)) {
                    foreach ($entry_id->extra as $item_key => $item_value) {
                        if (substr(trim($item_key), 0, 1) == "_")
                            $local_data[substr(strtolower(trim($item_key)), 1)] = $item_value;
                    }
                }
            }
            foreach ($entries as $entry_key => $entry_value) {
                /** @var $entry_id \KeePassPHP\Entry */
                $entries[$entry_key] = array_merge($entry_value, $local_data);
            }
        }
        
        //  Ищем среди групп нужную и возвращаем по ней информацию (если найдено), если не найдено, то возвращаем пустую строку
        /** @var $groups \KeePassPHP\Group */
        if ($groups->groups != null) {
            foreach ($groups->groups as &$group_id)
                /** @var $groups \KeePassPHP\Group */
                if ($group_id->name == $name)
                    return $this->getEntry($path, $group_id, $access, $list_only);
            return "";
        }
        
        //  Если искомый элемент присутствует в массиве и не выставлен флаг принудительного возврата списка,
        //  то возвращаем информацию по нему, а если нет, возвращаем информацию по всем записям в группе
        return (isset($entries[$name]) && !$list_only) ? $entries[$name] : $entries;
    }
    
    /**
     * Получение пароля записи
     *
     * @param string $uuid uuid записи
     *
     * @return string Возвращает пароль
     */
    function getPassword($uuid)
    {
        return $this->db->getPassword($uuid);
    }
    
}
