<?php
//  Отладка SQL выключена
if (!defined("DEFAULT_SQL_DEBUG"))
    define("DEFAULT_SQL_DEBUG", false);

//  Путь к php
if (!defined("PHP_PATH"))
    define("PHP_PATH", $_SERVER['HOME']."/etc/php/bin/php");

//  Путь к sqlplus
if (!defined("SQLPLUS_PATH"))
    define("SQLPLUS_PATH", "/u00/app/oracle/product/12.1.0/client_1/bin/sqlplus");

//  Путь в логу API
if (!defined("API_LOG"))
    define("API_LOG", "{$_SERVER['HOME']}/api.log");

//  Путь к GIT
if (!defined("GIT_PATH"))
    define("GIT_PATH", "/usr/bin/git");

//  Запуск GIT
if (!defined("GIT_RUN_PREFIX"))
    define("GIT_RUN_PREFIX", "ssh-agent bash -c 'ssh-add");

//  Название удалённого репозитория GIT
if (!defined("GITREMOTE"))
    define("GITREMOTE", "ansible-deploy");

//  Путь к ключу GIT
if (!defined("GITKEY"))
    define("GITKEY", "{$_SERVER['HOME']}/.ssh/id_rsa");

//  Путь к каталогу хранения репозиториев
if (!defined("GITREPOPATH"))
    define("GITREPOPATH", "{$_SERVER['HOME']}/repository");

//  Путь к хранилищу
if (!defined("VAULT_PATH"))
    define("VAULT_PATH", "{$_SERVER['HOME']}/vault");

//  Путь к ключам
if (!defined("PSDATA_KEYS_PATH"))
    define("PSDATA_KEYS_PATH", "/data/portaldata");

//  Путь к публичному ключу шифрования
if (!defined("VAULT_PUBLIC_KEY_FILE"))
    define("VAULT_PUBLIC_KEY_FILE", PSDATA_KEYS_PATH."/public.pem");

//  Путь к приватному ключу шифрования
if (!defined("VAULT_PRIVATE_KEY_FILE"))
    define("VAULT_PRIVATE_KEY_FILE", PSDATA_KEYS_PATH."/private.pem");

//  Путь к ключу KePass
if (!defined("KEEPASS_KEY_FILE"))
    define("KEEPASS_KEY_FILE", PSDATA_KEYS_PATH."/keepass.key");

//  Путь к git, в котором находится база KeePass
if (!defined("KEEPASS_REPOSITORY"))
    define("KEEPASS_REPOSITORY", "git@msk-bss-git01.megafon.ru:cfg/TNS.git");

//  Вычисляем путь корня репозитория
if (!defined("KEEPASSREPONAME"))
    define("KEEPASSREPONAME", preg_replace("/(.*?)([a-z]+\/.*?)(\.git|$)/", "\\2", KEEPASS_REPOSITORY));

//  Будем считать, что база KeePass находится в корне репозитория
if (!defined("KEEPASS_DATABASE"))
    define("KEEPASS_DATABASE", GITREPOPATH."/".KEEPASSREPONAME."/SUZ-BISOne.kdbx");

//  Путь к ansible
if (!defined("ANSIBLE"))
    define("ANSIBLE", "/usr/bin/ansible");

//  Путь к ansible-playbook
if (!defined("ANSIBLEPLAYBOOK"))
    define("ANSIBLEPLAYBOOK", "/usr/bin/ansible-playbook");

//  Путь к ansible-vault
if (!defined("ANSIBLEVAULT"))
    define("ANSIBLEVAULT", "/usr/bin/ansible-vault");

/*
//  Отключение авторизации (необходимо указывать в константах проектов - НИ В КОЕМ СЛУЧАЕ НЕ РАСКОММЕНТИРОВАТЬ ТУТ) - для роботов, работающих с API psdata
if (!defined("PSDATA_SKIP_AUTH"))
  define("PSDATA_SKIP_AUTH", true);

//  Отключение кэширования вывода (необходимо указывать в константах проектов - НИ В КОЕМ СЛУЧАЕ НЕ РАСКОММЕНТИРОВАТЬ ТУТ)
//    иногда роботам необходимо получать данные сразу, без кэширования (при ошибках кэш чистится))
if (!defined("PSDATA_CACHE_BUFFER_OUTPUT"))
  define("PSDATA_CACHE_BUFFER_OUTPUT", false);
*/
