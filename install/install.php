<?php
// Набор констант для шифрования при отсутствии авторизации
define("PSDATA_SKIP_AUTH", true);
define("PSDATA_CACHE_BUFFER_OUTPUT", false);

//  Загружаем библиотеки
define("SCRIPT_DIR", __DIR__);
@include_once "constant.php";
@include_once "psdata.inc.php";
@include_once "psdata/psdata.inc.php";
@include_once "../psdata/psdata.inc.php";
if (!defined("PSDATA_DIR"))
  die("Не найден каталог psdata");

//  Выходим при запуске через браузер
if (isset($_SERVER['SERVER_NAME']) || !is_array($_SERVER['argv']))
  api::out("Только для запуска в командной строке", 500);

//  Генерируем ключи
while (!file_exists(VAULT_PRIVATE_KEY_FILE) || !file_exists(VAULT_PUBLIC_KEY_FILE)) {
  print "Generate keys for vault...\n";
  `openssl genrsa -des3 -passout pass:x -out private.tmp 4096`;
  `openssl rsa -passin pass:x -in private.tmp -out private.pem`;
  `openssl rsa -in private.pem -outform PEM -pubout -out public.pem`;
  unlink("private.tmp");
  @mkdir(dirname(VAULT_PRIVATE_KEY_FILE), 0775, true);
  @mkdir(dirname(VAULT_PUBLIC_KEY_FILE), 0775, true);
  rename("private.pem", VAULT_PRIVATE_KEY_FILE);
  rename("public.pem", VAULT_PUBLIC_KEY_FILE);
}
print "\n\n";

//  Генерируем пароль для базы KeePass по-умолчанию
print "Enter password for default KeePass database (cfg/TNS) [hit enter for skip]: ";
$keepass = rtrim(fgets(STDIN));
if ($keepass != "") {
  $crypt_pass = vault::cryptPass(["pass" => $keepass]);
  if (file_put_contents(KEEPASS_KEY_FILE, $crypt_pass))
    print "Installed default KeePass password: successfully";
  else
    print "Installed default KeePass password: failed";
}
else
  print "Installed default KeePass password: skipped";
print "\n\n";

//  Генерируем пароль для других баз KeePass
while (true) {
  print "Enter path (with repository) for another KeePass database (like cfg_TNS_SUZ-BISOne) [hit enter for skip]: ";
  $databse_name = rtrim(fgets(STDIN));
  if ($databse_name == "") {
    print "Installed another KeePass password: skipped";
    break;
  }
  print "Enter password for KeePass database: ";
  $keepass = rtrim(fgets(STDIN));
  if ($keepass != "") {
    $crypt_pass = vault::cryptPass(["pass" => $keepass]);
    if (file_put_contents(dirname(KEEPASS_KEY_FILE)."/".$databse_name, $crypt_pass))
      print "Installed KeePass password for {$databse_name}: successfully";
    else
      print "Installed KeePass password for {$databse_name}: failed";
  }
  print "\n\n";
}
print "\n\n";

//  Обработка пользователя для работы с LDAP
print "Enter username for work with LDAP [gf-gnoc-svc]: ";
$username = rtrim(fgets(STDIN));
if ($username == "")
  $username = "gf-gnoc-svc";
do {
  print "Enter password for {$username} [hit enter for skip]: ";
  $password = rtrim(fgets(STDIN));
  $auth = json_decode(auth::getInfo(["username" => $username, "password" => $password]), true)['status'];
} while ($auth != 200 && $password != "");
if ($password != "") {
  $token = json_decode(auth::newToken(["username" => $username, "password" => $password]), true)['data'];
  $data = array(
    "name"     => $username,
    "password" => $password,
  );
  if (vault::setVaultPass(["key" => "psdata_ad_user", "pass" => json_encode($data, JSON_UNESCAPED_UNICODE)]) != "true")
    print "Installed LDAP auth user: failed";
  else
    print "Installed LDAP auth user: successfully";
}
else
  print "Installed LDAP auth user: skipped";
print "\n\n";
